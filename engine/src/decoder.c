#include "decoder.h"
#include <string.h>
#include <stdlib.h>

/***	HELPER FUNCTIONS	***/
static void parse_string_to_request(char ** const array, int length, bool * libraryindex, bool * topicindex, int * topic, int * exercise, char ** file)
{
	char * chunks[length];
	for (int i=0; i<length; i++)
	{
		chunks[i] = (char *) malloc(strlen(array[i])+1);
		strcpy(chunks[i],array[i]);
	}

	for (int i=0; i<length; i++)
	{		
		if(strcmp(chunks[i],MSGTYPE01) == 0)
		{
			*libraryindex = true;
		}
		else if (strcmp(chunks[i],MSGTYPE02) == 0)
		{
			*topicindex = true;	
		}
		else 
		{	
			char * field=NULL, * value=NULL;
			if (break_field_value(chunks[i], &field, &value)){
				if (strcmp(field, MSGTOPIC) == 0)
				{
					*topic = atoi(value);
					free(value);
				}
				else if (strcmp(field, MSGEXERCISE) == 0)
				{
					*exercise = atoi(value);
					free(value);
				}
				else if (strcmp(field, MSGFILE) == 0)
				{
					*file = value;
				}
			}
		}

	}
}

static int get_sum_message_combination(char ** const array, int length)
{
	int sum=0;
	for (int i=0;i<length; i++)
	{
		if (strcmp(array[i], MSGTYPE01) == 0) sum+= 1;
		else if (strcmp(array[i], MSGTYPE02) == 0) sum+= 2;
		else
		{
			char * field = NULL, * value = NULL;
			if (break_field_value(array[i], &field, &value))
			{	
				if (strcmp(field, MSGTOPIC) == 0) sum+= 4;
				else if (strcmp(field, MSGEXERCISE) == 0) sum+= 8;
				else if (strcmp(field, MSGFILE) == 0) sum+= 16;
				else sum += 32;
			}
		}
	}
	return sum;
}


static bool valid_message(char ** const array, int length)
{

	int sum = get_sum_message_combination(array, length);

	if (sum == 17) return true;
	else if (sum == 29) return true;
	else if (sum == 30) return true;

	return false;
}



/***	PUBLIC FUNCTIONS	***/
char ** break_string(char * const str, int * length)
{ 
	int i=0, fragments = 0;
	char ** array;
	char * chunk;
    char c[strlen(str)+1];
    strcpy(c,str);
    chunk = strtok(c, "?&");
    while(chunk != NULL)
	{
        fragments += 1;
        chunk = strtok(NULL, "?&");
    }
    memset(c,0,strlen(str));
    strcpy(c,str);
    array = (char **) malloc(fragments * sizeof(char *));
    chunk = strtok(c, "?&");
    array[i] = (char *) malloc(strlen(chunk)+1);
    strcpy(array[i],chunk);
    while(chunk != NULL)
    {
        i += 1;
        chunk = strtok(NULL, "?&");
        if (chunk != NULL)
        {
            array[i] = (char *) malloc(strlen(chunk)+1);
            strcpy(array[i],chunk);
        }
    }
    *length = fragments;

	return array;
}


bool break_field_value(char * const str, char ** field, char ** value)
{
	char chunk[strlen(str)];
	char * v= NULL;
	strcpy(chunk, str);
	*field = (char *) strtok_r(chunk,"=",&v);
	*value = malloc(strlen(v)+1);
	strcpy(*value,v);
	if (strlen(*field)>0 && strlen(*value)>0)
		return true;

	return false;
}



bool decode_message(char * const message, message_t ** mes)
{
	bool libraryindex=false, topicindex=false;
	char *file=NULL;
	int topic=0, exercise=0, length=0;;

	char ** m = break_string(message, &length);

	bool valid = valid_message(m, length);

	if (!valid) return false;

	parse_string_to_request(m, length, &libraryindex, &topicindex, &topic, &exercise, &file);

	*mes = (message_t *) malloc(sizeof(message_t));

	if (*mes == NULL)
		return false;

	(*mes)->libraryindex = libraryindex;
	(*mes)->topicindex = topicindex;
	(*mes)->topic = topic;
	(*mes)->exercise = exercise;
	(*mes)->file = file;

	for (int i=0; i <length; i++)
  	{
    	free(m[i]);
  	}
	free(m);
	
	return true;
}

void destroy_message(message_t * mes)
{
	(mes)? free(mes->file) : false;
	(mes)? free(mes) : false;
}