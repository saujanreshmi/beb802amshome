#include "thpool.h"
#include "kernel.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

extern pthread_mutex_t request_mutex;
extern pthread_cond_t got_request;

static request_t * head = NULL;
static request_t * tail = NULL;

static int requests = 0;
extern int clients;

/***	HELPER FUNCTIONS	***/
static request_t * get_request(void)
{
	int rc;
	request_t * tmp;
	if ((rc = pthread_mutex_lock(&request_mutex))!= 0 )
		printf("[WARNING]\tCannot lock the mutex [errono: %d]\n",rc);
	if (requests > 0){
		tmp = head;
		head= head->next;
		if (head == NULL){
			tail = NULL;
		}
		requests -= 1;
	}
	else{
		tmp = NULL;
	}
	if ((rc = pthread_mutex_unlock(&request_mutex)) != 0 )
		printf("[WARNING]\tCannot unlock the mutex [errono: %d]\n",rc);

	return tmp;
}


/***	PUBLIC FUNCTIONS	***/
bool add_request(char *ip, int sock, defence_t ** def, char * const user, char * const pass)
{
	int rc;
	request_t * tmp;

	tmp = (request_t *) malloc(sizeof(request_t));
	if (!tmp){
		perror("malloc");
	}
	strcpy(tmp->ip,ip);
	tmp->socket_id = sock;
	tmp->message = NULL;
	tmp->directory = def;
	tmp->username = user;
	tmp->password = pass;
	tmp->next = NULL;

	if ((rc = pthread_mutex_lock(&request_mutex)) != 0 )
		printf("[WARNING]\tCannot lock the mutex [errono: %d]\n",rc);

	if (requests == 0){
		head = tmp;
		tail = tmp;
	}
	else {
		tail->next = tmp;
		tail = tmp;
	}
	requests += 1;
	clients += 1;
	if ((rc = pthread_mutex_unlock(&request_mutex))!= 0 )
		printf("[WARNING]\tCannot unlock the mutex [errono: %d]\n",rc);

	if ((rc = pthread_cond_signal(&got_request))!= 0)
		printf("[WARNING]\tCannot signal the thread [errono: %d]\n",rc);

	return true;
}



void * handle_request(void * id)
{
	int rc;
	request_t * new_request;
	int thread_id = *((int *)id);
	if ((rc = pthread_mutex_lock(&request_mutex)) != 0 )
		printf("[WARNING]\tCannot lock the mutex [errono: %d]\n",rc);
	while(true)
	{
		if (requests > 0)
		{
			new_request = get_request();
			if (new_request)
			{
				if ((rc = pthread_mutex_unlock(&request_mutex)) != 0 )
					printf("[WARNING]\tCannot unlock the mutex [errono: %d]\n",rc);
		
				printf("[THREAD %d]\tEngine: got connection from %s in socket:%d\n", thread_id, new_request->ip, new_request->socket_id);
				
				if (!handle_user(new_request, thread_id))
					printf("[THREAD %d]\tEngine: Request Failed %s\n",thread_id, new_request->ip);
				else
					printf("[THREAD %d]\tEngine: Connection Closed by %s\n",thread_id, new_request->ip);
				clients -= 1;
				close(new_request->socket_id);
				free(new_request);
				if ((rc = pthread_mutex_lock(&request_mutex)) != 0 )
					printf("[WARNING]\tCannot lock the mutex [errono: %d]\n",rc);
			}
		}else
		{
			printf("[THREAD %d]\tWaiting for the request\n",thread_id);
			if ((rc = pthread_cond_wait(&got_request, &request_mutex)) != 0)
				printf("[WARNING]\tCannot block the thread [errono: %d]\n",rc);
		}
	}

}



void destroy_request(void)
{
	request_t * tmp = head;
	if (tmp != NULL)
	{	
		head = head->next;
		//clear message too
		free(tmp);
		tmp = head;
	}
}
