#include "messenger.h"
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

/***	HELPER FUNCTIONS	***/
static void clear_buffer(char * buffer, int length)
{
	memset(buffer, 0, length);
}


/***	PUBLIC FUNCTIONS	***/

bool receive_message(int socket_id, char * buffer, int length)
{
	int numbytes;
	clear_buffer(buffer, length);
	if ((numbytes= recv(socket_id, buffer, MAX_BUFFER_SIZE, 0)) == -1)
	{
		perror("recv");
		return false;
	}
	else if (numbytes == 0)
	{
		return false;	
	}
	buffer[numbytes] = '\0';
	return true;
}



bool send_message(int socket_id, char * buffer)
{
	if (strlen(buffer) == 0)
	{
		//buffer empty
		return false;
	}
	if (send(socket_id, buffer, strlen(buffer),0) == -1)
	{
		perror("send");
		return false;
	}
	return true;
}



bool receive_file(int socket_id, int file_size, char * filename, char * location)
{
	char filepath[strlen(location)+strlen(filename)+1];
	if (strlen(location) == 0 || strlen(filename)==0)
	{
		//filename or location empty
		return false;
	}
	strcpy(filepath, location);	
	strcat(filepath, filename);
	FILE *fp = fopen(filepath, "w");
	char *data = malloc(file_size+1);
    int sizecheck = 0;
    while(sizecheck < file_size){
		int received = recv(socket_id, data, file_size, 0);
		int written = fwrite(data, sizeof(char), received, fp);
      	sizecheck += written;
		for (int i=0;i<written; i++){
  	      	if (data[i] == '\n')	sizecheck += 1;
      	}
    }
    fclose(fp);
    free(data);
	return true;
}


bool send_file(int socket_id, char * filename, char * location)
{
	char filepath[strlen(location)+strlen(filename)+1];

	if (strlen(location) == 0 || strlen(filename)==0)
	{
		//filename or location empty
		return false;
	}
	strcpy(filepath, location);	
	strcat(filepath, filename);
	FILE *fp = fopen(filepath, "r");
	struct stat	obj;
	stat(filepath, &obj);
	int file_size = (int) obj.st_size;
	int sizecheck =0;
	char *data = (char *)malloc(file_size+1);
	while(sizecheck < file_size)
	{
		int read = fread(data, sizeof(char), file_size, fp);
		int sent = send(socket_id, data, read, 0);
		sizecheck += sent;
		for (int i=0; i<sent; i++)
		{
			if (data[i] == '\n') sizecheck += 1;
		}
	}
	fclose(fp);
	free(data);
	return true;
}






