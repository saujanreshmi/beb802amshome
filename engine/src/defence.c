#include "defence.h"

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

/***	HELPER FUNCTIONS	***/
static bool init_defence(defence_t ** def)
{
	*def = (defence_t *) malloc(sizeof(defence_t));
	if (!(*def))
	{
		perror("malloc");
		return false;
	}
	(*def)->topic = 0;
	(*def)->exercise = 0;
	(*def)->next = NULL;
	return true;
}

/***	PUBLIC FUNCTIONS	***/
bool check_defence(defence_t * const def, int topic, int exercise)
{
	defence_t *current = def;
	while(current != NULL)
	{
		if ((current->topic == topic) && (current->exercise == exercise))
		{
			return true;
		}
		current = current->next;
	}
	return false;
}



bool add_defence(defence_t ** def, int topic, int exercise)
{
	if (topic > 0 && exercise > 0 && !check_defence(*def, topic, exercise))
	{	
		defence_t * newnode = NULL;
		if (init_defence(&newnode))
		{
			if ((newnode->topic == 0) && (newnode->exercise == 0))
			{
				newnode->topic = topic;
				newnode->exercise = exercise;
				newnode->next = *def;
				*def = newnode;
				return true;
			}
		}
	}
	return false;
}



bool remove_defence(defence_t ** def, int topic, int exercise)
{
	defence_t *current = *def, *prev=NULL, *temp=NULL;
	while(current != NULL)
	{
		if ((current->topic == topic) && (current->exercise == exercise))
		{
			temp = current;
			current = temp->next;
			if (prev == NULL)
				*def = current;
			else
				prev->next = current;
			free(temp);
			return true;
		}
		prev = current;
		current = current->next;
	}
	return false;
}



void print_defence(defence_t * const def)
{
	defence_t * current = def;
	while(current != NULL)
	{
		printf("(T%dE%d)->",current->topic, current->exercise);
		current = current->next;
	}
	printf("\n");
}



void destroy_defence(defence_t * def)
{
	defence_t * temp = NULL;
	while(def != NULL)
	{
		temp = def;
		def = def->next;
		free(temp);
	}
}