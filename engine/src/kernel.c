#include "kernel.h"
#include "decoder.h"
#include "messenger.h"
#include <stdlib.h>
#include <semaphore.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>

extern sem_t r_mutex, w_mutex;

/***	HELPER FUNCTIONS	***/
static bool authenticate(request_t * req, char *msg)
{
	int length=0;
	int authorise = 0;

	char ** credential = break_string(msg, &length);
	char *field, *value;
	
	if (strcmp(credential[0],"login")==0)
	{
		for (int i=1; i<length; i++)
		{
			if (break_field_value(credential[i],&field, &value))
			{
				//printf("Value%d: %s %s\n",i,field,value);
				if (strcmp(field, "username")==0)
				{
					if (strcmp(value,req->username)==0)
						authorise += 1;
					free(value);
				}
				else if (strcmp(field, "password")==0)
				{
					if (strcmp(value,req->password)==0)
						authorise += 1;
					free(value);
				}
			}
		}
	}

	for (int i=0; i<length; i++)
  	{
    	free(credential[i]);
  	}

	free(credential);
	if (authorise == 2)
		return true;
	return false; 
}



static bool construct_filelocation(request_t * req, char location[])
{
	if (getcwd(location, MAX_BUFFER_SIZE) != NULL)
	{
		char * buf =  (char *) malloc(sizeof(char) * MAX_BUFFER_SIZE);
		strcpy(buf,location);
		char subfolder[MAX_BUFFER_SIZE];
		sprintf(subfolder,"/data/topic%d/exercise%d/",req->message->topic, req->message->exercise);
		strcat(buf, subfolder);
		memset(location, 0, MAX_BUFFER_SIZE);
		strcpy(location,buf);
		free(buf);
		return true;
	}
	else
	{
		perror("getcwd");
		return false;	
	}
}


static int process_user_request(request_t * req, char *msg)
{
	if (decode_message(msg, &(req->message)))
	{
		//wait here if this request topic and exercise are being served
		while (true){
			sem_wait(&r_mutex);
			sem_wait(&w_mutex);
			sem_post(&r_mutex);
			if (!check_defence(*(req->directory), req->message->topic, req->message->exercise))
			{
				sem_wait(&r_mutex);
				sem_post(&w_mutex);
				sem_post(&r_mutex);
				break;
			}
			sem_wait(&r_mutex);
			sem_post(&w_mutex);
			sem_post(&r_mutex);
		}

		sem_wait(&w_mutex);//need to lock this
		add_defence(req->directory, req->message->topic, req->message->exercise);
		sem_post(&w_mutex);

		char filesizerequest[MAX_BUFFER_SIZE];
		char buffer[MAX_BUFFER_SIZE];
		strcpy(filesizerequest,"getfilesize?");
		strcat(filesizerequest,req->message->file);
		send_message(req->socket_id, filesizerequest);
		receive_message(req->socket_id, buffer, MAX_BUFFER_SIZE);
		int size = atoi(buffer);
		send_message(req->socket_id, buffer);
		char filelocation[MAX_BUFFER_SIZE];
		construct_filelocation(req, filelocation);
		receive_file(req->socket_id, size, req->message->file, filelocation);
		
		if (req->message->libraryindex)
			return 1;
		else if (req->message->topicindex)
			return 2;
	}
	return 0;
}


/***	PUBLIC FUNCTIONS	***/
bool handle_user(request_t * req, int thid)
{
	struct stat st;
	char buffer[MAX_BUFFER_SIZE];
	receive_message(req->socket_id, buffer, MAX_BUFFER_SIZE);

	if (authenticate(req, buffer))
	{
		send_message(req->socket_id, SUCCESS);
		receive_message(req->socket_id, buffer, MAX_BUFFER_SIZE);
		
		int request = process_user_request(req, buffer);

		if (request == 1)
		{
			char filepath[MAX_BUFFER_SIZE];
			construct_filelocation(req, filepath);
			char command1[MAX_BUFFER_SIZE], command2[MAX_BUFFER_SIZE];
			sprintf(command1, "unzip %s%s -d %s",filepath, req->message->file, filepath);
			sprintf(command2, "rm %s%s",filepath, req->message->file);

			if (req->message->exercise != 0)	
				system(command1);//unzip
			
			system(command2);
		}
		else if (request == 2)
		{
			char filesize[MAX_BUFFER_SIZE];
			char filepath[MAX_BUFFER_SIZE];
			construct_filelocation(req, filepath);
			char command1[MAX_BUFFER_SIZE], command2[MAX_BUFFER_SIZE], command3[MAX_BUFFER_SIZE], command4[MAX_BUFFER_SIZE];
			sprintf(command1, "touch  %stranscript.txt",filepath);
			sprintf(command2, "make -C %s", filepath);
			sprintf(command3, "%s./game_controller >> %stranscript.txt", filepath, filepath);
			sprintf(command4, "rm %stranscript.txt", filepath);
			//run make command stream output to file
			system(command1);
			system(command2);
			system(command3);
			strcat(filepath, "transcript.txt");
			stat(filepath, &st);
			sprintf(filesize, "%d",st.st_size);
			//reply back with file
			//send file name
			send_message(req->socket_id, "transcript.txt");
			//receive file name
			receive_message(req->socket_id, buffer, MAX_BUFFER_SIZE);
			//send file size
			send_message(req->socket_id, filesize);
			//receive file size
			receive_message(req->socket_id, buffer, MAX_BUFFER_SIZE);

			construct_filelocation(req, filepath);
			send_file(req->socket_id, "transcript.txt", filepath);
			receive_message(req->socket_id, buffer, MAX_BUFFER_SIZE);
			system(command4);

		}
		else 
		{
			send_message(req->socket_id, FAILURE);
			return false;
		}

	}
	else 
	{
		send_message(req->socket_id, FAILURE);
		return false;
	}

	sem_wait(&w_mutex);
	remove_defence(req->directory, req->message->topic, req->message->exercise);
	sem_post(&w_mutex);
	send_message(req->socket_id, COMPLETE);
	return true;//successful communication
}
