#define _GNU_SOURCE
#include "thpool.h"
#include "defence.h"
#include "messenger.h"
#include <stdio.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <semaphore.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <errno.h>
#include <arpa/inet.h>
#include <signal.h>
#include <string.h>

#define PORT 1111

pthread_mutex_t request_mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
pthread_cond_t  got_request   = PTHREAD_COND_INITIALIZER;
sem_t r_mutex, w_mutex;
int clients = 0;
static defence_t * directory = NULL;
static char *username = NULL, *password = NULL; //authentication purpose 

/***	FUNCTION PROTOTYPES	***/
static bool parse_commandline_argument(int argc, char ** const arg, char **user, char **pass);
static bool create_socket(int *socket, struct sockaddr_in *m_addr);
static void mimic_signal(pid_t pid);
static void handle_signal(int sig);

int main(int argc, char **argv)
{
	//Get process id of the main thread
	int socket_fd, new_fd, clients=0;
	struct sockaddr_in my_addr;
	struct sockaddr_in their_addr;
	socklen_t sin_size;
	pthread_t pthread[MAX_REQUEST];
	int thid[MAX_REQUEST];
	pid_t mainthread = getpid();
	sem_init(&r_mutex, 0 ,1);
	sem_init(&w_mutex, 0 ,1);

	if (!parse_commandline_argument(argc, argv, &username, &password))
	{
		printf("[ERROR]\tparse_commandline_argument(int argc, char ** const arg, char **user, char **pass)\n");
		return EXIT_FAILURE;
	}

	if (!create_socket(&socket_fd, &my_addr))
	{
		printf("[ERROR]\tcreate_socket(int *socket, struct sockaddr_in *m_addr)\n");
		return EXIT_FAILURE;
	}

	//create threads to handle requests
	for(int i=0; i<MAX_REQUEST; i++)
	{
		thid[i] = i;
		if (pthread_create(&pthread[i], NULL, handle_request, (void *)&thid[i]) != 0){
			printf("[ERROR]\tfailure in threads creation\n");
			return EXIT_FAILURE; //error occured
		}
	}

	//signal hanlder if Ctrl+c interrupt is detected
	signal(SIGINT, handle_signal);

	while(true)
	{
		sin_size = sizeof(struct sockaddr_in);
		if ((new_fd = accept(socket_fd, (struct sockaddr *)&their_addr, &sin_size)) == -1) 
		{
			perror("accept");
			continue;
		}
		char buffer[MAX_BUFFER_SIZE];
		receive_message(new_fd, buffer, MAX_BUFFER_SIZE);
		if (clients < MAX_REQUEST)
		{
			if (strcmp(buffer,"signin")==0)//check if request is signin
			{
				send_message(new_fd, CONNECTED);
				if (!add_request(inet_ntoa(their_addr.sin_addr), new_fd, &directory, username, password))
				{
					printf("[ERROR]\tRequest cannot be queued\n");
					receive_message(new_fd, buffer, MAX_BUFFER_SIZE);
					send_message(new_fd, FAILURE);
					continue;
				}
			}
			else if (strcmp(buffer, "signout")==0)//check if request is to terminate engine
			{
				close(new_fd);
				mimic_signal(mainthread);
			}
			else 
			{
				send_message(new_fd,REJECTED);
				close(new_fd);
			}
		}
		else
		{
			close(new_fd);
			printf("[ROOT THR]\tEngine: Connection rejected for %s\n", inet_ntoa(their_addr.sin_addr));
		}

	}
	return EXIT_FAILURE;
}



static bool parse_commandline_argument(int argc, char ** const arg, char **user, char **pass)
{
	if (argc == 3)
	{
		*user = (char *) malloc(strlen(arg[1]));
		*pass = (char *) malloc(strlen(arg[2]));
		strcpy(*user, arg[1]);
		strcpy(*pass, arg[2]);
	}
	else{
		printf("[ERROR]\tUsage: ./engine <username> <password>\n");
		return false;
	}
	return true;
}



static bool create_socket(int *socket_id, struct sockaddr_in *m_addr)
{
	if ((*socket_id = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		return false;
	}
	m_addr->sin_family = AF_INET;         /* host byte order */
	m_addr->sin_port = htons(PORT);     /* short, network byte order */
	m_addr->sin_addr.s_addr = INADDR_ANY; /* auto-fill with my IP */

	/* bind the socket to the end point */
	if (bind(*socket_id, (struct sockaddr *)m_addr, sizeof(struct sockaddr)) == -1) {
		perror("bind");
		return false;
	}

	/* start listnening */
	if (listen(*socket_id, MAX_REQUEST) == -1) {
		perror("listen");
		return false;
	}
	printf("[ROOT THR]\tEngine started\n");
	printf("[ROOT THR]\tListening in %d\n",PORT);
	return true;
}



static void handle_signal(int sig)
{
	signal(sig, SIG_IGN);
	free(username);
	free(password);
	destroy_defence(directory);
	destroy_request();
	pthread_mutex_destroy(&request_mutex);
	printf("\n[ROOT THR]\tCtrl+c Detected!\n");
	printf("[ROOT THR]\tEngine: Terminating...\n");
	exit(EXIT_SUCCESS);
}


static void mimic_signal(pid_t pid)
{
	kill(pid, SIGINT);
}
