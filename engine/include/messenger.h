#ifndef MESSENGER_H
#define MESSENGER_H

#include <stdbool.h>

#define MAX_BUFFER_SIZE 1024
#define CONNECTED "connected"
#define REJECTED "rejected"
#define SUCCESS "authorised"
#define FAILURE "forbidden"
#define COMPLETE "success"
//This header file provides the communciation apis that communicates with the webserver

/***	HELPER FUNCTIONS	***/
//this function clears the buffer content
//static void clear_buffer(char * buffer, int length);

/***	PUBLIC FUNCTIONS	***/

bool receive_message(int socket_id, char * buffer, int length);

bool send_message(int socket_id, char * buffer);

bool receive_file(int socket_id, int file_size, char * filename, char * location);

bool send_file(int socket_id, char * filename, char * location);



#endif