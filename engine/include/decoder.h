#ifndef DECODER_H
#define DECODER_H

#include <stdbool.h>

#define MSGTYPE01 "libraryindex"
#define MSGTYPE02 "topicindex"
#define MSGTOPIC "topic"
#define MSGEXERCISE "exercise"
#define MSGFILE "file"

//This header file provides the api to decode the communication strings and returns struct's object


/* sample communication string:
	libraryindex?file=true                    -> libraries
	topicindex?topic=1&exercise=1&file=true   -> user submitted code for exercise = 1 of topic = 1 
	libraryindex?topic=1&exercise=1&file=true -> testing framework for exercise =1 of topic = 1
*/
typedef struct message message_t;

struct message{
	bool libraryindex;
	bool topicindex;
	int topic;
	int exercise;
	char *file;
};



/***	HELPER FUNCTIONS	***/
/*
//this function extracts the information from array
void parse_string_to_request(char ** const array, int length, bool * libraryindex, bool * topicindex, int * topic, int * exercise, char ** file);

//this function gets the sum of 
int get_sum_message_combination(char ** const array, int length);

//this function checks if request message is valid or not
bool valid_message(char * const message);*/

/***	PUBLIC FUNCTIONS	***/
//this function breaks the request string to chucks 
char ** break_string(char * const str, int * length);

//this function breaks the field value string into fied and values
bool break_field_value(char * const str, char ** field, char ** value);

bool decode_message(char * const message, message_t ** mes);

void destroy_message(message_t * mes);



/*
struct {
	bool libraryindex;
	bool topicindex;
	int topic;
	int exercise;
	bool file;
}

if libraryindex and topicindex both are true then attached file is test library for specific topic and exercise

if only libraryindex is true then attached file is emulator or zdk

if only topicindex is true then attached file is user submitted code

*/

#endif