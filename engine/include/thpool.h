#ifndef THPOOL_H
#define THPOOL_H

#include <pthread.h>
#include "decoder.h"
#include "defence.h"
//This header file provides the apis to manage multiple requests via threads
#define MAX_REQUEST 5
#define IPV4_LENGTH 16

typedef struct request request_t;

struct request{
	char ip[IPV4_LENGTH];
	int socket_id;
	message_t * message;
	defence_t ** directory;
	char * username;
	char * password;
	request_t * next;
};

/***	HELPER FUNCTIONS	***/
//static request_t * get_request(void);

/***	PUBLIC FUNCTIONS	***/
void * handle_request(void * id);

bool add_request(char *ip, int sock, defence_t ** def, char * const user, char * const pass);

void destroy_request(void);


#endif