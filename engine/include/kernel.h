#ifndef KERNEL_H
#define KERNEL_H

#include "thpool.h"
#include <stdbool.h>
//This is a heart of this program
/***	HELPER FUNCTIONS	***/
//static bool authenticate(request_t * req, char *msg);

/***	PUBLIC FUNCTIONS	***/
bool handle_user(request_t * req, int thid);

#endif 