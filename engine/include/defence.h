#ifndef DEFENCE_H
#define DEFENCE_H

#include <stdbool.h>

typedef struct defence defence_t;

struct defence{
	int topic;
	int exercise;
	defence_t * next;
};

/***	HELPER FUNCTIONS	***/
/*
static bool init_defence(defence_t ** def);*/

/***	PUBLIC FUNCTIONS	***/
bool add_defence(defence_t ** def, int topic, int exercise);

bool check_defence(defence_t * const def, int topic, int exercise);

bool remove_defence(defence_t ** def, int topic, int exercise);

void print_defence(defence_t * const def);

void destroy_defence(defence_t * def);
#endif