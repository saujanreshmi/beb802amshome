#ifndef _UTIL_DELAY_H_
#define _UTIL_DELAY_H_ 1

#ifndef F_CPU
/* prevent compiler error by supplying a default */
# warning "F_CPU not defined for <util/delay.h>"
# define F_CPU 1000000UL
#endif

void _delay_ms(double __ms);

void _delay_us(double __us);

#endif /* _UTIL_DELAY_H_ */
