#if ! defined(__FRAME_ANALYSIS_H__)
#define __FRAME_ANALYSIS_H__

#include "cells.h"
#include "cell_clusters.h"
#include "frames.h"

typedef struct {
	/**
	 *	Gets a list of cell clusters from a list of cells. 
	 */
	FunctionVar( GetConnectedCells, ppCellClusterVector ) ( 
		CellList * cells, 
		FunctionVar( predicate, bool ) ( pCell cell, Pointer data ), 
		Pointer data 
	);

	/**
	 *	Compares two frame lists, returning index number of the first pair of 
	 *	mismatching Frames. If a discrepancy is found, a standardised error 
	 *	message is displayed.
	 */
	FunctionVar( CompareFrames, void )( 
		FrameList * expected_frames, 
		FrameList * actual_frames, 
		int * frame_number, 
		bool * all_ok 
	);

	/**
	 *	Compares two frame lists, returning index number of the first pair of 
	 *	mismatching Frames. If a discrepancy is found, a standardised error 
	 *	message is displayed.
	 */
	FunctionVar(CompareScreens, void) (
		FrameList * expected_frames, 
		FrameList * actual_frames, 
		int * frame_number, 
		bool * all_ok
	);

} cFrameAnalysis;

/**
 *	Helper functions for analysis of frame and cell lists.
 */
extern cFrameAnalysis tFrameAnalysis;

#endif
