#ifndef __KEYWORD_H__
#define __KEYWORD_H__

#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stddef.h>
#include "types.h"
#include "utility.h"
#include "char_vector.h"

typedef enum Token {
	sError,
	sEndOfFile,

	// 6.4 token
	sKeyword,
	sIdentifier,
	sNumericLiteral, // Constant
	sStringLiteral,  // Constant
	sCharLiteral,    // Constant Note: char literals may be longer than 1 character.

	// 6.4 preprocessing token 
	sHeaderName,
	sPPIdentifier,
	sCharacterConstant,
	sOtherChar, // non-white-space-chars that cannot be one of the preprocessing tokens.

	// 6.4.1 Keywords
	sAuto,
	sBreak,
	sCase,
	sChar,
	sConst,
	sContinue,
	sDefault,
	sDefine,
	sDo,
	sDouble,
	sElse,
	sEnum,
	sExtern,
	sFloat,
	sFor,
	sGoto,
	sIf,
	sIfdef,
	sIfndef,
	sInline,
	sInt,
	sLong,
	sPragma,
	sRegister,
	sRestrict,
	sReturn,
	sShort,
	sSigned,
	sSizeof,
	sStatic,
	sStruct,
	sSwitch,
	sTypedef,
	sUndef,
	sUnion,
	sUnsigned,
	sVoid,
	sVolatile,
	sWhile,

	// Punctuators
	sLeftBracket,
	sRightBracket,
	sLeftPar,
	sRightPar,
	sLeftBrace,
	sRightBrace,
	sDot,
	sArrow,
	sPlusPlus,
	sMinusMinus,
	sAmp,
	sStar,
	sPlus,
	sMinus,
	sTilde,
	sBang,
	sSlash,
	sPercent,
	sLtLt,
	sGtGt,
	sLt,
	sGt,
	sLe,
	sGe,
	sEqEq,
	sBangEq,
	sCaret,
	sPipe,
	sAmpAmp,
	sPipePipe,
	sQuestion,
	sColon,
	sSemi,
	sEllipsis,
	sEq,
	sStarEq,
	sSlashEq,
	sPercentEq,
	sPlusEq,
	sMinusEq,
	sLtLtEq,
	sGtGtEq,
	sAmpEq,
	sCaretEq,
	sPipeEq,
	sComma,
	sCrunch,
	sCrunchCrunch,
	sLtColon,       // <:
	sColonGt,       // :>
	sLtPercent,     // <% 
	sPercentGt,     // %>
	sPercentColon,  // %:
	sPercentColon2, // %:%:

	sEOL, // new-line marker.
	sSpace, // arbitrary amount of space (' ', '\t', '\r')
	sComment, // A single-line or block comment.
} Token;

typedef struct Keyword {
	char * literal;
	Token tokenType;
} Keyword;

typedef Keyword * pKeyword;

typedef struct cKeyword {
	/**
	 *	An array containing the defined keywords, terminted by a record with 
	 *	tokenType = sError. 
	 */
	pKeyword List;

	/**
	 *	Searches the list for the designated identifier, returning non-null
	 *	record IFF there is a record matching (case sensitive) by name.
	 */
	pKeyword( *Find )( pString identifier );
} cKeyword;

/**
 *	Keyword pseudo-class.
 */
extern cKeyword tKeyword;
/*---------------------------------------------------------------------*/

typedef struct TokenVal {
	Token token;
	CharVector value;
} TokenVal;

/*---------------------------------------------------------------------*/
#endif
