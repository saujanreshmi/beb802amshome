#pragma once 

#include <stdbool.h>
#include <stdlib.h>
#include "function_types.h"

#include "utility.h"

typedef struct PointerVector {
	size_t capacity;
	size_t length;
	Pointer * data;
	FunctionVar( clone, void )( Pointer *dest, Pointer *source );
	FunctionVar( destructor, void ) ( Pointer *item );
} PointerVector;

typedef PointerVector *pPointerVector;

typedef struct cPointerVector {
	/* Allocate and initialise a new vector. */
	FunctionVar( Create, pPointerVector ) (
		size_t capacity,
		FunctionVar( clone, void )( Pointer *dest, Pointer *source ),
		FunctionVar( destructor, void ) ( Pointer *item )
		);

	/* Free the dynamic array and the vector itself (must have been initialised via Create). */
	FunctionVar( Destroy, void ) ( pPointerVector this );

	/* Initialise a pre-existing vector. */
	FunctionVar( Initialise, bool ) (
		pPointerVector this,
		size_t capacity,
		FunctionVar( clone, void )( Pointer *dest, Pointer *source ),
		FunctionVar( destructor, void ) ( Pointer *item )
		);

	/* Release the storage used by dynamic array without deallocating a pre-existing vector. */
	FunctionVar( Finalise, void ) (
		pPointerVector this
		);

	/* Append a new item to the vector. */
	FunctionVar( Add, bool ) (
		pPointerVector this,
		Pointer item
		);

	/* Insert a new item into the vector at the designated position. */
	FunctionVar( Insert, bool ) (
		pPointerVector this,
		Pointer item,
		size_t pos
		);

	/* Reallocate the dynamic array to a new size. */
	FunctionVar( Resize, bool ) (
		pPointerVector this,
		size_t new_size
		);

	/* Delete the designated item from the vector. If the destructor is not null it will be applied to the removed item prior to removal. */
	FunctionVar( Remove, void ) (
		pPointerVector this,
		size_t pos
		);

	/* Traverse the items in the vector. */
	FunctionVar( Foreach, void ) (
		pPointerVector this,
		FunctionVar( func, bool )( Pointer *item, Pointer data ),
		Pointer data
		);

	/* Reset the size of the vector back to zero. If the destructor is non-null, it will be invoked on each item. */
	FunctionVar( Clear, void ) (
		pPointerVector this
		);

	/* Appends a list of items to the current vector, using a shallow copy (if clone is null) or by calling clone for each item. */
	FunctionVar( AddRange, void ) (
		pPointerVector this,
		const Pointer * newItems,
		size_t n
		);

	/* Sets the item at the specified location to a new value. */
	FunctionVar( Set, void ) (
		pPointerVector this,
		size_t pos,
		Pointer value
		);

	/* Gets the item at the specified location. */
	FunctionVar( Get, Pointer ) (
		pPointerVector this,
		size_t pos
		);

} cPointerVector;

extern cPointerVector tPointerVector;
