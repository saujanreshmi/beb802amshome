#include <assert.h>
#include <stdlib.h>
#include <string.h>

#undef DB
#include "db.h"
#include "string_list.h"
#include "strutil.h"

//----------------------------------------------------------

/*
 *	Creates a string node, saves the supplied string pointer, and
 *	remembers whether it is ok to free the referenced text when the
 *	node is destroyed.
 */

StringNode * create_string_node( char * text, bool ok_to_free ) {
	StringNode * node = (StringNode *)calloc( 1, sizeof( StringNode ) );
	node->text = text;
	node->ok_to_free = ok_to_free;
	return node;
}

//----------------------------------------------------------

StringList * create_string_list( ) {
	return calloc( 1, sizeof( StringList ) );
}

//----------------------------------------------------------

void init_string_list( StringList * list ) {
	assert( list != NULL );

	list->first = list->last = NULL;
}

//----------------------------------------------------------

void add_string_node( StringNode * node, StringList * list ) {
	assert( node != NULL );
	assert( list != NULL );

	if ( list->first == NULL ) {
		list->first = node;
	}
	else {
		list->last->next = node;
	}

	list->last = node;
	list->count++;
}

//----------------------------------------------------------

bool string_nodes_equal( StringNode * node1, StringNode * node2 ) {
	assert( node1 != NULL );
	assert( node2 != NULL );

	return strcmp( node1->text, node2->text ) == 0;
}

//----------------------------------------------------------

/**
 *	Compares two lines and returns 
 */

bool string_lists_equal( StringList * list1, StringList * list2 ) {
	assert( list1 != NULL );
	assert( list2 != NULL );

	if ( list1->count != list2->count ) return false;

	StringNode * node1 = list1->first;
	StringNode * node2 = list2->first;

	while ( node1 != NULL && node2 != NULL ) {
		if ( ! string_nodes_equal( node1, node2 ) ) {
			return false;
		}
		node1 = node1->next;
		node2 = node2->next;
	}

	return true;
}

//----------------------------------------------------------

void print_lines( FILE * file, StringList * lines, int max_lines ) {
	assert( file != NULL );
	assert( lines != NULL );

	int n = 0;
	StringNode * current_line = lines->first;

	while ( n < max_lines && current_line != NULL ) {
		fprintf( file, "%s\n", current_line->text );
		current_line = current_line->next;
		n++;
	}
}

//----------------------------------------------------------

StringList * split_string( char * text, char * delimiters ) {
	if ( text == NULL || text[0] == '\0' ) {
		return NULL;
	}

	StringList * list = create_string_list();
	char * cursor = text;
	char * token = get_token( &cursor, delimiters );
	DUMP( ( "token = %s", token ) );

	while ( token != NULL ) {
		StringNode * node = create_string_node( token, true );
		add_string_node( node, list );
		token = get_token( &cursor, delimiters );
		DUMP( ( "token = %s", token ) );
	}

	return list;
}

//----------------------------------------------------------

void destroy_string_node( StringNode * node ) {
	assert( node != NULL );

	if ( node->ok_to_free && node->text != NULL ) {
		free( node->text );
	}

	free( node );
}

//----------------------------------------------------------

void destroy_string_list( StringList * list ) {
	assert( list != NULL );

	StringNode * next = list->first; 
	
	while( next != NULL ) {
		StringNode * next_next = next->next;
		destroy_string_node( next );
		next = next_next;
	}

	free( list );
}

//----------------------------------------------------------
