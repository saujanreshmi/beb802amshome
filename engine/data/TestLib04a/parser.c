#include "parser.h"

static void Initialise ( pParser instance, pScanner scanner, pString file_name );
static void ParseFile  ( pParser instance );

cParser tParser = {
	Initialise,
	ParseFile,
};

static void Initialise( pParser instance, pScanner scanner, pString file_name ) {
	instance->scanner = scanner;
	tSourceFile.Initialise( &instance->document_scope, file_name );
}

static void ParseFile( pParser instance ) {
}
