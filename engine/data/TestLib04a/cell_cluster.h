#ifndef __CELL_CLUSTER_H__
#define __CELL_CLUSTER_H__

#include "cell_vector.h"

typedef struct CellCluster CellCluster;
typedef CellCluster * pCellCluster;

struct CellCluster {
	ppCellVector cells;
};

typedef struct {
	FunctionVar( Create, pCellCluster ) ( void );
	FunctionVar( Destroy, void ) ( pCellCluster this );
	FunctionVar( IsAdjacent, bool ) ( pCellCluster this, pCell cell );
	FunctionVar( IsAdjacentAny, bool ) ( pCellCluster this, pCellCluster other );
} cCellCluster;

extern cCellCluster tCellCluster;

#endif
