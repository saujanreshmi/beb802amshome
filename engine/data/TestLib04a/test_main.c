#include "testlib_all.h"

int main( void ) {
	void run_all_tests( Action * tests, char * category );

	extern Action strutil_tests[];
	extern Action cell_tests[];
	extern Action vector_tests[];

	run_all_tests( strutil_tests, "strutil" );
	run_all_tests( cell_tests, "cell" );
	run_all_tests( vector_tests, "vector" );

	printf( "Unit tests done.\n" );
}

/**
 *	Executes each function in the array of tests.
 */
void run_all_tests( Action * tests, char * category ) {
	for ( int i = 0; tests[i] != NULL; i++ ) {
		printf( "Running %s test %d.\n", category, i );
		Action test = tests[i]; 
		test();
	}
}
