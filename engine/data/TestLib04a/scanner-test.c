/*
|+--------------------------------------------------------------------
||	File:	test_scanner.c
||	Author:	Lawrence Buckingham
||
||	Test shell for the scanner module.
|+--------------------------------------------------------------------
*/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include "scanner.h"

#undef DB
#include "db.h"

void test( void );

/*
**	Non-exhaustive test of numeric literals separated by newlines.
*/
void test_01( void ) {
	printf( "test_01: numeric literals" "\n" );

	FILE * old_stdin = stdin;
	FILE * old_stdout = stdout;

	char * infile =
		"1" "\n"
		"12" "\n"
		"123" "\n"
		"0x123abc" "\n"
		"6.023e+24" "\n"
		"9.10938356e-31" "\n"
		"0b11011011011101" "\n"
		"1ul" "\n"
		"17ULL" "\n"
		"0xffeeaaUL" "\n"
		"9.10938356e-31f" "\n"
		"1.6726219e-27l" "\n"
		"9.10938356e-31F" "\n"
		"1.6726219e-27L" "\n"
		;

	char * actual;
	size_t actual_len;

	stdin = fmemopen( infile, strlen( infile ), "r" );
	stdout = open_memstream( &actual, &actual_len );

	test();

	fclose( stdout );
	stdout = old_stdout;
	stdin = old_stdin;

	char * expected =
		"number              : 1" "\n"
		"number              : 12" "\n"
		"number              : 123" "\n"
		"number              : 0x123abc" "\n"
		"number              : 6.023e+24" "\n"
		"number              : 9.10938356e-31" "\n"
		"number              : 0b11011011011101" "\n"
		"number              : 1ul" "\n"
		"number              : 17ULL" "\n"
		"number              : 0xffeeaaUL" "\n"
		"number              : 9.10938356e-31f" "\n"
		"number              : 1.6726219e-27l" "\n"
		"number              : 9.10938356e-31F" "\n"
		"number              : 1.6726219e-27L" "\n"
		"EOF" "\n"
		;

	if ( strcmp( expected, actual ) ) {
		printf( "\tFailed\n" );
		printf( "\texpected = (((%s)))", expected );
		printf( "\tactual = (((%s)))", actual );
	}
	else {
		printf( "\tPassed\n" );
	}
	free( actual );
}

void test_02_single_line_comments( void ) {
	printf( "test_02: single line comments" "\n" );

	char * infile =
		"// A single line comment" "\n"
		"// Followed by a second single line comment" "\n"
		"/// A longer start to the comment" "\n"
		"///* a nested comment in a single line comment. */" "\n"
		"//" "\n"
		"// While the previous line contained nothing apart from the comment" "\n"
		"" "\n"
		"" "\n"
		"// What if there is // a comment in the middle?    " "\n"
		"// Part of\\" "\n"
		" a single line comment that extends across two lines." "\n"
		"/\\" "\n"
		"/ An unusually constructed single-line comment." "\n"
		;

	char * actual;
	size_t actual_len;

#pragma region Redirect stdio and run test.
	FILE * old_stdin = stdin;
	FILE * old_stdout = stdout;
	stdin = fmemopen( infile, strlen( infile ), "r" );
	stdout = open_memstream( &actual, &actual_len );

	test();

	fclose( stdout );
	stdout = old_stdout;
	stdin = old_stdin;
#pragma endregion

	char * expected =
		"comment             : // A single line comment" "\n"
		"comment             : // Followed by a second single line comment" "\n"
		"comment             : /// A longer start to the comment" "\n"
		"comment             : ///* a nested comment in a single line comment. */" "\n"
		"comment             : //" "\n"
		"comment             : // While the previous line contained nothing apart from the comment" "\n"
		"comment             : // What if there is // a comment in the middle?    " "\n"
		"comment             : // Part of a single line comment that extends across two lines." "\n"
		"comment             : // An unusually constructed single-line comment." "\n"
		"EOF" "\n"
		;

	if ( strcmp( expected, actual ) ) {
		printf( "\tFailed\n" );
		printf( "\texpected = (((%s)))", expected );
		printf( "\tactual = (((%s)))", actual );
	}
	else {
		printf( "\tPassed\n" );
	}
	free( actual );
}

void test_03_extended_lines( void ) {
	printf( "test_03: extended lines" "\n" );

	char * infile =
		"// a line which spreads across \\" "\n"
		"two lines" "\n"
		"// a line which spreads across \\" "\n"
		"three \\" "\n"
		"lines" "\n"
		"// a line which contains \\" "\n"
		"\\" "\n"
		"an intervening empty lines" "\n"
		"// a line which contains \\" "\n"
		"\\" "\n"
		"\\" "\n"
		"\\" "\n"
		"several intervening empty line" "\n"
		"\\" "\n"
		"\\" "\n"
		"\\" "\n"
		"// and finally a line that starts with several line extenders" "\n"
		;

	char * actual;
	size_t actual_len;

#pragma region Redirect stdio and run test.
	FILE * old_stdin = stdin;
	FILE * old_stdout = stdout;
	stdin = fmemopen( infile, strlen( infile ), "r" );
	stdout = open_memstream( &actual, &actual_len );

	test();

	fclose( stdout );
	stdout = old_stdout;
	stdin = old_stdin;
#pragma endregion

	char * expected =
		"comment             : // a line which spreads across two lines" "\n"
		"comment             : // a line which spreads across three lines" "\n"
		"comment             : // a line which contains an intervening empty lines" "\n"
		"comment             : // a line which contains several intervening empty line" "\n"
		"comment             : // and finally a line that starts with several line extenders" "\n"
		"EOF" "\n"
		;

	if ( strcmp( expected, actual ) ) {
		printf( "\tFailed\n" );
		printf( "\texpected = (((%s)))", expected );
		printf( "\tactual = (((%s)))", actual );
	}
	else {
		printf( "\tPassed\n" );
	}
	free( actual );
}

void test_04_block_comments( void ) {
	printf( "test_04: block_comments" "\n" );

	char * infile =
		"/* A block comment followed by an identifier: */" "abcdefg" "\n"
		"identifier /* followed by a comment */" "\n"
		"( /*** A longer start to the comment */ )" "\n"
		"( /* A longer end to the comment ****/ )" "\n"
		"/*// A comment with odd delimiters //*/" "\n"
		"/* /* a nested comment start inside a comment. */" "\n"
		"/* A comment that spans several lines" "\n"
		"" "\n"
		"" "\n"
		"   **" "\n"
		"**" "\n"
		"    and then finishes like this. */" "\n"
		;

	char * actual;
	size_t actual_len;

#pragma region Redirect stdio and run test.
	FILE * old_stdin = stdin;
	FILE * old_stdout = stdout;
	stdin = fmemopen( infile, strlen( infile ), "r" );
	stdout = open_memstream( &actual, &actual_len );

	test();

	fclose( stdout );
	stdout = old_stdout;
	stdin = old_stdin;
#pragma endregion

	char * expected =
		"comment             : /* A block comment followed by an identifier: */" "\n"
		"identifier          : abcdefg" "\n"
		"identifier          : identifier" "\n"
		"comment             : /* followed by a comment */" "\n"
		"left-paren          : (" "\n"
		"comment             : /*** A longer start to the comment */" "\n"
		"right-paren         : )" "\n"
		"left-paren          : (" "\n"
		"comment             : /* A longer end to the comment ****/" "\n"
		"right-paren         : )" "\n"
		"comment             : /*// A comment with odd delimiters //*/" "\n"
		"comment             : /* /* a nested comment start inside a comment. */" "\n"
		"comment             : /* A comment that spans several lines" "\n"
		"" "\n"
		"" "\n"
		"   **" "\n"
		"**" "\n"
		"    and then finishes like this. */" "\n"
		"EOF" "\n"
		;

	if ( strcmp( expected, actual ) ) {
		printf( "\tFailed\n" );
		printf( "\texpected = (((%s)))", expected );
		printf( "\tactual = (((%s)))", actual );
	}
	else {
		printf( "\tPassed\n" );
	}
	free( actual );
}

void test_05_include_directives( void ) {
	printf( "test_05: include directives" "\n" );

	char * infile =
		"#include <stdio.h>"     "\n"
		"#include <stddef.h>"    "\n"
		"#include \"types.h\""   "\n"
		"#include \"scanner.h\"" "\n"
		;

	char * actual;
	size_t actual_len;

#pragma region Redirect stdio and run test.
	FILE * old_stdin = stdin;
	FILE * old_stdout = stdout;
	stdin = fmemopen( infile, strlen( infile ), "r" );
	stdout = open_memstream( &actual, &actual_len );

	test();

	fclose( stdout );
	stdout = old_stdout;
	stdin = old_stdin;
#pragma endregion

	char * expected =
		"crunch              : #" "\n"
		"identifier          : include" "\n"
		"lt                  : <" "\n"
		"identifier          : stdio" "\n"
		"dot                 : ." "\n"
		"identifier          : h" "\n"
		"gt                  : >" "\n"

		"crunch              : #" "\n"
		"identifier          : include" "\n"
		"lt                  : <" "\n"
		"identifier          : stddef" "\n"
		"dot                 : ." "\n"
		"identifier          : h" "\n"
		"gt                  : >" "\n"

		"crunch              : #" "\n"
		"identifier          : include" "\n"
		"string              : \"types.h\"" "\n"

		"crunch              : #" "\n"
		"identifier          : include" "\n"
		"string              : \"scanner.h\"" "\n"

		"EOF" "\n"
		;

	if ( strcmp( expected, actual ) ) {
		printf( "\tFailed\n" );
		printf( "\texpected = (((%s)))", expected );
		printf( "\tactual = (((%s)))", actual );
	}
	else {
		printf( "\tPassed\n" );
	}
	free( actual );
}

void test_06_punctuation( void ) {
	printf( "test_06: punctuation" "\n" );

	char * infile =
		"[ ] ( ) { } . ->                       "
		"++ -- & * + - ~ !							"
		"/ % << >> < > <= >= == != ^ | && ||	"
		"? : ; ...								"
		"= *= /= %= += -= <<= >>= &= ^= |=		"
		", # ##									"
		"<: :> <% %> %: %:%:                   "
		;

	char * actual;
	size_t actual_len;

#pragma region Redirect stdio and run test.
	FILE * old_stdin = stdin;
	FILE * old_stdout = stdout;
	stdin = fmemopen( infile, strlen( infile ), "r" );
	stdout = open_memstream( &actual, &actual_len );

	test();

	fclose( stdout );
	stdout = old_stdout;
	stdin = old_stdin;
#pragma endregion

	char * expected =
		"left-bracket        : [" "\n" 
		"right-bracket       : ]" "\n" 
		"left-paren          : (" "\n" 
		"right-paren         : )" "\n"
		"left-brace          : {" "\n" 
		"right-brace         : }" "\n"
		"dot                 : ." "\n" 
		"arrow               : ->" "\n"
		"plus-plus           : ++" "\n" 
		"minus-minus         : --" "\n" 
		"amp                 : &" "\n" 
		"star                : *" "\n"
		"plus                : +" "\n" 
		"minus               : -" "\n" 
		"tilde               : ~" "\n" 
		"bang                : !" "\n"
		"slash               : /" "\n" 
		"percent             : %" "\n" 
		"lt-lt               : <<" "\n" 
		"gt-gt               : >>" "\n"
		"lt                  : <" "\n" 
		"gt                  : >" "\n" 
		"le                  : <=" "\n" 
		"ge                  : >=" "\n"
		"eq-eq               : ==" "\n" 
		"bang-eq             : !=" "\n" 
		"caret               : ^" "\n"
		"pipe                : |" "\n" 
		"amp-amp             : &&" "\n" 
		"pipe-pipe           : ||" "\n"
		"question            : ?" "\n" 
		"colon               : :" "\n" 
		"semicolon           : ;" "\n"
		"ellipsis            : ..." "\n" 
		"eq                  : =" "\n" 
		"star-eq             : *=" "\n"
		"slash-eq            : /=" "\n" 
		"percent-eq          : %=" "\n" 
		"plus-eq             : +=" "\n"
		"minus-eq            : -=" "\n" 
		"lt-lt-eq            : <<=" "\n" 
		"gt-gt-eq            : >>=" "\n"
		"amp-eq              : &=" "\n" 
		"caret-eq            : ^=" "\n" 
		"pipe-eq             : |=" "\n"
		"comma               : ," "\n" 
		"crunch              : #" "\n" 
		"crunch-crunch       : ##" "\n"
		"lt-colon            : <:" "\n" 
		"colon-gt            : :>" "\n" 
		"lt-percent          : <%" "\n"
		"percent-gt          : %>" "\n" 
		"percent-colon       : %:" "\n" 
		"percent-colon-2     : %:%:" "\n"
		"EOF" "\n"
		;

	if ( strcmp( expected, actual ) ) {
		printf( "\tFailed\n" );
		printf( "\texpected = (((%s)))", expected );
		printf( "\tactual = (((%s)))", actual );
	}
	else {
		printf( "\tPassed\n" );
	}
	free( actual );
}

void add_keyword( pKeyword k, pCharVector buffer ) {
	if ( buffer->length > 0 ) {
		buffer->length--;
	}

	tCharVector.AddRange( buffer, k->literal, strlen( k->literal ) );
	tCharVector.Add( buffer, '\n' );
	tCharVector.Add( buffer, '\0' );
}

void append_string( pString s, pCharVector buffer ) {
	if ( buffer->length > 0 ) {
		buffer->length--;
	}

	tCharVector.AddRange( buffer, s, strlen( s ) );
	tCharVector.Add( buffer, '\0' );
}

void add_keyword_result( pKeyword k, uint colonPos, pCharVector buffer ) {
	if ( buffer->length > 0 ) {
		buffer->length--;
	}

	tCharVector.AddRange( buffer, "keyword", strlen( "keyword" ) );

	for ( int i = strlen( "keyword" ); i < colonPos; i++ ) {
		tCharVector.Add( buffer, ' ' );
	}

	tCharVector.AddRange( buffer, ": ", 2 );
	tCharVector.AddRange( buffer, k->literal, strlen( k->literal ) );
	tCharVector.Add( buffer, '\n' );
	tCharVector.Add( buffer, '\0' );
}

void test_07_keywords( void ) {
	printf( "test_07: keywords" "\n" );

	pCharVector infile = tCharVector.Create( 1000, NULL, NULL );
	pKeyword keywords = tKeyword.List;

	for ( pKeyword k = keywords; k != NULL && k->tokenType != sError; k++ ) {
		add_keyword( k, infile );
	}

	char * actual;
	size_t actual_len;

#pragma region Redirect stdio and run test.
	FILE * old_stdin = stdin;
	FILE * old_stdout = stdout;
	stdin = fmemopen( infile->data, infile->length - 1, "r" );
	stdout = open_memstream( &actual, &actual_len );

	test();

	fclose( stdout );
	stdout = old_stdout;
	stdin = old_stdin;
#pragma endregion

	pCharVector expected = tCharVector.Create( 1000, NULL, NULL );

	for ( pKeyword k = keywords; k != NULL && k->tokenType != sError; k++ ) {
		add_keyword_result( k, 20, expected );
	}

	append_string( "EOF\n", expected );

	if ( strcmp( expected->data, actual ) ) {
		printf( "\tFailed\n" );
		printf( "\texpected = (((%s)))", expected->data );
		printf( "\tactual = (((%s)))", actual );
	}
	else {
		printf( "\tPassed\n" );
	}

	free( actual );
	tCharVector.Destroy( expected );
	tCharVector.Destroy( infile );
}

int	main( int argc, char **argv ) {
	Action tests[] = {
		test_01,
		test_02_single_line_comments,
		test_03_extended_lines,
		test_04_block_comments,
		test_05_include_directives,
		test_06_punctuation,
		test_07_keywords,
		NULL
	};

	for ( int i = 0; tests[i]; i++ ) {
		Action test = tests[i];
		test();
	}

	return 0;
}

void print_token( pScanner s, uint colonPos, const char * literal ) {
	pCharVector buffer = tCharVector.Create( 1000, NULL, NULL );
	tCharVector.AddRange( buffer, literal, strlen( literal ) );

	while ( buffer->length < colonPos ) {
		tCharVector.Add( buffer, ' ' );
	}

	tCharVector.AddRange( buffer, ": ", 2 );
	tCharVector.AddRange( buffer, s->word.data, s->word.length - 1 );
	tCharVector.Add( buffer, '\0' );

	printf( "%s\n", buffer->data );
	fflush( stdout );

	tCharVector.Destroy( buffer );
}

void test() {
	pScanner s = tScanner.NewScanner( "stdin", stdin );

	do {
		tScanner.SkipSpaces( s );

		switch ( s->token ) {
		case sError:
		{
			print_token( s, 20, "unrecognised input" );
			break;
		}
		case sIdentifier:
		{
			print_token( s, 20, "identifier" );
			break;
		}
		case sKeyword:
		{
			print_token( s, 20, "keyword" );
			break;
		}
		case sStringLiteral:
		{
			print_token( s, 20, "string" );
			break;
		}
		case sNumericLiteral:
		{
			print_token( s, 20, "number" );
			break;
		}
		case sLeftPar:
		{
			print_token( s, 20, "left-paren" );
			break;
		}
		case sRightPar:
		{
			print_token( s, 20, "right-paren" );
			break;
		}
		case sComma:
		{
			print_token( s, 20, "comma" );
			break;
		}
		case sCrunch:
		{
			print_token( s, 20, "crunch" );
			break;
		}
		case sCrunchCrunch:
		{
			print_token( s, 20, "crunch-crunch" );
			break;
		}
		case sComment:
		{
			print_token( s, 20, "comment" );
			break;
		}
		case sLt:
		{
			print_token( s, 20, "lt" );
			break;
		}
		case sLe:
		{
			print_token( s, 20, "le" );
			break;
		}
		case sLtLt:
		{
			print_token( s, 20, "lt-lt" );
			break;
		}
		case sLtLtEq:
		{
			print_token( s, 20, "lt-lt-eq" );
			break;
		}
		case sLtColon:
		{
			print_token( s, 20, "lt-colon" );
			break;
		}
		case sLtPercent:
		{
			print_token( s, 20, "lt-percent" );
			break;
		}
		case sGt:
		{
			print_token( s, 20, "gt" );
			break;
		}
		case sGe:
		{
			print_token( s, 20, "ge" );
			break;
		}
		case sGtGt:
		{
			print_token( s, 20, "gt-gt" );
			break;
		}
		case sGtGtEq:
		{
			print_token( s, 20, "gt-gt-eq" );
			break;
		}
		case sDot:
		{
			print_token( s, 20, "dot" );
			break;
		}
		case sLeftBracket:
		{
			print_token( s, 20, "left-bracket" );
			break;
		}
		case sRightBracket:
		{
			print_token( s, 20, "right-bracket" );
			break;
		}
		case sLeftBrace:
		{
			print_token( s, 20, "left-brace" );
			break;
		}
		case sRightBrace:
		{
			print_token( s, 20, "right-brace" );
			break;
		}
		case sBang:
		{
			print_token( s, 20, "bang" );
			break;
		}
		case sBangEq:
		{
			print_token( s, 20, "bang-eq" );
			break;
		}
		case sSlash:
		{
			print_token( s, 20, "slash" );
			break;
		}
		case sSlashEq:
		{
			print_token( s, 20, "slash-eq" );
			break;
		}
		case sStar:
		{
			print_token( s, 20, "star" );
			break;
		}
		case sStarEq:
		{
			print_token( s, 20, "star-eq" );
			break;
		}
		case sEllipsis:
		{
			print_token( s, 20, "ellipsis" );
			break;
		}
		case sArrow:
		{
			print_token( s, 20, "arrow" );
			break;
		}
		case sMinusMinus:
		{
			print_token( s, 20, "minus-minus" );
			break;
		}
		case sMinusEq:
		{
			print_token( s, 20, "minus-eq" );
			break;
		}
		case sMinus:
		{
			print_token( s, 20, "minus" );
			break;
		}
		case sPercent:
		{
			print_token( s, 20, "percent" );
			break;
		}
		case sPercentColon:
		{
			print_token( s, 20, "percent-colon" );
			break;
		}
		case sPercentColon2:
		{
			print_token( s, 20, "percent-colon-2" );
			break;
		}
		case sPercentEq:
		{
			print_token( s, 20, "percent-eq" );
			break;
		}
		case sPercentGt:
		{
			print_token( s, 20, "percent-gt" );
			break;
		}
		case sAmp:
		{
			print_token( s, 20, "amp" );
			break;
		}
		case sAmpAmp:
		{
			print_token( s, 20, "amp-amp" );
			break;
		}
		case sAmpEq:
		{
			print_token( s, 20, "amp-eq" );
			break;
		}
		case sEq:
		{
			print_token( s, 20, "eq" );
			break;
		}
		case sEqEq:
		{
			print_token( s, 20, "eq-eq" );
			break;
		}
		case sCaret:
		{
			print_token( s, 20, "caret" );
			break;
		}
		case sCaretEq:
		{
			print_token( s, 20, "caret-eq" );
			break;
		}
		case sPlus:
		{
			print_token( s, 20, "plus" );
			break;
		}
		case sPlusEq:
		{
			print_token( s, 20, "plus-eq" );
			break;
		}
		case sPlusPlus:
		{
			print_token( s, 20, "plus-plus" );
			break;
		}
		case sTilde:
		{
			print_token( s, 20, "tilde" );
			break;
		}
		case sQuestion:
		{
			print_token( s, 20, "question" );
			break;
		}
		case sPipe:
		{
			print_token( s, 20, "pipe" );
			break;
		}
		case sPipeEq:
		{
			print_token( s, 20, "pipe-eq" );
			break;
		}
		case sPipePipe:
		{
			print_token( s, 20, "pipe-pipe" );
			break;
		}
		case sColon:
		{
			print_token( s, 20, "colon" );
			break;
		}
		case sColonGt:
		{
			print_token( s, 20, "colon-gt" );
			break;
		}
		case sSemi:
		{
			print_token( s, 20, "semicolon" );
			break;
		}
		case sEndOfFile:
		{
			printf( "EOF\n" );
			break;
		}
		default:
		{
			char buffer[100];
			sprintf( buffer, "token with code %3d", (int) s->token );
			print_token( s, 20, buffer );
			break;
		}
		} // end of switch
	}
	while ( s->token != sEndOfFile );
}
