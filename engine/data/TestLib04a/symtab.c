/*
||	File:	symtab.c
||
||	A basic symbol table.
||
*/
#include <assert.h>
#include "symtab.h"

//------------------------------------------------------------------
static pSymbol	FindSymbol( pSymtab symtab, pString name );

/*---------------------------------------------------------*/
static void	FinaliseSymbol( pSymbol instance ) {
	if ( instance && instance->destructor ) {
		instance->destructor( instance );
	}
}
/*---------------------------------------------------------*/
static void	InitialiseSymbol(
	pSymbol this,
	SyntaxType syntax_type,
	pSymtab symtab,
	pString name,
	pString file_name,
	int line_number,
	SymbolPrinter printer,
	SymbolDestructor destructor
	) {
	this->destructor = destructor;
	this->fileName = file_name == NULL ? NULL : strdup( file_name );
	this->lineNum = line_number;
	this->name = name == NULL ? NULL : strdup( name );
	this->printer = printer;

	if ( symtab ) {
		tSymtab.Add( this, symtab );
	}

	this->syntaxType = syntax_type;
}
/*---------------------------------------------------------*/

static void destroy_helper( Pointer * p ) {
	FinaliseSymbol( *p );
}

// Initialise the fields of a symbol table, including the operation of adding 
// it to the nominated parent symbol table it that value is not null.
static void InitialiseSymtab( pSymtab this ) {
	tPointerVector.Initialise( &this->symbols, 10, NULL, destroy_helper );
}

/*---------------------------------------------------------*/

static void FinaliseSymtab( pSymtab instance ) {
	tPointerVector.Finalise( &instance->symbols );
}

/*---------------------------------------------------------*/
static void	AddSymbol( pSymbol symbol, pSymtab symtab ) {
	assert( symbol && symbol->enclosingScope == NULL );
	tPointerVector.Add( &symtab->symbols, symbol );
	symbol->enclosingScope = symtab;
}
/*---------------------------------------------------------*/
static pSymbol	FindSymbol( pSymtab symtab, pString name ) {
	if ( symtab == NULL ) return NULL;

	if ( symtab->symbols.length ) {
		for ( size_t i = 0; i < symtab->symbols.length; i++ ) {
			pSymbol symbol = symtab->symbols.data[i];

			if ( strcmp( name, symbol->name ) == 0 ) {
				return symbol;
			}
		}
	}

	return FindSymbol( symtab->base.enclosingScope, name );
}
/*---------------------------------------------------------*/
static void	PrintSymbol( FILE *stream, pSymbol s ) {
	if ( s->printer ) {
		s->printer( stream, s );
	}
}

/*---------------------------------------------------------*/

cSymtab tSymtab = {
	InitialiseSymtab,
	AddSymbol,
	FindSymbol,
	PrintSymbol,
	FinaliseSymtab,
};

// -----------------------------------------------------------------

cSymbol tSymbol = {
	InitialiseSymbol,
	PrintSymbol,
	FinaliseSymbol
};

// -----------------------------------------------------------------

static void PrintSourceFile( FILE * stream, pSymbol instance ) {
	pSourceFile srcFile = (pSourceFile) instance;
	fprintf( stream, "<source_file><name>%s</name><content>", srcFile->symtab.base.fileName );

	if ( srcFile->symtab.symbols.length ) {
		for ( size_t i = 0; i < srcFile->symtab.symbols.length; i++ ) {
			pSymbol content = srcFile->symtab.symbols.data[i];
			PrintSymbol( stream, content );
		}
	}

	fprintf( stream, "</content></source_file>" );
}

static void DestroySourceFile( pSymbol instance ) { 
	abort(); 
}

static void InitialiseSourceFile( pSourceFile instance, pString file_name ) {
	tSymbol.Initialise( &instance->symtab.base,
		se_source_file,
		NULL,
		"globals",
		file_name,
		0,
		PrintSourceFile,
		DestroySourceFile
		);
	tSymtab.Initialise( &instance->symtab );
}

static void FinaliseSourceFile( pSourceFile instance ) {
	tSymtab.Finalise( &instance->symtab );
	tSymbol.Finalise( &instance->symtab.base );
}

cSourceFile tSourceFile = {
	InitialiseSourceFile,
	FinaliseSourceFile,
};
// -----------------------------------------------------------------
