#pragma once

/*
==========================================================================
File:	lists.h
Author:	Lawrence Buckingham

Interface module for gerneral purpose linked list unit.

Nodes can be embedded in a data item, or just point to some data item.
==========================================================================
*/

#include <stddef.h>
#include <stdbool.h>
#include "types.h"
typedef struct Node 	*pNode;
typedef struct List	*pList;

typedef enum { Owner, Embedded, Hanger, Subtree } Role;

typedef struct Node {
		Role 	role;
		pNode 	left, right;
		pList 	owner;
		SafePointer	container;
	} Node;

#include "utility.h"

typedef void (*Destructor)( pNode node );

typedef struct List {
		Node	anchor;
		long	size;
		Destructor destroy;
	} List;

#define Initialised_List(l,d) List l = { { Owner, &l.anchor, &l.anchor, &l, NULL }, 0L, d }

#ifdef use_alternate_initlist	
	#define InitList InitList2
#else
	void	InitList( pList l, Destructor destroy );
#endif

void	InitList2( pList l, Destructor destroy );

void	InitNode( pNode p, Role role, SafePointer container );

void 	AddFirst( pNode n, pList l );

void 	AddLast( pNode n, pList l );

pNode	First( pList l );

pNode	Last( pList l );

bool ListEmpty( pList l );

#ifdef __USE_FUNCTION_RIGHT__
	pNode	Right( pNode n );
#else
	#define Right(n) ((n)->right->container == NULL ? NULL : (n)->right)
#endif

pNode	Left( pNode n );

void 	InsertNodeAfter( pNode n, pNode oldNode );

void	RemoveNode( pNode n );

pNode 	Element( long n, pList l );

typedef bool (*ListProc)( pNode n, SafePointer dataBlock );

void	TraverseList( pList l, ListProc p, SafePointer dataBlock );

SafePointer	Container( const pNode n );

void	DestroyEmbedded( pNode node );

void	DestroyHanger( pNode node );

void	DestroyList( pList l );

#define ForEachNode( n, l ) for( pNode n = First( l ); n != NULL; n = Right( n ) )

#define NewNode( n ) { NewMem( (n) ); InitNode( &(n)->link, Embedded, (n) ); }

#define NewNodeInList( n, l ) { NewNode( n ); AddLast( &(n)->link, (l) ); }

#define NewHanger( n, d ) { New(n); InitNode( &(n)->link, Hanger, (d) ); }

#define NewHangerInList( n, d ) { NewHanger(n,d); AddLast( &(n)->link, (l) ); }

/*-------------------------------------------------------------------*/
