#include <stdio.h>
#include <stdarg.h>
#include "db.h"

void __dump__( char * format, ... ) {
	va_list args;
	va_start( args, format );
	vfprintf( stderr, format, args );
	va_end( args );
	fprintf( stderr, "\n" );
	fflush( stderr );
}
