
#if 0
============================================================
List unit.
----------

A module of general-purpose linked list actions.

============================================================
#endif

#define __USE_FUNCTION_RIGHT__

#include <assert.h>
#include "lists.h"

void	DestroyEmbedded( pNode node )
{
	/* 
	||	assumes that the container holds the list node. 
	||	and that the container was created by 1 malloc.
	*/
	assert( node->role == Embedded );
	FreeMem( node->container );
}

void	DestroyHanger( pNode node )
{
	/* 
	||	Assumes that the container DOES NOT hold the list node. 
	*/
	assert( node->role == Hanger );
	FreeMem( node );
}

void	DestroyList( pList l )
{
	pNode n;
	
	for( n = First( l ); n != NULL; n = First( l ) )
	{
		RemoveNode( n );
		if( l->destroy ) l->destroy( n->container ); 
	}
}

void	TraverseList( pList l, ListProc p, SafePointer dataBlock )
{
	pNode	n = First( l );

	while( n != NULL && p( n, dataBlock ) ) n = Right( n );
}

void	InitList( pList l, Destructor destroy )
{
	assert( l != NULL );
	assert( destroy != NULL );
	
	l->anchor.role	    = Owner;
	l->anchor.right     = l->anchor.left = &l->anchor;
	l->anchor.container = NULL;
	l->anchor.owner     = l;
	l->size             = 0L;
	l->destroy	    = destroy;
}

void	InitList2( pList l, Destructor destroy )
{
	assert( l != NULL );
	assert( destroy != NULL );
	
	l->anchor.role	    = Owner;
	l->anchor.right     = l->anchor.left = &l->anchor;
	l->anchor.container = NULL;
	l->anchor.owner     = l;
	l->size             = 0L;
	l->destroy	    = destroy;
}

void InitNode( pNode n, Role role, SafePointer container )
{
	assert( n    != NULL );
	assert( container != NULL );
	
	n->role 	= role;
	n->right     	= n->left = NULL;
	n->owner     	= NULL;
	n->container 	= container;
}

void 	AddFirst( pNode n, pList l )
{
 
	assert( n->owner         == NULL );
	assert( n->container     != NULL );
	assert( l                != NULL );
	assert( l->anchor.owner == l );
	
	n->right	= l->anchor.right;
	n->right->left  = n;
	n->left		= &l->anchor;
	n->owner	= l;
	l->anchor.right	= n;
	l->size ++;
}

void 	AddLast( pNode n, pList l )
{
 
	assert( n->owner         == NULL );
	assert( n->container     != NULL );
	assert( l 	         != NULL );
	assert( l->anchor.owner == l );
	
	n->right       = &l->anchor;
	n->left	       = l->anchor.left;
	n->left->right = n;
	n->owner       = l;
	l->anchor.left = n;
	l->size ++;
}

pNode	First( pList l )
{
	assert( l != NULL );
	return l->size == 0 ? NULL : l->anchor.right;
}

pNode	Last( pList l )
{
	assert( l != NULL );

	return l->size == 0 ? NULL : l->anchor.left;
}

bool ListEmpty( pList l )
{
	assert( l != NULL );

	return l->size == 0;
}

pNode	Right( pNode n )
{
	assert( n        != NULL );
	assert( n->owner != NULL );

	return n->right->container == NULL ? NULL : n->right;
		/* Making use of the fact that container may only be NULL */
		/* for the list anchor.                                   */
}

pNode	Left( pNode n )
{
	assert( n        != NULL );
	assert( n->owner != NULL );

	return n->left->container == NULL ? NULL : n->left;
		/* Making use of the fact that container may only be NULL */
		/* for the list anchor.                                   */
}

void 	InsertNodeAfter( pNode n, pNode oldNode )
{
	assert( n	       != NULL );
	assert( n->owner       == NULL );
	assert( n->container   != NULL );
	assert( oldNode        != NULL );
	assert( oldNode->owner != NULL );
	assert( oldNode->owner->anchor.owner == oldNode->owner );

	n->right 	= oldNode->right;
	n->right->left 	= n;
	n->left 	= oldNode;
	oldNode->right 	= n;
	n->owner 	= oldNode->owner;
	n->owner->size ++;
}

void	RemoveNode( pNode n )
{
	assert( n              != NULL );
	assert( n->owner       != NULL );
	assert( n->container   != NULL );
	assert( n->owner->size >= 1 );
	
	n->right->left = n->left;
	n->left->right = n->right;
	n->owner->size --;
	
	InitNode( n, n->role, n->container );
}

SafePointer	Container( const pNode n )
{
	return n->container;
}

pNode 	Element( long n, pList l )
{
	long	i = 1;
	SafePointer	p = First( l );

	while( i < n && p != NULL )
	{
		i++;
		p = Right( p );
	} 

	return p;
}

/* End of File lists.c */
