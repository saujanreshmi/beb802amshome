#include <stdlib.h>
#include <cab202_timers.h>

/**
 *	TimerOverride namespace.
 */
typedef struct cTimerOverride {
	/**
	 *	Returns the true current clock time.
	 */
	double( *TrueTime )( void );

	/**
	 *	Sets the internal time ot a designated value. 
	 */
	void( *SetTime )( double value );

	/**
	 *	Installs the timer override functions and sets the
	 *	maximum jitter for time readings.
	 *
	 *	Input:
	 *		max_jitter - a small positive real number that serves as the upper
	 *					bound on a random time adjustment which is intended to 
	 *					mimic the inherent uncertainty in timings on a real 
	 *					system.
	 */
	void( *Install )( double max_jitter );

	/**
	 *	Restores time-keeping to defaults.
	 */
	void( *Uninstall ) ( void );
} cTimerOverride;

/**
 *	Timer override abstract data type.
 */
extern cTimerOverride TimerOverride;
