#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cab202_graphics.h>

#include "testlib_all.h"

/*
 * Renders something into the display and returns the resulting movie as a string.
 *
 * Make sure you free the string when you're finished with it.
 */

char * draw_one_frame() {
	FILE * old_stdout = stdout;
	char * junk;
	size_t jlen;
	stdout = open_memstream( &junk, &jlen );

	setup_screen();
	char * capture;
	size_t len;
	zdk_save_stream = open_memstream( &capture, &len );

	for ( int i = 0; i < 100; i++ ) {
		int x = rand() % screen_width();
		int y = rand() % screen_height();

		if ( zdk_screen->pixels[y][x] == ' ' ) {
			draw_char( x, y, '*' );
		}
		else {
			// Fudge the counter to keep going until we get a clear space.
			i--;
		}
	}

	show_screen();

	cleanup_screen();
	fclose( stdout );
	
	stdout = old_stdout;
	return capture;
}

static void two_identical_frames( void ) {
	char * capture = draw_one_frame();
	FILE * f = fmemopen( capture, strlen(capture), "r" );
	FrameList * expected = read_frame_list( f );
	fclose( f );

	assert( expected != NULL );
	assert( expected->count == 1 );

	f = fmemopen( capture, strlen( capture ), "r" );
	FrameList * actual = read_frame_list( f );
	fclose( f );

	assert( actual != NULL );
	assert( actual->count == 1 );

	Frame * expf, *actf;
	int frame_number;

#if defined(DB)
	print_frame_list( stdout, expected ); 
	print_frame_list( stdout, actual );
#endif

	assert( frame_lists_equal( expected, actual, &expf, &actf, &frame_number, false ) );

	printf( "Test passed!\n" );
}

Action cell_tests[] = {
	two_identical_frames,
	NULL
};
