#ifndef __language_h__
#define __language_h__
/* 
|+----------------------------------------------------------
||	unit:	language
||
|+----------------------------------------------------------
*/ 
#define 	IF	if(
#define 	THEN	){
#define 	ELIF	} else if(
#define 	ELSE	} else {
#define 	ENDIF	} /* end of if */

#define 	WHILE	while(
#define 	FOR	for(
#define 	DO	){
#define 	ENDFOR	}/* end of for */
#define 	ENDWHILE }/* end of while */

#define 	BEGIN	{ /* beginning of function */
#define 	END	} /* end of function */

#define 	AND	&&
#define 	OR	||
#define 	NOT	!
#define		EQ	==

#endif
