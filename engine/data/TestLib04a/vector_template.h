#pragma once 

#include <stdbool.h>
#include <stdlib.h>
#include "function_types.h"

typedef struct { int x, y, z; } _TYPE_; // Remove this when instantiating the type.

typedef struct _TYPE_Vector {
	size_t capacity;
	size_t length;
	_TYPE_ * data;
	FunctionVar( clone, void )( _TYPE_ *dest, _TYPE_ *source );
	FunctionVar( destructor, void ) ( _TYPE_ *item );
} _TYPE_Vector;

typedef _TYPE_Vector *p_TYPE_Vector;

typedef struct c_TYPE_Vector {
	/* Allocate and initialise a new vector. */
	FunctionVar( Create, p_TYPE_Vector ) (
		size_t capacity,
		FunctionVar( clone, void )( _TYPE_ *dest, _TYPE_ *source ),
		FunctionVar( destructor, void ) ( _TYPE_ *item )
		);

	/* Free the dynamic array and the vector itself (must have been initialised via Create). */
	FunctionVar( Destroy, void ) ( p_TYPE_Vector this );

	/* Initialise a pre-existing vector. */
	FunctionVar( Initialise, bool ) (
		p_TYPE_Vector this,
		size_t capacity,
		FunctionVar( clone, void )( _TYPE_ *dest, _TYPE_ *source ),
		FunctionVar( destructor, void ) ( _TYPE_ *item )
		);

	/* Release the storage used by dynamic array without deallocating a pre-existing vector. */
	FunctionVar( Finalise, void ) (
		p_TYPE_Vector this
		);

	/* Append a new item to the vector. */
	FunctionVar( Add, bool ) (
		p_TYPE_Vector this,
		_TYPE_ item
		);

	/* Insert a new item into the vector at the designated position. */
	FunctionVar( Insert, bool ) (
		p_TYPE_Vector this,
		_TYPE_ item,
		size_t pos
		);

	/* Reallocate the dynamic array to a new size. */
	FunctionVar( Resize, bool ) (
		p_TYPE_Vector this,
		size_t new_size
		);

	/* Delete the designated item from the vector. If the destructor is not null it will be applied to the removed item prior to removal. */
	FunctionVar( Remove, void ) (
		p_TYPE_Vector this,
		size_t pos
		);

	/* Traverse the items in the vector. */
	FunctionVar( Foreach, void ) (
		p_TYPE_Vector this,
		FunctionVar( func, bool )( _TYPE_ *item, Pointer data ),
		Pointer data
		);

	/* Reset the size of the vector back to zero. If the destructor is non-null, it will be invoked on each item. */
	FunctionVar( Clear, void ) (
		p_TYPE_Vector this
		);

	/* Appends a list of items to the current vector, using a shallow copy (if clone is null) or by calling clone for each item. */
	FunctionVar( AddRange, void ) (
		p_TYPE_Vector this,
		const _TYPE_ * newItems,
		size_t n
		);

	/* Sets the item at the specified location to a new value. */
	FunctionVar( Set, void ) (
		p_TYPE_Vector this,
		size_t pos,
		_TYPE_ value
		);

	/* Gets the item at the specified location. */
	FunctionVar( Get, _TYPE_ ) (
		p_TYPE_Vector this,
		size_t pos
		);

} c_TYPE_Vector;

extern c_TYPE_Vector t_TYPE_Vector;
