#if !defined(__MATHUTIL_H__)
#define __MATHUTIL_H__

#define max(x,y) ((x)>(y)?(x):(y))

#define min(x,y) ((x)<(y)?(x):(y))

typedef struct {
	bool has_value;
	int value;
}  nullable_int;

#endif
