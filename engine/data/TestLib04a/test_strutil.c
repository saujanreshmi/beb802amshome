#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "testlib_all.h"

void strutil_test01( void ) {
	printf( "strutil_test01..." );

	char * source_original = "The rain in Spain;lies  mainly in;;The;Plain";
	char * source = "The rain in Spain;lies  mainly in;;The;Plain";
	char * delimiters = " ;";
	char * expected_tokens[] ={
		"The", "rain", "in", "Spain", "lies", "", "mainly", "in", "", "The", "Plain",
	};

	int expected_count = sizeof( expected_tokens ) / sizeof( expected_tokens[0] );

	int actual_count = 0;
	char * cursor = source;
	char * token = get_token( &cursor, delimiters );
	DUMP( ( "token = %s", token ) );

	while ( token != NULL ) {
		free( token );
		token = get_token( &cursor, delimiters );
		actual_count++;
	}

	if ( token != NULL ) {
		free( token );
	}

	assert( expected_count == actual_count );
	assert( strcmp( source, source_original ) == 0 );

	cursor = source;

	for ( int i = 0; i < actual_count; i++ ) {
		char * token = get_token( &cursor, delimiters );
		bool tokens_match = strcmp( token, expected_tokens[i] ) == 0;
		free( token );
		assert( tokens_match );
	}

	printf( "Test passed!\n" );
}

void strutil_test02( void ) {

	TRACE;
	printf( "strutil_test02..." );

	char * in_text =
"TARGET=libtestlib02.a\n"
"FLAGS=-Wall -Werror -std=gnu99 -g\n"
"DIRS=-I../ZDK02 -L../ZDK02\n"
"LIB_SRC=cells.c db.c grames.c lines.c stdio_helpers.c strutil.c\n"
"LIB_HDR=cells.h db.h grames.h lines.h stdio_helpers.h strutil.h\n"
"TEST_SRC=test_main.c test_strutil.c\n"
"\n"
"all: $(TARGET) unit_tests\n"
"\n"
"clean:\n"
"	rm $(TARGET) \n"
"	rm *.o\n"
"	rm unit_tests.exe\n"
"\n"
"rebuild: clean all\n"
"\n"
"$(TARGET): *.c *.h\n"
"	gcc -c *.c $(FLAGS) $(DIRS)\n"
"	ar r $(TARGET) *.o\n"
"\n"
"unit_tests: $(TARGET) $(TEST_SRC)\n"
"	gcc -o $@ $(TEST_SRC) $(FLAGS) $(TARGET) $(DIRS) -lzdk02 -lncurses -lm -DDB\n"
					 ;

	char * split_text [] = {
"TARGET=libtestlib02.a",
"FLAGS=-Wall -Werror -std=gnu99 -g",
"DIRS=-I../ZDK02 -L../ZDK02",
"LIB_SRC=cells.c db.c grames.c lines.c stdio_helpers.c strutil.c",
"LIB_HDR=cells.h db.h grames.h lines.h stdio_helpers.h strutil.h",
"TEST_SRC=test_main.c test_strutil.c",
"",
"all: $(TARGET) unit_tests",
"",
"clean:",
"	rm $(TARGET) ",
"	rm *.o",
"	rm unit_tests.exe",
"",
"rebuild: clean all",
"",
"$(TARGET): *.c *.h",
"	gcc -c *.c $(FLAGS) $(DIRS)",
"	ar r $(TARGET) *.o",
"",
"unit_tests: $(TARGET) $(TEST_SRC)",
"	gcc -o $@ $(TEST_SRC) $(FLAGS) $(TARGET) $(DIRS) -lzdk02 -lncurses -lm -DDB",
	NULL,
	};

	TRACE;
	StringList * in_lines = split_string( in_text, "\n" );
	TRACE;

	StringNode * node = in_lines->first;
	int i = 0;
	bool all_ok = true;

	while ( node != NULL && split_text[i] != NULL ) {
		bool words_same = 0 == strcmp( split_text[i], node->text );

		if ( ! words_same ) {
			printf( "words do not match:\n\texpected = '%s'\n\tactual = '%s'\n\n", split_text[i], node->text );
		}

		all_ok = all_ok && words_same;
		node = node->next;
		i++;
	}

	assert( all_ok );

	printf( "Test passed!\n" );
}

Action strutil_tests[] ={
	strutil_test01,
	strutil_test02,
	NULL
};