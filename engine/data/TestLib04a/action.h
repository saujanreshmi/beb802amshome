#pragma once

/**
*	Callback function that represents a parameterless action.
*/

typedef void( *Action )( void );

/**
*	Callback function that represents a parameterless action.
*/

typedef void( *ParameterisedAction )( void * params );
