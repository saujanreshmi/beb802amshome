#if ! defined(__STRING_UTIL_H__)
#define __STRING_UTIL_H__

#include "function_types.h"

//-------------------------------------------------------------------

/**
 *	To make programs more readable. If I want to use the address of a char as
 *	the address of one char, use char *. If I want to abstract away the
 *	implementation of String a little, or beef it up as an object some
 *	how, I can do that too.
 */
typedef char *String;

typedef struct StringUtilClass {

	//-------------------------------------------------------------------

	/**
	 *	Gets the next token from the specified string. Similar to strtok
	 *	but not so destructive.
	 *
	 *	Advances the pointer referenced by cursor until either a terminating
	 *	zero or one of the delimiters is encountered.
	 *
	 *	The characters traversed up to but not including the delimiter are
	 *	concatenated to create a new, dynamically allocated string, which
	 *	becomes the return result. This is constructed via strdup, and so
	 *	it should be freed later.
	 *
	 *	On termination, the pointer referenced by cursor is pointing at
	 *	the character that caused the scan to stop. You can dereference
	 *	that pointer to to determine which delimiter was used to terminate
	 *	the scan.
	 *
	 *	Once a zero terminator is encountered, the scan will permanently stop,
	 *	and a NULL string is returned.
	 */

	FunctionVar( GetToken, String ) ( String *cursor, String delimiters );

	//-------------------------------------------------------------------

	/**
	 *	Compares two strings in a case-insensitive manner, return results that
	 *	are otherwise consistent with strcmp;
	 */

	FunctionVar( CompareInsensitive, int ) ( const String s, const String t );

	//-------------------------------------------------------------------
} StringUtilClass;

extern StringUtilClass tStringUtil;

#endif