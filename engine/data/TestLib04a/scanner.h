#pragma once

/*
|+----------------------------------------------------------------------
||	File:	scanner.h
||	Author: Lawrence Buckingham.
||
||	The scanner unit provides conversion of a sequential input
||	stream to a sequence of lexical tokens.
||
||	For each input stream, create a Scanner structure to control the
||	lexical scan, and associate it with the file.  This is operated
||	on by procedures in the scanner unit.
||
||	It recognises tokens as follows:
||
||	Identifier 	= letter { letter | digit }.
||	Number		= ['-']digits['.'digits][('E'|'e')['-']digits].
||	Comma		= ','.
||	LeftPar		= '('.
||	RightPar	= ')'.
||	String		= '"'{anychar}'"'
||	Comment 	= '{' {anything but '}'} '}'.
||
||	where	letter	= 'a' - 'z', 'A' - 'Z', '_'.
||		digit	= '0' - '9'.
||		digits	= digit { digit }.
||
||	This set of tokens suffices to allow reasonable flexibility in the
||	data files manipulated by the system.  Symbolic and numeric
||	attribute values are useable, and possibly structured values also.
|+----------------------------------------------------------------------
*/

#include <stdio.h>
#include <stddef.h>
#include "types.h"
#include "symtab.h"
#include "keyword.h"
#include "char_vector.h"

typedef struct Scanner {
	Node		link;
	pString		fileName;
	FILE 		*fileHandle;
	int			nextChar;
	CharVector	word;
	Token		token;
	long		lineNumber;
	long		magic;
} Scanner;

typedef Scanner *pScanner;

/**
 *	tScanner pseudo-class.
 */
typedef struct cScanner {
	/*
	||	If stream is NULL, attach scanner to stdin.  Return non-null
	||	iff success. Filename is purely for cosmetic purposes.
	*/
	pScanner( *NewScanner )( const char * fileName, FILE * stream );

	/*
	||	Close the file and reset scanner to prevent reading.
	*/
	void( *FreeScanner )( pScanner s );

	/*
	||	Instruct the scanner to read the next token.  This then
	||	becomes available in the 'token' field of the scanner.
	*/
	Token( *GetToken )( pScanner s );

	/*
	||	Consume and discard tokens until something other than a
	||	space or newline is encountered.
	*/
	Token( *SkipSpaces )( pScanner s );

	/*
	||	Return true iff last word matches string.
	*/
	bool( *Match )( pScanner scanner, pString string );

	/*
	||	Destructor for use in conjunction with List.
	*/
	void( *Destructor )( pNode node );
} cScanner;

extern cScanner tScanner;
