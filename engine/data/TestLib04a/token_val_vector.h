#pragma once 

#include <stdbool.h>
#include <stdlib.h>
#include "function_types.h"

#include "keyword.h"

typedef struct TokenValVector {
	size_t capacity;
	size_t length;
	TokenVal * data;
	FunctionVar( clone, void )( TokenVal *dest, TokenVal *source );
	FunctionVar( destructor, void ) ( TokenVal *item );
} TokenValVector;

typedef TokenValVector *pTokenValVector;

typedef struct cTokenValVector {
	/* Allocate and initialise a new vector. */
	FunctionVar( Create, pTokenValVector ) (
		size_t capacity,
		FunctionVar( clone, void )( TokenVal *dest, TokenVal *source ),
		FunctionVar( destructor, void ) ( TokenVal *item )
		);

	/* Free the dynamic array and the vector itself (must have been initialised via Create). */
	FunctionVar( Destroy, void ) ( pTokenValVector this );

	/* Initialise a pre-existing vector. */
	FunctionVar( Initialise, bool ) (
		pTokenValVector this,
		size_t capacity,
		FunctionVar( clone, void )( TokenVal *dest, TokenVal *source ),
		FunctionVar( destructor, void ) ( TokenVal *item )
		);

	/* Release the storage used by dynamic array without deallocating a pre-existing vector. */
	FunctionVar( Finalise, void ) (
		pTokenValVector this
		);

	/* Append a new item to the vector. */
	FunctionVar( Add, bool ) (
		pTokenValVector this,
		TokenVal item
		);

	/* Insert a new item into the vector at the designated position. */
	FunctionVar( Insert, bool ) (
		pTokenValVector this,
		TokenVal item,
		size_t pos
		);

	/* Reallocate the dynamic array to a new size. */
	FunctionVar( Resize, bool ) (
		pTokenValVector this,
		size_t new_size
		);

	/* Delete the designated item from the vector. If the destructor is not null it will be applied to the removed item prior to removal. */
	FunctionVar( Remove, void ) (
		pTokenValVector this,
		size_t pos
		);

	/* Traverse the items in the vector. */
	FunctionVar( Foreach, void ) (
		pTokenValVector this,
		FunctionVar( func, bool )( TokenVal *item, Pointer data ),
		Pointer data
		);

	/* Reset the size of the vector back to zero. If the destructor is non-null, it will be invoked on each item. */
	FunctionVar( Clear, void ) (
		pTokenValVector this
		);

	/* Appends a list of items to the current vector, using a shallow copy (if clone is null) or by calling clone for each item. */
	FunctionVar( AddRange, void ) (
		pTokenValVector this,
		const TokenVal * newItems,
		size_t n
		);

	/* Sets the item at the specified location to a new value. */
	FunctionVar( Set, void ) (
		pTokenValVector this,
		size_t pos,
		TokenVal value
		);

	/* Gets the item at the specified location. */
	FunctionVar( Get, TokenVal ) (
		pTokenValVector this,
		size_t pos
		);

} cTokenValVector;

extern cTokenValVector tTokenValVector;
