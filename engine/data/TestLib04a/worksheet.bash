# -----------------------------------------------

make parser-test && parser-test

if [ -f parser-test.exe.stackdump ]; then
	cat parser-test.exe.stackdump
	objdump -d -S parser-test.exe | less
	rm parser-test.exe.stackdump
fi

echo Done.
exit 0

# -----------------------------------------------

make scanner-test && scanner-test

if [ -f scanner-test.exe.stackdump ]; then
	cat scanner-test.exe.stackdump
	objdump -d -S scanner-test.exe | less
	rm scanner-test.exe.stackdump
fi

echo Done.
exit 0

# -----------------------------------------------

cd q:/temp
sort punctuators.txt >punctuators-sorted.txt
echo Done.
exit 0

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

make list-test && list-test 10

echo Done.
exit 0

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ls *.c | sort
echo ~~~~~~~~~~~~~~~~~~
ls *.h | sort

echo Done.
exit 0

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
