#ifndef __UTILITY_H__
#define __UTILITY_H__

/*
|+-----------------------------------+-------------------------------------
||	File:	utility.h            |
||	Author: Lawrence Buckingham  |
|+-----------------------------------+
||
||	Miscellaneous useful procedures.
|+-------------------------------------------------------------------------
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <limits.h>
#include <assert.h>
#include <math.h>
#include <time.h>
#include "types.h"
#include "language.h"
#include "table.h"
#include "lists.h"

#define Random HB_Random

extern pString	programName;

#define trace {printf( "file %s; line %d\n", __FILE__, __LINE__ ); fflush(stdout);} 

/*-----------------------------------------------------------------------
||	Central register of known objects.
*/
typedef struct {
	Node 	node;
	char	*type;
	long	para;
	char	*file;
	long	noid;
	int	line;
	void	*object;
} tPlug;
typedef tPlug *pPlug;

#ifdef TRACK_MEMORY

void	Register( void *object, pPlug plug, char *type, char *file, int line);
/*
||	Add the object to the registration list.
*/

void	Deregister_( pPlug plug, char *file, int line );
/*
||	Remove the link from the list.
*/

void	Display_Register( FILE *f );
/*
||	Print a list of plugs to the specified file.
*/

void	Display_Register_From( FILE *f, pPlug marker );
/*
||	Print a list of plugs to the specified file.
*/


void	Check_Register_( char *file, int line );
/*
||	Check list of plugs to (hopefully) see if they're valid.
*/

#define Relocate( p ) { (p).file = __FILE__; (p).line = __LINE__; }
#define Deregister( p ) Deregister_( p, __FILE__, __LINE__ )
#define Check_Register(x) Check_Register_( __FILE__, __LINE__ )
#else
#define Relocate( p ) {}
#define Deregister( p ) {}
#define Check_Register(x) {}
#define Register( o,p,t,f,l) {}
#define Deregister_( p, f, l ) {}
#define Display_Register(f) {}
#define Display_Register_From( f, m ) {}
#define Check_Register_( f, l ) {}
#endif
/*-----------------------------------------------------------------------
||	To do with partitioning datasets (done by index tables that
||	indicate whether a point is for training, testing, or neither.
*/
typedef enum { aUnavailable, aTrain, aTest } tAvailability;
typedef TableOf( tAvailability ) tPartition;
typedef tPartition *pPartition;
typedef TableOf( int ) tIntTable;
typedef tIntTable *pIntTable;

void	Print_Availability( FILE *f, tAvailability a );
/*
||	Display a value of type tAvailability.
*/

void	Generate_X_Fold( pIntTable x_fold, const pPartition part, int folds );
/*
||	Repartition states labelled Train in `part' into a set of `folds'
||	cross-validation sets, in x_fold.  The new values are in the range
||	1 .. folds, with zero indicating Unavailable points.
*/

void	Select_Partition( pPartition part, const pIntTable x_fold, int fold );
/*
||	Mark points in subset #fold as Test, and other non-zero
||	ones as Train.
*/

void	Initialise_Partition( pPartition part, int size );
/*
||	Create a "partition, with all points marked for training.
||	(This will be fed to Generate_X_Fold later.)
*/
/*-----------------------------------------------------------------------*/
typedef char String[256];
typedef char BigString[1000];
/*-----------------------------------------------------------------------*/
extern int	Execute( pString format, ... );
/*-----------------------------------------------------------*/
typedef	TableOf( int ) tPermutation;
typedef tPermutation *pPermutation;

extern void Generate_Permutation( pPermutation perm, uint N );
/*
||	Fills a table with a random permutation of the
||	numbers in 0 .. N-1.
*/
/*-----------------------------------------------------------*/
extern void	Progress( pString s, int upto, int outof );
/*-----------------------------------------------------------*/
extern double Quadratic_Decay(
	double	initial_learning_rate,
	uint	epoch,
	uint	maximum_epochs
	)
	/*
	||	Reduce the learing rate asymptotically to 0,
	||	using a quadratic decay.
	*/
	;
/*-----------------------------------------------------------*/
extern double Linear_Decay(
	double	initial_learning_rate,
	uint	epoch,
	uint	maximum_epochs
	)
	/*
	||	Reduce the learing rate asymptotically to 0,
	||	using a quadratic decay.
	*/
	;
/*-----------------------------------------------------------*/
extern void Randomize( int seed );
/*
||	Prepare the random number generator.
*/
/*-----------------------------------------------------------*/
extern double Random( double upperLimit );
/*
||	Generate a uniform random number in [ 0, upperLimit ).
*/
/*-----------------------------------------------------------*/
double Gaussian( double mean, double standard_deviation );
/*-----------------------------------------------------------*/
/*
||	Gets pointer to data area containig a string with the
||	current date-time in it.
*/
extern char *TimeStr( void );
/*-----------------------------------------------------------*/
/*
||	Return true iff strings are equal
*/
#define StrEq( s, t ) ( strcmp( (s), (t) ) == 0 )
/*-----------------------------------------------------------*/
typedef struct { int numberWanted; int remaining; } Selector;
typedef Selector *pSelector;
/*-----------------------------------------------------------*/
/*
||	Used to select a uniform sample of say, 5 out of 100.
||	Make a pass thru the data set, with numberWanted initialised
||	to the number you want, and remainig set to the size of the pop
||	ulation (at first). They are automatically counted down as cases
||	are selected.
*/
bool	Select( pSelector s );

#define NewMem(x) { (x) = calloc( 1, sizeof(*(x)) ); assert( (x) != NULL); }

#define NewArray(x,n) { (x) = calloc( (n), sizeof(*(x)) ); assert( (x) != NULL); }

#define NewMatrix(x,m,n) { int __i__; 		\
	(x) = calloc( (m), sizeof(*(x)) ); 	\
	assert( (x) != NULL); 			\
	for( __i__ = 0; __i__ < (m); __i__++ ){ 	\
		NewArray( (x)[__i__], (n) ); 	\
		} 					\
	}

#define FreeMem(x) { assert( (x) != NULL ); free( (x) ); (x) = NULL; }

#define swap(x,y,t) { t __tmp__ = x; x = y; y = __tmp__; }

#define ClearMem( x ) { memset( &x, 0, sizeof(x) ); }

#define not !
#define and &&
#define or ||
/*------------------------------------------------------------------------*/
/*
||	Echoes the set of strings pointed at by help to stderr
||	and kills the run.
*/
void Do_Help( pString help[] );
/*------------------------------------------------------------------------*/
/*
||	Use qsort to sort a buffer of doubles.
*/
void	Sort_Double( double *buffer, size_t size );
/*------------------------------------------------------------------------*/
#ifdef MAC
char * strdup( char * s );
#endif
/*----------------------------------------------------------------------*/
/*
||	Prints the specified arguments and then flushes standard output.
*/
int	Printf_Immediate( const char * format, ... );
/*----------------------------------------------------------------------*/
/*
||	Prints the specified arguments and then flusshes standard output.
*/
int	FPrintf_Immediate( FILE *f, const char * format, ... );
/*------------------------------------------------------------------------*/
#endif
