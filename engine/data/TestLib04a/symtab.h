#pragma once

#include "keyword.h"
#include "lists.h"
#include "utility.h"
#include "char_vector.h"
#include "token_val_vector.h"
#include "pointer_vector.h"

typedef enum SyntaxType {
	// A source file.
	se_source_file,

	// An unstructured token 
	se_token,

	// #include 
	se_include_directive,

	// #define
	se_define_directive,

	// #if, #ifdef, #ifndef, #elif. #else
	se_if_directive

	// 
} SyntaxType;

typedef struct Symbol Symbol;
typedef	Symbol *pSymbol;
typedef struct Symtab Symtab;
typedef Symtab * pSymtab;
typedef void( *SymbolPrinter )( FILE * stream, pSymbol symbol );
typedef void( *SymbolDestructor )( pSymbol symbol );

struct Symbol {
	SyntaxType	syntaxType;
	pSymtab		enclosingScope;
	pString		name;
	pString		fileName;
	int			lineNum;
	SymbolPrinter printer;
	SymbolDestructor destructor;
};

struct Symtab {
	Symbol base;
	PointerVector symbols;
};

typedef struct SourceFile {
	Symtab symtab;
} SourceFile;

typedef SourceFile * pSourceFile;

typedef struct cSourceFile {
	void( *Initialise )( pSourceFile instance, pString file_name );
	void( *Finalise )( pSourceFile instance );
} cSourceFile;

extern cSourceFile tSourceFile;

typedef struct IncludeDirective {
	Symtab symtab;
	CharVector included_file;
} IncludeDirective;

typedef struct cSymbol {
	// Initialise the fields of a symbol, including the operation of adding 
	// it to the nominated symbol table it that value is not null.
	void( *Initialise )(
		pSymbol instance,
		SyntaxType syntax_type,
		pSymtab symtab,
		pString name,
		pString file_name,
		int line_number,
		SymbolPrinter printer,
		SymbolDestructor destructor
		);

	// Calls the symbol's print function
	void( *Print )( FILE *f, pSymbol s );

	// Clean up a symbol.
	void( *Finalise )( pSymbol instance );
} cSymbol;

extern cSymbol tSymbol;

typedef struct cSymtab {
	// Initialise the fields of a symbol table, including the operation of adding 
	// it to the nominated parent symbol table it that value is not null.
	void( *Initialise )( pSymtab this );

	// Adds a symbol to the designated symbol table.
	void( *Add )( pSymbol symbol, pSymtab symtab );

	// Searches the chain of containing symbol tables starting at symtab,
	// working upwards through the parent chain until either a symbol with
	// the designated name is found, or we run out of places to look.
	pSymbol( *Find )( pSymtab symtab, pString name );

	// If the printer attached to symbol s is non-null, it is 
	// called to serialise the symbol state to the designated stream.
	void( *PrintSymbol )( FILE *stream, pSymbol s );

	// Destructor for use in linked lists.
	void( *Finalise )( pSymtab instance );
} cSymtab;

extern cSymtab tSymtab;
