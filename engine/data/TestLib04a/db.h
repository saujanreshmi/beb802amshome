#if !defined(__DB_H__)
#define __DB_H__

#if defined(DB)
void __dump__( char * format, ... );
#define DUMP(args) { __dump__ args; }
#else
#define DUMP(args) 
#endif

#define TRACE DUMP(( "%s(%d)\n", __FILE__, __LINE__ ))
#define ENTER(proc) DUMP(( "%s - %s(%d)\n", #proc, __FILE__, __LINE__ ))

#endif
