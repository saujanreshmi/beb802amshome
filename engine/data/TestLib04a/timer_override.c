#include <stdio.h>
#include <stdlib.h>
#include <cab202_timers.h>
#include "timer_override.h"

static double override_time;
static double jitter = 0.001;

static void delay() {
	//
	double delay = jitter * rand() / RAND_MAX;
	override_time += delay;
}

static double get_true_current_time() {
	void * p = zdk_get_current_time;
	zdk_get_current_time = NULL;
	double result = get_current_time();
	zdk_get_current_time = p;
	return result;
}

static double override_get_current_time( void ) {
#if defined(DB)
	fprintf( stderr, "override_get_current_time @ %f\n", get_true_current_time() );
#endif

	delay();
	return override_time;
}

static void override_timer_pause( long milliseconds ) {
#if defined(DB)
	fprintf( stderr, "override_timer_pause @ %f\n", get_true_current_time() );
#endif

	delay();
	override_time += milliseconds / 1000.0;
}

static void install( double jitter_ ) {
	jitter = jitter_;
	zdk_get_current_time = override_get_current_time;
	zdk_timer_pause = override_timer_pause;
}

static void uninstall() {
	zdk_get_current_time = NULL;
	zdk_timer_pause = NULL;
}

static void set_time( double value ) {
	override_time = value;
}

cTimerOverride TimerOverride = {
	get_true_current_time,
	set_time,
	install,
	uninstall,
};
