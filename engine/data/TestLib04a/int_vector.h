#pragma once 

#include <stdbool.h>
#include <stdlib.h>
#include "function_types.h"

typedef int Int;

typedef struct IntVector {
	size_t capacity;
	size_t length;
	Int * data;
	FunctionVar( clone, void )( Int *dest, Int *source );
	FunctionVar( destructor, void ) ( Int *item );
} IntVector;

typedef IntVector *pIntVector;

typedef struct cIntVector {
	/* Allocate and initialise a new vector. */
	FunctionVar( Create, pIntVector ) (
		size_t capacity,
		FunctionVar( clone, void )( Int *dest, Int *source ),
		FunctionVar( destructor, void ) ( Int *item )
		);

	/* Free the dynamic array and the vector itself (must have been initialised via Create). */
	FunctionVar( Destroy, void ) ( pIntVector this );

	/* Initialise a pre-existing vector. */
	FunctionVar( Initialise, bool ) (
		pIntVector this,
		size_t capacity,
		FunctionVar( clone, void )( Int *dest, Int *source ),
		FunctionVar( destructor, void ) ( Int *item )
		);

	/* Release the storage used by dynamic array without deallocating a pre-existing vector. */
	FunctionVar( Finalise, void ) (
		pIntVector this
		);

	/* Append a new item to the vector. */
	FunctionVar( Add, bool ) (
		pIntVector this,
		Int item
		);

	/* Insert a new item into the vector at the designated position. */
	FunctionVar( Insert, bool ) (
		pIntVector this,
		Int item,
		size_t pos
		);

	/* Reallocate the dynamic array to a new size. */
	FunctionVar( Resize, bool ) (
		pIntVector this,
		size_t new_size
		);

	/* Delete the designated item from the vector. If the destructor is not null it will be applied to the removed item prior to removal. */
	FunctionVar( Remove, void ) (
		pIntVector this,
		size_t pos
		);

	/* Traverse the items in the vector. */
	FunctionVar( Foreach, void ) (
		pIntVector this,
		FunctionVar( func, bool )( Int *item, Pointer data ),
		Pointer data
		);

	/* Reset the size of the vector back to zero. If the destructor is non-null, it will be invoked on each item. */
	FunctionVar( Clear, void ) (
		pIntVector this
		);

	/* Appends a list of items to the current vector, using a shallow copy (if clone is null) or by calling clone for each item. */
	FunctionVar( AddRange, void ) (
		pIntVector this,
		const Int * newItems,
		size_t n
		);

	/* Sets the item at the specified location to a new value. */
	FunctionVar( Set, void ) (
		pIntVector this,
		size_t pos,
		Int value
		);

	/* Gets the item at the specified location. */
	FunctionVar( Get, Int ) (
		pIntVector this,
		size_t pos
		);

} cIntVector;

extern cIntVector tIntVector;
