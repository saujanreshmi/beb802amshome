#if ! defined ( __STRUTIL_H__ )
#define __STRUTIL_H__

/**
 *	Gets the next token from the specified string. Similar to strtok 
 *	but not so destructive.
 *
 *	Advances the pointer referenced by cursor until either a terminating 
 *	zero or one of the delimiters is encountered.
 *
 *	The characters traversed up to but not including the delimiter are
 *	concatenated to create a new, dynamically allocated string, which 
 *	becomes the return result. This is constructed via strdup, and so 
 *	it should be freed later.
 *
 *	On termination, the pointer referenced by cursor is pointing at 
 *	the character that caused the scan to stop. You can dereference 
 *	that pointer to to determine which celimiter was used to terminate
 *	the scan.
 *
*	Once a zero terminator is encountered, the scan will permanently stop,
*	and a NULL string is returned.
*/

char * get_token( char **cursor, char * delimiters );

#endif