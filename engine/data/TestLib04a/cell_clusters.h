#pragma once 

#include <stdbool.h>
#include <stdlib.h>
#include "function_types.h"

#include "cell_cluster.h"

typedef struct pCellClusterVector {
	size_t capacity;
	size_t length;
	pCellCluster * data;
	FunctionVar( clone, void )( pCellCluster *dest, pCellCluster *source );
	FunctionVar( destructor, void ) ( pCellCluster *item );
} pCellClusterVector;

typedef pCellClusterVector *ppCellClusterVector;

typedef struct cpCellClusterVector {
	/* Allocate and initialise a new vector. */
	FunctionVar( Create, ppCellClusterVector ) (
		size_t capacity,
		FunctionVar( clone, void )( pCellCluster *dest, pCellCluster *source ),
		FunctionVar( destructor, void ) ( pCellCluster *item )
		);

	/* Free the dynamic array and the vector itself (must have been initialised via Create). */
	FunctionVar( Destroy, void ) ( ppCellClusterVector this );

	/* Initialise a pre-existing vector. */
	FunctionVar( Initialise, bool ) (
		ppCellClusterVector this,
		size_t capacity,
		FunctionVar( clone, void )( pCellCluster *dest, pCellCluster *source ),
		FunctionVar( destructor, void ) ( pCellCluster *item )
		);

	/* Release the storage used by dynamic array without deallocating a pre-existing vector. */
	FunctionVar( Finalise, void ) (
		ppCellClusterVector this
		);

	/* Append a new item to the vector. */
	FunctionVar( Add, bool ) (
		ppCellClusterVector this,
		pCellCluster item
		);

	/* Insert a new item into the vector at the designated position. */
	FunctionVar( Insert, bool ) (
		ppCellClusterVector this,
		pCellCluster item,
		size_t pos
		);

	/* Reallocate the dynamic array to a new size. */
	FunctionVar( Resize, bool ) (
		ppCellClusterVector this,
		size_t new_size
		);

	/* Delete the designated item from the vector. If the destructor is not null it will be applied to the removed item prior to removal. */
	FunctionVar( Remove, void ) (
		ppCellClusterVector this,
		size_t pos
		);

	/* Traverse the items in the vector. */
	FunctionVar( Foreach, void ) (
		ppCellClusterVector this,
		FunctionVar( func, bool )( pCellCluster *item, Pointer data ),
		Pointer data
		);

	/* Reset the size of the vector back to zero. If the destructor is non-null, it will be invoked on each item. */
	FunctionVar( Clear, void ) (
		ppCellClusterVector this
		);

	/* Appends a list of items to the current vector, using a shallow copy (if clone is null) or by calling clone for each item. */
	FunctionVar( AddRange, void ) (
		ppCellClusterVector this,
		const pCellCluster * newItems,
		size_t n
		);

	/* Sets the item at the specified location to a new value. */
	FunctionVar( Set, void ) (
		ppCellClusterVector this,
		size_t pos,
		pCellCluster value
		);

	/* Gets the item at the specified location. */
	FunctionVar( Get, pCellCluster ) (
		ppCellClusterVector this,
		size_t pos
		);

} cpCellClusterVector;

extern cpCellClusterVector tpCellClusterVector;
