#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stddef.h>
#include <stdbool.h>

#include "frames.h"
#include "strutil.h"
#include "string_list.h"
#include "cab202_graphics.h"

#undef DB
#include "db.h"

//----------------------------------------------------------

/**
 *	Creates a newly allocated, empty, Frame.
 */

Frame * create_frame() {
	Frame * frame = (Frame *) calloc( 1, sizeof( Frame ) );
	return frame;
}
//----------------------------------------------------------

/**
 *	Creates a newly allocated, empty, FrameList.
 */

FrameList * create_frame_list( void ) {
	return (FrameList *) calloc( 1, sizeof( FrameList ) );
}

//----------------------------------------------------------

/**
 *	Appends a frame to the designated list, setting the screen number
 *	of the frame (if it is a screen) and updating the count of the and 
 *	screenCountframe list to reflect the position of the frame within 
 *	the list.
 */

void add_frame( Frame * frame, FrameList * list ) {
	if ( list->first == NULL ) {
		list->first = frame;
	}
	else {
		list->last->next = frame;
	}

	if ( frame->frameType == FrameType_Screen ) {
		frame->screenNumber = list->screenCount;
		list->screenCount++;
	}

	list->last = frame;
	list->count++;
}

//----------------------------------------------------------
static bool cells_equal( int height, char ** x, char ** y ) {
	for ( int i = 0; i < height; i++ ) {
		if ( strcmp( x[i], y[i] ) != 0 ) return false;
	}

	return true;
}
//----------------------------------------------------------

/**
 *	Compares two frame lists for equality by comparing cells.
 *
 *	Cells are compared in the order they appear in the frame,
 *	which will work OK as long as the frames were generated
 *	from a window of the same dimensions by the save-screen
 *	function.
 *
 *	This should be fixed at some point, but for now it'll have
 *	to do.
 */

bool frames_equal( Frame * f1, Frame * f2 ) {
	if ( f1 == f2 ) return true;

	if ( f1 != NULL && f2 == NULL ) return false;

	if ( f2 != NULL && f1 == NULL ) return false;

	if ( f1->frameType != f2->frameType ) return false;

	if ( f1->frameType == FrameType_Char ) return f1->charCode == f2->charCode;

	return f1->width == f2->width
		&& f1->height == f2->height
		&& cells_equal( f1->height, f1->cells, f2->cells );
}

//----------------------------------------------------------

/**
 *	Dumps the contents of the designated frame to a stream.
 */

void print_frame( FILE * file, Frame * frame ) {
	if ( frame == NULL || file == NULL ) return;

	if ( frame->frameType == FrameType_Char ) {
		fprintf( file, "Char(%d,%f)\n", frame->charCode, frame->timeStamp );
	}
	else {
		fprintf( file, "Frame(%d,%d,%f)\n", frame->width, frame->height, frame->timeStamp );

		for ( int y = 0; y < frame->height; y++ ) {
			fprintf( file, "%s\n", frame->cells[y] );
		}

		fprintf( file, "EndFrame\n" );
	}
}

//----------------------------------------------------------

/**
 *	Parses a Frame from the supplied input stream.
 */

Frame * read_frame( FILE * file ) {
	if ( file == NULL ) {
		DUMP( ( "file == NULL" ) );
		return NULL;
	}

	char buffer[1000];
	char * text = fgets( buffer, sizeof( buffer ), file );

	if ( text == NULL ) {
		DUMP( ( "first line NULL" ) );
		return NULL;
	}

	char * s = strtok( text, "\n" );

	Frame * frame = NULL;

	StringList * tokens = split_string( s, "(,)" );
	StringNode * node = tokens->first;

	if ( tokens->count >= 3 && strcmp( node->text, "Char" ) == 0 ) {
		frame = create_frame();
		frame->frameType = FrameType_Char;
		node = node->next; frame->charCode = atoi( node->text );
		node = node->next; frame->timeStamp = atof( node->text );
	}
	else if ( tokens->count >= 4 && strcmp( tokens->first->text, "Frame" ) == 0 ) {
		frame = create_frame();
		frame->frameType = FrameType_Screen;
		node = node->next; frame->width = atoi( node->text );
		node = node->next; frame->height = atoi( node->text );
		node = node->next; frame->timeStamp = atof( node->text );
		frame->cells = calloc( frame->height, sizeof( char * ) );

		int frameIdx = 0;

		text = fgets( buffer, sizeof( buffer ), file );
		s = strtok( text, "\n" );

		while ( text != NULL && strncmp( text, "EndFrame", strlen( "EndFrame" ) ) != 0 ) {
			DUMP( ( "current line = '%s'", text ) );

			if ( frameIdx < frame->height ) {
				frame->cells[frameIdx++] = strdup( s );
			}

			text = fgets( buffer, sizeof( buffer ), file );
			s = strtok( text, "\n" );
		}
	}
	else {
		DUMP( ( "Expected Char or Frame declaration\n" ) );
		DUMP( ( "Input text: (((%s)))\n", text ) );
	}

	destroy_string_list( tokens );
	return frame;
}

//----------------------------------------------------------

/**
*	Parses a FrameList from the supplied input stream.
*/

FrameList * read_frame_list( FILE * file ) {
	FrameList * frame_list = create_frame_list();
	Frame * current = NULL;

	while ( ( current = read_frame( file ) ) != NULL ) {
		add_frame( current, frame_list );
	}

	return frame_list;
}

//----------------------------------------------------------

/**
 *	This function is not yet correctly implemented.
 */

FrameList * get_connected_parts( Frame * frame ) {
	assert( frame != NULL );

	if ( frame->height == 0 ) return NULL;

	FrameList * list = create_frame_list();

	//Frame *f = create_frame();
	//f->height = frame->height;
	//f->width = frame->width;

	//add_frame( f, list );

	//for ( Cell * cell = frame->cells->first; cell != NULL; cell = cell->next ) {
	//	if ( cell == frame->cells->first ) {
	//		add_cell( cell, f->cells );
	//		continue;
	//	}

	//	Frame * find_list_containing_adjacent_cell( Cell * cell, FrameList * list );

	//	// Frame * f = find_list_containing_adjacent_cell( cell, list );

	//	DUMP( ( "This function is not implemented yet!!!!" ) );
	//	exit( 1 );
	//}

	return list;
}

//----------------------------------------------------------

/**
 *	Compares two non-null FrameLists for equality, by comparing the parallel
 *	lists of Frames.
 *
 *	If all frames are equal, the return result is true, and the
 *	values referenced by x_mismatch and y_mismatch are not
 *	altered.
 *
 *	If any pair of frames are not equal, the return result is false.
 *	In that case, the variable referenced by x_mismatch will contain
 *	the first Frame in x that does not match the corresponding and y_mismatch are not
 *	altered.
 */

bool frame_lists_equal(
	FrameList * x,
	FrameList * y,
	Frame ** x_mismatch,
	Frame ** y_mismatch,
	int * frame_number,
	bool screens_only
	) {
	assert( x != NULL );
	assert( y != NULL );
	assert( x_mismatch != NULL );
	assert( y_mismatch != NULL );

	*x_mismatch = NULL;
	*y_mismatch = NULL;

	if ( x == y ) return true;

	DUMP( ( "Compare frame lists at %p and %p", x, y ) );

	Frame * x_frame = x->first; 
	Frame * y_frame = y->first;
	(*frame_number) = 0;

	Frame * x_latest_screen = NULL;
	Frame * y_latest_screen = NULL;

	while ( x_frame != NULL && y_frame != NULL ) {
		if ( x_frame->frameType == FrameType_Screen ) x_latest_screen = x_frame;
		if ( y_frame->frameType == FrameType_Screen ) y_latest_screen = y_frame;

		if ( !frames_equal( x_frame, y_frame ) ) {
			DUMP( ( "Frame mismatch found at %d", x_frame->timeStamp ) );
			*x_mismatch = screens_only && x_latest_screen ? x_latest_screen : x_frame;
			*y_mismatch = screens_only && y_latest_screen ? y_latest_screen : y_frame;
			return false;
		}

		x_frame = x_frame->next;
		y_frame = y_frame->next;
		(*frame_number)++;
	}

	DUMP( ( "Compare frames at %p and %p", x_frame, y_frame ) );

	if ( x_frame == NULL && y_frame == NULL ) {
		DUMP( ( "Appears to be the same number of frames" ) );
		return true;
	}
	else if ( x_frame ) {
		while ( x_frame && x_frame->frameType != FrameType_Screen ) x_frame = x_frame->next;
		if ( x_frame && x_frame->frameType == FrameType_Screen ) x_latest_screen = x_frame;
	}
	else if ( y_frame ) {
		while ( y_frame && y_frame->frameType != FrameType_Screen ) y_frame = y_frame->next;
		if ( y_frame && y_frame->frameType == FrameType_Screen ) y_latest_screen = y_frame;
	}

	DUMP( ( "Returning FALSE." ) );

	*x_mismatch = screens_only && x_latest_screen ? x_latest_screen : x_frame;
	*y_mismatch = screens_only && y_latest_screen ? y_latest_screen : y_frame;
	return false;
}

//----------------------------------------------------------

/**
*	Dumps the contents of the designated frame list to a stream.
*/

void print_frame_list( FILE * file, FrameList * list ) {
	for ( Frame * f = list->first; f != NULL; f = f->next ) {
		print_frame( file, f );
	}
}

//----------------------------------------------------------

void destroy_frame( Frame * frame ) {
	if ( frame == NULL ) return;

	if ( frame->frameType == FrameType_Screen ) {
		for ( int i = 0; i < frame->height; i++ ) {
			free( frame->cells[i] );
		}
	}

	free( frame );
}

void destroy_frame_list( FrameList * list ) {
	if ( list == NULL ) return;

	Frame * nextFrame = list->first;

	while ( nextFrame ) {
		Frame * current = nextFrame;
		nextFrame = nextFrame->next;
		destroy_frame( current );
	}

	free( list );
}
