#pragma once 

#include <stdbool.h>
#include <stdlib.h>
#include "function_types.h"

#include "frames.h"

typedef struct pFrameVector {
	size_t capacity;
	size_t length;
	pFrame * data;
	FunctionVar( clone, void )( pFrame *dest, pFrame *source );
	FunctionVar( destructor, void ) ( pFrame *item );
} pFrameVector;

typedef pFrameVector *ppFrameVector;

typedef struct cpFrameVector {
	/* Allocate and initialise a new vector. */
	FunctionVar( Create, ppFrameVector ) (
		size_t capacity,
		FunctionVar( clone, void )( pFrame *dest, pFrame *source ),
		FunctionVar( destructor, void ) ( pFrame *item )
		);

	/* Free the dynamic array and the vector itself (must have been initialised via Create). */
	FunctionVar( Destroy, void ) ( ppFrameVector this );

	/* Initialise a pre-existing vector. */
	FunctionVar( Initialise, bool ) (
		ppFrameVector this,
		size_t capacity,
		FunctionVar( clone, void )( pFrame *dest, pFrame *source ),
		FunctionVar( destructor, void ) ( pFrame *item )
		);

	/* Release the storage used by dynamic array without deallocating a pre-existing vector. */
	FunctionVar( Finalise, void ) (
		ppFrameVector this
		);

	/* Append a new item to the vector. */
	FunctionVar( Add, bool ) (
		ppFrameVector this,
		pFrame item
		);

	/* Insert a new item into the vector at the designated position. */
	FunctionVar( Insert, bool ) (
		ppFrameVector this,
		pFrame item,
		size_t pos
		);

	/* Reallocate the dynamic array to a new size. */
	FunctionVar( Resize, bool ) (
		ppFrameVector this,
		size_t new_size
		);

	/* Delete the designated item from the vector. If the destructor is not null it will be applied to the removed item prior to removal. */
	FunctionVar( Remove, void ) (
		ppFrameVector this,
		size_t pos
		);

	/* Traverse the items in the vector. */
	FunctionVar( Foreach, void ) (
		ppFrameVector this,
		FunctionVar( func, bool )( pFrame *item, Pointer data ),
		Pointer data
		);

	/* Reset the size of the vector back to zero. If the destructor is non-null, it will be invoked on each item. */
	FunctionVar( Clear, void ) (
		ppFrameVector this
		);

	/* Appends a list of items to the current vector, using a shallow copy (if clone is null) or by calling clone for each item. */
	FunctionVar( AddRange, void ) (
		ppFrameVector this,
		const pFrame * newItems,
		size_t n
		);

	/* Sets the item at the specified location to a new value. */
	FunctionVar( Set, void ) (
		ppFrameVector this,
		size_t pos,
		pFrame value
		);

	/* Gets the item at the specified location. */
	FunctionVar( Get, pFrame ) (
		ppFrameVector this,
		size_t pos
		);

} cpFrameVector;

extern cpFrameVector tpFrameVector;
