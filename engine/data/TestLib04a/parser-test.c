
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include "scanner.h"
#include "parser.h"

#undef DB
#include "db.h"

void test( void );

void test_01_include_directives( void ) {
	printf( "test_01: include directives" "\n" );

	char * infile =
		"#include <stdio.h>"     "\n"
		"#include <stddef.h>"    "\n"
		"#include \"types.h\""   "\n"
		"#include \"scanner.h\"" "\n"
		;

	char * actual;
	size_t actual_len;

#pragma region Redirect stdio and run test.
	FILE * old_stdin = stdin;
	FILE * old_stdout = stdout;
	stdin = fmemopen( infile, strlen( infile ), "r" );
	stdout = open_memstream( &actual, &actual_len );

	pScanner s = tScanner.NewScanner( "stdin", stdin );
	Parser p;
	tParser.Initialise( &p, s, "stdin" );
	tParser.ParseFile( &p );
	tScanner.FreeScanner( s );
	tSymbol.Print( stdout, &p.document_scope.symtab.base );

	fclose( stdout );
	stdout = old_stdout;
	stdin = old_stdin;
#pragma endregion

	char * expected =
		"<source_file>"
		"<name>stdin</name>"
		"<content>"
		"<include>&lt;stdio.h&gt;</include>"
		"<include>&lt;stddef.h&gt;</include>"
		"<include>\"types.h\"</include>"
		"<include>\"scanner.h\"</include>"
		"</content>"
		"</source_file>"
		;

	if ( strcmp( expected, actual ) ) {
		printf( "\tFailed\n" );
		printf( "\texpected = (((%s)))\n", expected );
		printf( "\tactual = (((%s)))\n", actual );
	}
	else {
		printf( "\tPassed\n" );
	}
	free( actual );
}

int	main( int argc, char **argv ) {
	Action tests[] = {
		test_01_include_directives,
		NULL
	};

	for ( int i = 0; tests[i]; i++ ) {
		Action test = tests[i];
		test();
	}

	return 0;
}
