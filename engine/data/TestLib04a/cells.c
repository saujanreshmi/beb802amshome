#include <stdlib.h>
#include <assert.h>
#include "cells.h"

#include "db.h"

//----------------------------------------------------------

Cell * create_cell( int x, int y, char ch ) {
	Cell * cell = (Cell *)calloc( 1, sizeof( Cell ) );
	cell->ch = ch;
	cell->x = x;
	cell->y = y;
	return cell;
}

//----------------------------------------------------------

CellList * create_cell_list() {
	return (CellList *)calloc( 1, sizeof( CellList ) );
}

//----------------------------------------------------------

void init_cell_list( CellList * list ) {
	assert( list != NULL );
	list->first = list->last = NULL;
}

//----------------------------------------------------------

void add_cell( Cell * cell, CellList * list ) {
	assert( cell != NULL );
	assert( list != NULL );

	if ( list->first == NULL ) {
		list->first = cell;
	}
	else {
		list->last->next = cell;
	}

	list->last = cell;
	list->count++;
}

//----------------------------------------------------------

bool cells_equal( Cell * c1, Cell * c2 ) {
	assert( c1 != NULL );
	assert( c2 != NULL );

	DUMP( ( "Comparing (%d,%d,%d) with (%d,%d,%d)", c1->x, c1->y, c1->ch, c2->x, c2->y, c2->ch ) );
	DUMP( ( "(c1->ch == c2->ch) = %d", ( c1->ch == c2->ch ) ) );
	DUMP( ( "(c1->x == c2->x) = %d", ( c1->x == c2->x ) ) );
	DUMP( ( "(c1->y == c2->y) = %d", ( c1->y == c2->y ) ) );

	return (c1->ch == c2->ch) && (c1->x == c2->x) && (c1->y == c2->y);
}

//----------------------------------------------------------

/**
 *	Compares two cells and returns
 */

bool cell_lists_equal( CellList * list1, CellList * list2 ) {
	assert( list1 != NULL );
	assert( list2 != NULL );

	Cell * c1 = list1->first;
	Cell * c2 = list2->first;

	while ( c1 != NULL && c2 != NULL ) {
		DUMP( ( "Comparing (%d,%d,%d) with (%d,%d,%d)", c1->x, c1->y, c1->ch, c2->x, c2->y, c2->ch ) );

		if ( !cells_equal( c1, c2 ) ) {
			DUMP( ( "Mismatch found." ) );
			return false;
		}
		c1 = c1->next;
		c2 = c2->next;
	}

	DUMP( ( "Returning %d.", c1 == NULL && c2 == NULL ) );
	return ( c1 == NULL && c2 == NULL );
}

//----------------------------------------------------------

void print_cells( FILE * file, CellList * cells ) {
	assert( file != NULL );
	assert( cells != NULL );

	Cell * current_cell = cells->first;

	while ( current_cell != NULL ) {
		fprintf( file, "%d,%d,%d\n", current_cell->x, current_cell->y, (int)current_cell->ch );
		current_cell = current_cell->next;
	}
}

//----------------------------------------------------------

int count_cells( CellList * cells ) {
	assert( cells != NULL );

	Cell * current_cell = cells->first;
	int n = 0;

	while ( current_cell != NULL ) {
		n = n + 1;
		current_cell = current_cell->next;
	}

	return n;
}

//----------------------------------------------------------

CellList * select_cells( CellList * c1, CellPredicate predicate, void * args ) {
	CellList * list = create_cell_list();

	for ( Cell * c = c1->first; c != NULL; c = c->next ) {
		if ( predicate( c, args ) ) {
			Cell* clone = create_cell( c->x, c->y, c->ch );
			add_cell( clone, list );
		}
	}

	return list;
}

//----------------------------------------------------------

void destroy_cell_list( CellList * cells ) {
	for ( Cell * c = cells->first; c != NULL; ) {
		Cell * next = c->next;
		free( c );
		c = next;
	}

	free( cells );
}