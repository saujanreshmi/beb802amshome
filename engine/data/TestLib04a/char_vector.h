#pragma once 

#include <stdbool.h>
#include <stdlib.h>
#include "function_types.h"

typedef char Char;

typedef struct CharVector {
	size_t capacity;
	size_t length;
	Char * data;
	FunctionVar( clone, void )( Char *dest, Char *source );
	FunctionVar( destructor, void ) ( Char *item );
} CharVector;

typedef CharVector *pCharVector;

typedef struct cCharVector {
	/* Allocate and initialise a new vector. */
	FunctionVar( Create, pCharVector ) (
		size_t capacity,
		FunctionVar( clone, void )( Char *dest, Char *source ),
		FunctionVar( destructor, void ) ( Char *item )
		);

	/* Free the dynamic array and the vector itself (must have been initialised via Create). */
	FunctionVar( Destroy, void ) ( pCharVector this );

	/* Initialise a pre-existing vector. */
	FunctionVar( Initialise, bool ) (
		pCharVector this,
		size_t capacity,
		FunctionVar( clone, void )( Char *dest, Char *source ),
		FunctionVar( destructor, void ) ( Char *item )
		);

	/* Release the storage used by dynamic array without deallocating a pre-existing vector. */
	FunctionVar( Finalise, void ) (
		pCharVector this
		);

	/* Append a new item to the vector. */
	FunctionVar( Add, bool ) (
		pCharVector this,
		Char item
		);

	/* Insert a new item into the vector at the designated position. */
	FunctionVar( Insert, bool ) (
		pCharVector this,
		Char item,
		size_t pos
		);

	/* Reallocate the dynamic array to a new size. */
	FunctionVar( Resize, bool ) (
		pCharVector this,
		size_t new_size
		);

	/* Delete the designated item from the vector. If the destructor is not null it will be applied to the removed item prior to removal. */
	FunctionVar( Remove, void ) (
		pCharVector this,
		size_t pos
		);

	/* Traverse the items in the vector. */
	FunctionVar( Foreach, void ) (
		pCharVector this,
		FunctionVar( func, bool )( Char *item, Pointer data ),
		Pointer data
		);

	/* Reset the size of the vector back to zero. If the destructor is non-null, it will be invoked on each item. */
	FunctionVar( Clear, void ) (
		pCharVector this
		);

	/* Appends a list of items to the current vector, using a shallow copy (if clone is null) or by calling clone for each item. */
	FunctionVar( AddRange, void ) (
		pCharVector this,
		const Char * newItems,
		size_t n
		);

	/* Sets the item at the specified location to a new value. */
	FunctionVar( Set, void ) (
		pCharVector this,
		size_t pos,
		Char value
		);

	/* Gets the item at the specified location. */
	FunctionVar( Get, Char ) (
		pCharVector this,
		size_t pos
		);

} cCharVector;

extern cCharVector tCharVector;
