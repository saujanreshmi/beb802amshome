/*
|+-------------------------------------------------------------------------
||	File:	utility.c
||	Author: Lawrence Buckingham
||
||	Miscellaneous useful procedures.
|+-------------------------------------------------------------------------
*/
#include "utility.h"
#include "db.h"

#ifdef TRACK_MEMORY

/*----------------------------------------------------------------------*/
Initialised_List( Registry, NULL /* no destructor */ );
#define PARA	('p' << 24 | 'a' << 16 | 'r' << 8 | 'a')
#define NOID	('n' << 24 | 'o' << 16 | 'i' << 8 | 'd')
/*----------------------------------------------------------------------*/
void	Register( void *object, pPlug plug, char *type, char *file, int line)
	/*
	||	Add the object to the registration list.
	*/
{	assert( object != NULL ); assert( plug != NULL );
{
	InitNode( &plug->node, Embedded, plug );
	plug->type = type;
	plug->file = file;
	plug->line = line;
	plug->para = PARA;  
	plug->noid = NOID;  
	plug->object = object;
	AddLast( &plug->node, &Registry );
}}
/*----------------------------------------------------------------------*/
void	Deregister_( pPlug plug, char *file, int line )
	/*
	||	Remove the link from the list.
	*/
{	
	IF plug->para != PARA THEN
		printf( "%s(%d): memory violation: PARA\n", file, line );
		abort();
	ENDIF
	IF plug->noid != NOID THEN
		printf( "%s(%d): memory violation: NOID\n", file, line );
		abort();
	ENDIF
	RemoveNode( &plug->node );
	plug->para = 0;
	plug->noid = 0;
}
/*----------------------------------------------------------------------*/
void	Display_Register( FILE *f )
	/*
	||	Print a list of plugs to the specified file.
	*/
{
	pNode	n = First( &Registry );

	fprintf( f, "Currently registered objects:\n" );
	while( n != NULL ){
		pPlug p = n->container;
		assert( p->para == PARA );
		assert( p->noid == NOID );
		fprintf( f, "\t( %s, %s, %d, %p )\n", p->type, p->file, p->line, p->object );
		n = Right( n );
	}
}
/*----------------------------------------------------------------------*/
void	Display_Register_From( FILE *f, pPlug marker )
	/*
	||	Print a list of plugs to the specified file.
	*/
{
	pNode	n = &marker->node;

	fprintf( f, "Currently registered objects from marker:\n" );
	while( n != NULL ){
		pPlug p = n->container;
		assert( p->para == PARA );
		assert( p->noid == NOID );
		fprintf( f, "\t( %s, %s, %d, %p )\n", p->type, p->file, p->line, p->object );
		n = Right( n );
	}
}
/*----------------------------------------------------------------------*/
void	Check_Register_( char *file, int line )
	/*
	||	Check list of plugs to the specified file.
	*/
{
	pNode	n = First( &Registry );

	while( n != NULL ){
		pPlug p = n->container;
		IF p->para != PARA THEN
			printf( "%s(%d): PARA check fails in Check_Register(): %d\n",
				file, line, p->para
			 );
			 abort();
		ENDIF
		IF p->noid != NOID THEN
			printf( "%s(%d): NOID check fails in Check_Register(): %d\n",
				file, line, p->noid
			 );
			 abort();
		ENDIF
		n = Right( n );
	}
}
#endif
/*----------------------------------------------------------------------*/
void	Print_Availability( FILE *f, tAvailability a )
	/*
	||	Display a value of type tAvailability.
	*/
{
	static pString name[] = { 
		"Unavailable", 
		"Train", 
		"Test" 
	};
	fprintf( f, name[a] );
}
/*----------------------------------------------------------------------*/
void	Generate_X_Fold( pIntTable x_fold, const pPartition part, int folds )
	/*
	||	Repartition states labelled Train in `part' into a set of `folds'
	||	cross-validation sets, in x_fold.  The new values are in the range 
	||	1 .. folds, with zero indicating Unavailable points.
	*/
{
	int	fold, 
		remaining;
	double 	r;
	
	NewTable( *x_fold, part->size );
	x_fold->size = part->size;

	remaining = 0;
	TraverseTable( *part, i,
		IF part->value[i] == aTrain THEN remaining ++; ENDIF 
	)
	r = (double)( remaining ) / folds;
		
	FOR fold = 1; fold <= folds; fold ++ DO
		Selector s;
		int	wanted = (int)( fold * r + 0.5 ) - (int)( ( fold - 1 ) * r + 0.5 ); 
		s.numberWanted = wanted;
		s.remaining = remaining;
		TraverseTable( *part, i,
			IF x_fold->value[i] == 0 
			&& part->value[i] == aTrain 
			&& Select( &s ) THEN
				x_fold->value[i] = fold;
			ENDIF
		)
		remaining -= wanted;
	ENDFOR
}
/*----------------------------------------------------------------------*/
void	Select_Partition( pPartition part, const pIntTable x_fold, int fold )
	/*
	||	Mark points in subset #fold as Test, and other non-zero
	||	ones as Train.
	*/
{
	InitTable( *part, x_fold->size );
	part->size = x_fold->size;
	TraverseTable( *x_fold, i,
		int t = x_fold->value[i];
		IF t == 0 THEN
			part->value[i] = aUnavailable;
		ELIF t == fold THEN
			part->value[i] = aTest;
		ELSE
			part->value[i] = aTrain;
		ENDIF
	) 
}
/*-----------------------------------------------------------------------*/
void	Initialise_Partition( pPartition part, int size )
	/*
	||	Create a "partition, with all points marked for training.
	||	(This will be fed to Generate_X_Fold later.)
	*/
{
	NewTable( *part, size );
	part->size = size;
	TraverseTable( *part, i, part->value[i] = aTrain );
}
/*-----------------------------------------------------------------------*/
int	Execute( pString format, ... )
{
	va_list	parms;
	BigString s;
	
	va_start( parms, format );
	vsprintf( s, format, parms );
	va_end( parms );
	
	return system( s );
}
/*-----------------------------------------------------------*/		
void Generate_Permutation( pPermutation perm, uint N )
	/*
	||	Fills a table with a random permutation of the
	||	numbers in 0 .. N-1.
	*/
{
	uint 	i;
	
	NewTable( *perm, N );
	perm->size = N;

	FOR i = 0; i < N; i++ DO
		perm->value[i] = i;
	ENDFOR
	
	FOR i = 0; i < N-1; i++ DO
		int	j = 1 + (int)( Random(1.0) * (N-1-i) );
		swap( perm->value[i], perm->value[i+j], int );
	ENDFOR
}
/*-----------------------------------------------------------*/		
void	Progress( pString s, int upto, int outof )
{
	int 	allowed = 78 - strlen(s);
	int	print = ( allowed * ( upto * outof + outof / 2 ) ) / ( outof * outof );
	int	i;
	char	buffer[256], *b = buffer;
	
	b += sprintf( buffer, "\r%s: [", s );
	for( i = 0; i < allowed; i++ )
	{
		if( i < print )
			b += sprintf( b, "%c", '=' );
		else
			b += sprintf( b, " " );
	}
	b += sprintf( b, "]" );
	fprintf( stderr, buffer );
	fflush( stderr );
}
/*-----------------------------------------------------------*/		
double Quadratic_Decay( 
			double	initial_learning_rate,
			uint	epoch,
			uint	maximum_epochs
		)
	/*
	||	Reduce the learing rate asymptotically to 0,
	||	using a quadratic decay.
	*/
{
	double	x = epoch, 
		i = initial_learning_rate,
		m = maximum_epochs, 
		d = i * ( 1 + x * ( x - 2 * m ) / ( m * m ) );
	DUMP(( "epoch %6d, d = %lf\n", epoch, d ))
	return d;
}
/*-----------------------------------------------------------*/		
double Linear_Decay( 
			double	initial_learning_rate,
			uint	epoch,
			uint	maximum_epochs
		)
	/*
	||	Reduce the learing rate asymptotically to 0,
	||	using a quadratic decay.
	*/
{
	double	x = epoch, 
		i = initial_learning_rate,
		m = maximum_epochs, 
		d = i * ( 1 - x / m );
	DUMP(( "epoch %6d, d = %lf\n", epoch, d ))
	return d;
}
/*-----------------------------------------------------------------------*/
void	Create_Parmfile( pString temp, int argc, pString argv[] )
	/*
	||	Create a text file holding the command line arguments.
	||	This file can then be associated with a scanner, for easy
	||	sophisticated command line parsing.
	||	PRE:	. temp holds the name of the file to create.
	||	POST:	. the command line parameters are copied to the file.
	*/
{
	FILE 	*f = fopen( temp, "wa" );
	int	i, err;
	
	programName = argv[0];
	
	IF f == NULL THEN
		fprintf( stderr, "Unable to create temporary file %s.\n", temp );
		abort();
	ENDIF
	
	FOR i = 1; i < argc; i++ DO
		fprintf( f, "%s ", argv[i] );
		IF ( err = ferror( f ) ) != 0 THEN
			fclose( f );
			fprintf( stderr, "Error %d writing to file %s\n", err, temp );
			abort();
		ENDIF
	ENDFOR 
	fclose( f );
}
/*----------------------------------------------------------------------*/
pString	programName = "anonymous program";
/*----------------------------------------------------------------------*/
static int Random_Seed = 1;
#define RAND_MOD (16807L)
#undef RAND_MAX
#define RAND_MAX (2147483647L)
#define RAND_QUOT (127773L)
#define RAND_REM (2836L)

void	Randomize( int seed )
	/*
	||	Prepare the random number generator.
	*/
{
	 Random_Seed = ( seed < 0 ? - seed : seed );
}
	
/*----------------------------------------------------------------------*/
long int Rand( void )
	/*
	**	Based on the code in Wirths Oberon book.
	*/
{
	long int gamma;
	
	if( Random_Seed == 0 ) Random_Seed = 1234567;
	if( Random_Seed == RAND_MAX ) Random_Seed = 9876543;
	
	gamma = RAND_MOD * ( Random_Seed % RAND_QUOT ) -
			 RAND_REM * ( Random_Seed / RAND_QUOT );

	Random_Seed = (gamma > 0) ? gamma : gamma + RAND_MAX;

	return Random_Seed;
}
/*----------------------------------------------------------------------*/
double	Random( double upperLimit )
	/*
	||	Generate a uniform random number in [ 0, upperLimit ).
	*/
{
	return upperLimit * ( (double)Rand() / RAND_MAX );
}
	
/*----------------------------------------------------------------------*/
double Gaussian( double mean, double standard_deviation )
{
	double x, p;
	
	FOR ;; DO
		x = Random( 100.0 ) - 50.0;
		p = Random( 1.0 );
		IF p < exp( - x * x / 2 ) THEN
			return x * standard_deviation + mean;
		ENDIF
	ENDFOR
}
/*----------------------------------------------------------------------*/
char *TimeStr( void )
	/*
	||	Gets pointer to data area containig a string with the
	||	current date-time in it.
	*/
{
	time_t	t;
	pString s;
	
	time( &t );
	s = ctime( &t );
	s[ 24 ] = 0;
	return s;
}
/*----------------------------------------------------------------------*/
bool	Select( pSelector s )
	/*
	||	Used to select a uniform sample of say, 5 out of 100.
	||	Make a pass thru the data set, with numberWanted initialised 
	||	to the number you want; it is counted down as cases are 
	||	selected. 
	*/
{
	IF s->numberWanted <= 0 OR s->remaining <= 0 THEN
		return false;
	ELSE
		int roll = (int)( Random(1.0) * s->remaining );
	
		s->remaining --;
	
		IF roll < s->numberWanted THEN
			s->numberWanted --;
			return true;
		ENDIF
		return false;
	ENDIF
}
/*------------------------------------------------------------------------*/
void Do_Help( pString help[] )
	/*
	||	Echoes the set of strings pointed at by help to stderr
	||	and kills the run.
	*/ 
{
	/* Index into help strings: */
		int i;

	/* Print each string: */
		FOR i = 0; help[i] != NULL; i++ DO
			fprintf( stderr, "%s\n", help[i] );
		ENDFOR		
		
	/* And kill the run: */
		exit( EXIT_FAILURE );
}
/*------------------------------------------------------------------------*/
static int     compare_double( void const *x, void const *y )
/*
	||	comparison function for qsort.
	*/
{
	const double *xx = x, *yy = y;
	
	IF *xx < *yy THEN
		return -1;
	ELIF *xx > *yy THEN
		return +1;
	ELSE
		return 0;
	ENDIF  
}
/*----------------------------------------------------------------------*/ 
void	Sort_Double( double *buffer, size_t size )
	/*
	||	Use qsort to sort a buffer of doubles.
	*/
{
	qsort( buffer, size, sizeof( double ), compare_double );
}	
/*----------------------------------------------------------------------*/
#ifdef MAC
char * strdup( char * s ){
	char * t = malloc( strlen( s ) + 1 );
	if( t != NULL )
		strcpy( t, s );
	return t;
}
#endif
/*----------------------------------------------------------------------*/
int	Printf_Immediate( const char * format, ... )
	/*
	||	Prints the specified arguments and then flusshes standard output.
	*/
{
	int	printf_result;
	va_list	args;
	
	va_start( args, format );
	printf_result = vprintf( format, args );
	va_end( args );
	
	return printf_result;
}
/*----------------------------------------------------------------------*/
int	FPrintf_Immediate( FILE *f, const char * format, ... )
	/*
	||	Prints the specified arguments and then flusshes standard output.
	*/
{
	int	printf_result;
	va_list	args;
	
	va_start( args, format );
	printf_result = vfprintf( f, format, args );
	va_end( args );
	
	return printf_result;
}
/*---------------------------------------------------------------------*/
