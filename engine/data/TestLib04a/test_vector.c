#include <stdio.h>

#include "function_types.h"
#include "int_vector.h"
#include "string_util.h"

static Function( Test01, void ) ( void );

Action vector_tests[] = {
	Test01,
	NULL
};

static Function( Print, bool )( int *item, Pointer data );
static Function( PrintList, void )( String prefix, pIntVector list, String suffix );

static Function( Test01, void ) ( void ) {
	pIntVector list = tIntVector.Create( 0, NULL, NULL );

	for ( int i = 0; i < 100; i++ ) {
		tIntVector.Add( list, i );
	}

	PrintList( "List as initialised  : ", list, "\n" );

	tIntVector.Remove( list, 0 );
	PrintList( "Remove first element : ", list, "\n" );

	tIntVector.Remove( list, list->length - 1 );
	PrintList( "Remove last element  : ", list, "\n" );

	tIntVector.Remove( list, list->length / 2 );
	PrintList( "Remove middle element: ", list, "\n" );
}


static Function( PrintList, void )( String prefix, pIntVector list, String suffix ) {
	bool isFirst = true;
	printf( prefix );
	tIntVector.Foreach( list, Print, &isFirst );
	printf( suffix );
}

static Function( Print, bool )( int *item, Pointer data ) {
	bool *is_first = data;

	if ( !( *is_first ) ) {
		printf( ", " );
	}
	else {
		*is_first = false;
	}

	printf( "%d", (*item) );
	return true;
}
