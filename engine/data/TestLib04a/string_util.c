#include <assert.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "db.h"
#include "string_util.h"

/**
*	Gets the next token from the specified string. Similar to strtok
*	but not so destructive.
*
*	Advances the pointer referenced by cursor until either a terminating
*	zero or one of the delimiters is encountered.
*
*	The characters traversed up to but not including the delimiter are
*	concatenated to create a new, dynamically allocated string, which
*	becomes the return result. This is constructed via strdup, and so
*	it should be freed later.
*
*	On termination, the pointer referenced by cursor is pointing at
*	the character that caused the scan to stop. You can dereference
*	that pointer to to determine which celimiter was used to terminate
*	the scan.
*
*	Once a zero terminator is encountered, the scan will permanently stop,
*	and a NULL string is returned.
*/

static Function( GetToken, String ) ( String *cursor, String delimiters ) {
	assert( cursor != NULL );
	assert( delimiters != NULL );

	char * str = *cursor;
	int len = 0;
	DUMP( ( "len = %d; str[len] = %d", len, (int)str[len] ) );

	if ( str[len] == 0 ) {
		DUMP( ( "End of string found." ) );
		return NULL;
	}


	while ( ( str[len] != '\0' ) && ( NULL == strchr( delimiters, str[len] ) ) ) {
		DUMP( ( "** len = %d; str[len] = '%c'", len, str[len] ) );
		len++;
	}

	DUMP( ( "** len = %d; str[len] = %d", len, (int)str[len] ) );

	char * token = (char *)malloc( len + 1 );

	for ( size_t i = 0; i < len; i++ ) {
		token[i] = str[i];
	}

	token[len] = '\0';

	*cursor = str[len] == '\0' ? &str[len] : &str[len + 1];

	DUMP( ( "get_token result = %s", token ) );
	return token;
}

//---------------------------------------------------------------

/**
 *	Compares two strings in a case-insensitive manner, return results that
 *	are otherwise consistent with strcmp;
 */

static Function( CompareInsensitive, int ) ( const String s, const String t ) {
	assert( s != NULL && t != NULL );

	int i = 0;

	for ( ; s[i] && t[i]; i++ ) {
		int diff = tolower( (unsigned char) s[i] ) - tolower( (unsigned char) t[i] );

		if ( diff != 0 ) {
			return diff;
		}
	}

	return s[i] - t[i];
}

StringUtilClass tStringUtil = { GetToken, CompareInsensitive };
