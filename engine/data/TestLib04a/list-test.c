/*
|+----------------------------------------------------------------------
||	File:	test_lists.c
||	Author:	Lawrence Buckingham
||
||	Give the lists unit a run-around.
||	Reads in a set of integers from stdin, then does some things.
|+----------------------------------------------------------------------
*/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "lists.h"

void	LoadData( pList l, int n );
void	PrintList( pList l );
void	PrintListReverse( pList l );
void	DeleteElement( pList l, int n );
void	DeleteElements( pList l );
void	DeleteData( pList l );

typedef struct Value {
	Node	link;
	int	value;
} Value;

typedef Value *pValue;

Initialised_List( testList, DestroyEmbedded );

int main( int argc, char **argv ) {
	if ( argc < 2 ) {
		fprintf( stderr, "Usage: list-test N, where N is the number of random numeric list items to be generated.\n" );
		exit( 1 );
	}

	int n;
	int items_scanned = sscanf( argv[1], "%d", &n );

	if ( items_scanned < 1 ) {
		fprintf( stderr, "Usage: list-test N, where N is the number of random numeric list items to be generated.\n" );
		fprintf( stderr, "Expecting one numeric argument.\n" );
		exit( 1 );
	}

	srand( time( NULL ) );

	LoadData( &testList, n );
	PrintList( &testList );
	PrintListReverse( &testList );

	DeleteElements( &testList );
	PrintList( &testList );
	PrintListReverse( &testList );

	DeleteData( &testList );
	return 0;
}

void LoadData( pList l, int n ) {
	for ( int i = 0; i < n; i++ ) {
		pValue	n;
		NewNodeInList( n, l );
		n->value = rand() % 1000;
	}
}

typedef struct GlobalData {
	int	counter;
} GlobalData;

typedef GlobalData *pGlobalData;

bool NodePrinter( pNode n, SafePointer dataBlock ) {
	pValue		v = n->container;
	pGlobalData	g = dataBlock;

	g->counter++;
	printf( "%d ", v->value );
	return true;
}

void	PrintList( pList l ) {
	GlobalData g;

	g.counter = 0;
	printf( "Contents of the list are:\n" );
	TraverseList( l, NodePrinter, &g );
	printf( "\n\nList has %d elements\n\n", g.counter );
}

void	PrintListReverse( pList l ) {
	GlobalData g;
	pNode	   n;

	g.counter = 0;
	printf( "Contents of the list in reverse are:\n" );
	for ( n = Last( l ); n != NULL; n = Left( n ) )
		NodePrinter( n, &g );
	printf( "\n\nList has %d elements\n\n", g.counter );

}

void	DeleteElement( pList l, int n ) {
	pNode node = Element( n, l );

	if ( node != NULL ) {
		pValue v = node->container;
		RemoveNode( node );
		FreeMem( v );
	}
}

void	DeleteElements( pList l ) {
	int n = rand() % ( 3 * l->size / 4 );
	printf( "Deleting %d elements...\n", n );

	for ( int k = 0; k < n; k++ ) {
		int i = rand() % l->size + 1;
		DeleteElement( l, i );
	}
}

void	DeleteData( pList l ) {
	DestroyList( l );
}



