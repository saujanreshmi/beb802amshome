#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "stdio_helpers.h"
#include "cab202_graphics.h"

//----------------------------------------------------------

char * read_all_text( char * file_name ) {
	void * f = fopen( file_name, "r" );
	fseek( f, 0, SEEK_END );
	long flen = ftell( f );
	fseek( f, 0, SEEK_SET );
	char * buffer = malloc( ( flen + 1 ) * sizeof( char ) );
	fread( buffer, 1, flen, f );
	fclose( f );
	buffer[flen] = (char) 0;
	return buffer;
}

//----------------------------------------------------------

size_t write_all_text( char * file_name, char * text ) {
	void * f = fopen( file_name, "w" );
	size_t chars_written = fwrite( text, sizeof( char ), strlen( text ), f );
	fclose( f );
	return chars_written;
}

//----------------------------------------------------------

void execute_stdout_test( char ** dest, size_t * dest_len, Action action ) {
	FILE * old_stdout = stdout;
	char *d;
	size_t l;

	stdout = open_memstream( &d, &l );
	action();
	fclose( stdout );

	*dest = d;
	*dest_len = l;

	stdout = old_stdout;
}

//----------------------------------------------------------

void execute_stdinout_test(
	char    * stdin_text,
	char   ** dest,
	size_t  * dest_len,
	Action    action
	) {

	FILE * old_stdin = stdin;
	stdin = fmemopen( stdin_text, strlen( stdin_text ), "r" );
	execute_stdout_test( dest, dest_len, action );
	stdin = old_stdin;
}

//----------------------------------------------------------

FrameList * execute_simple_zdk_test( Action action ) {
	char * capture;
	size_t capture_len;
	zdk_save_stream = open_memstream( &capture, &capture_len );

	setup_screen();
	action();
	cleanup_screen();

	// Read the saved frames from capture buffer.
	FILE * file = fmemopen( capture, capture_len, "r" );
	FrameList * frame_list = read_frame_list( file );
	free( capture );

	return frame_list;
}

//----------------------------------------------------------

FrameList * execute_stdin_zdk_test( char * stdin_text, Action action ) {
	zdk_input_stream = fmemopen( stdin_text, strlen( stdin_text ), "r" );
	FrameList * result = execute_simple_zdk_test( action );
	zdk_input_stream = NULL;
	return result;
}

//----------------------------------------------------------

FrameList * execute_parameterised_zdk_test(
	char * stdin_text,
	ParameterisedAction action,
	void * params
	) {
	zdk_input_stream = fmemopen( stdin_text, strlen( stdin_text ), "r" );

	char * capture;
	size_t capture_len;
	zdk_save_stream = open_memstream( &capture, &capture_len );

	setup_screen();
	action( params );
	cleanup_screen();

	// Read the saved frames from capture buffer.
	FILE * file = fmemopen( capture, capture_len, "r" );
	FrameList * frame_list = read_frame_list( file );

	free( capture );

	return frame_list;
}

//----------------------------------------------------------

