#ifndef __TABLE_H__
#define __TABLE_H__

#include "utility.h"

#define TableOf(t) struct { int capacity, size, magic; t *value; void (*dtor)( t x ); tPlug plug; }

#define SetOf TableOf

#define NewTable( t, cap ) { ClearTable(t); InitTable(t,cap); }

#define ClearTable( t ) { (t).capacity = (t).size = (t).magic = 0; (t).value = NULL; (t).dtor = NULL; }						

#define Table_Valid( t ) ((t).magic == 5198983)

#define InitTable( t, cap ) { 						\
	assert( ! Table_Valid( t ) );					\
	(t).capacity = cap; 						\
	(t).size = 0;			 		 		\
	(t).magic = 5198983;						\
	(t).dtor = NULL;						\
	(t).value = calloc( (t).capacity, sizeof( (t).value[0] ) );	\
	assert( (t).value != NULL );					\
}  	

#define InitTableD( t, cap, destructor ) { 				\
	assert( ! Table_Valid( t ) );					\
	(t).capacity = cap; 						\
	(t).size = 0;			 		 		\
	(t).magic = 5198983;						\
	(t).dtor = destructor;						\
	(t).value = calloc( (t).capacity, sizeof( (t).value[0] ) );	\
	assert( (t).value != NULL );					\
}  	

#define Size( t ) (t).size

#define CopyTable( t2, t1 ) {	 					\
	int __i__;							\
	assert( Table_Valid(t1) );					\
	assert( Table_Valid(t2) );					\
	(t2).size = 0;		 		 			\
	for( __i__ = 0; __i__ < (t1).size; __i__++ )			\
		AppendTable( (t2), (t1).value[__i__] );			\
}  	

#define DestroyTable(t) {						\
	assert( Table_Valid(t) );					\
	if((t).dtor){ 							\
		TraverseTable((t),__i__,(t).dtor((t).value[__i__])); }	\
	free( (t).value );						\
	ClearTable( t ); }
 
#define AppendTable( t, v )					\
{	assert( Table_Valid(t) );				\
	if( (t).size >= (t).capacity )				\
	{							\
		(t).capacity = (int)( (t).capacity * 1.25 ) + 1;\
		(t).value = realloc( (t).value, (t).capacity * sizeof( (t).value[0] ) );\
		assert( (t).value != NULL );			\
	} 							\
	(t).value[ (t).size ++ ] = v;				\
}

#define Value( t, i ) (t).value[i]

#define TraverseTable( t, i, op ) { int i; for( i = 0; i < (t).size; i++ ) { op; } }

#define DeleteTable( t, k )					\
{	int _i_; Boolean _s_ = false;				\
	for( _i_ = 0; _i_ < (t).size; _i_++ )			\
	{							\
		if( (k) == (t).value[_i_] ) _s_ = true;		\
		if( _s_ ) (t).value[_i_] = (t).value[_i_+1]; 	\
	}							\
	if( _s_ ) (t).size--;					\
}

#define AppendUnique( t, k )							\
{	int __i__; Boolean __f__;						\
	for( __i__ = 0, __f__ = false; __i__ < (t).size && !__f__; __i__++ ){	\
		if( (t).value[__i__] == (k) ) __f__ = true;			\
	}									\
	if( !__f__ ) AppendTable( (t), (k) );					\
}

#define AppendSet AppendUnique

#define SplitTable( t, t1, t2, i, cond )				\
{	int i;								\
	for( i = 0; i < (t).size; i++ )					\
	{								\
		if( cond ) AppendTable( (t1), (t).value[i] )		\
		else AppendTable( (t2), (t).value[i] );			\
	}								\
}

#endif
