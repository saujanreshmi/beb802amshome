#if ! defined( __CELLS_H__ )
#define __CELLS_H__

#include <stdbool.h>

#include <stdio.h>

//----------------------------------------------------------

typedef struct Cell Cell;
typedef Cell *pCell;

typedef struct Cell {
	pCell next;
	int x, y;
	char ch;
} Cell;

//----------------------------------------------------------

typedef struct CellList {
	Cell * first;
	Cell * last;
	int count;
} CellList;

//----------------------------------------------------------

typedef bool( *CellPredicate )( Cell * cell, void * args );

//----------------------------------------------------------

int count_cells( CellList * cells );

//----------------------------------------------------------

Cell * create_cell( int x, int y, char ch );

//----------------------------------------------------------

CellList * create_cell_list( void );

//----------------------------------------------------------

void init_cell_list( CellList * list );

//----------------------------------------------------------

void add_cell( Cell * cell, CellList * list );

//----------------------------------------------------------

void print_cells( FILE * file, CellList * first );

//----------------------------------------------------------

bool cells_equal( Cell * c1, Cell * c2 );

//----------------------------------------------------------

bool cell_lists_equal( CellList * c1, CellList * c2 );

//----------------------------------------------------------

/**
 *	Selects a subset of the cells in a list. The items chosen by the
 *	predicate are cloned and copied to a newly created CellList.
 */

CellList * select_cells( CellList * c1, CellPredicate predicate, void * args );

//----------------------------------------------------------

void destroy_cell_list( CellList * cells );

#endif
