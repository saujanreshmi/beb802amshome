#if !defined(__FUNCTION_TYPES_H__)
#define __FUNCTION_TYPES_H__

#define FunctionVar(name,result) result (*name)
#define Function(name,result) result name

typedef FunctionVar( Action, void ) ( void );

typedef unsigned int uint;
typedef unsigned long int ulong;
typedef unsigned short int ushort;

typedef void *Pointer;

#define New(T) malloc( sizeof( T ) )
#define Newc(T) calloc( 1, sizeof( T ) )
#define Free free

#endif
