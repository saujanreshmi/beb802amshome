#if ! defined( __FRAMES_H__ )
#define __FRAMES_H__

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include "action.h"

//----------------------------------------------------------

typedef struct Frame Frame;
typedef Frame * pFrame;

typedef enum { FrameType_Screen, FrameType_Char } FrameType;

struct Frame {
	struct Frame * next;
	FrameType frameType;
	double timeStamp;
	int screenNumber;
	int charCode;
	int width;
	int height;
	char ** cells;
};

//----------------------------------------------------------

typedef struct FrameList {
	Frame * first;
	Frame * last;
	int count;
	int screenCount;
} FrameList;

//----------------------------------------------------------

/**
*	Creates a newly allocated, empty, Frame.
*/

Frame * create_frame();

//----------------------------------------------------------

/**
 *	Creates a newly allocated, empty, FrameList.
 */

FrameList * create_frame_list( void );

//----------------------------------------------------------

/**
 *	Appends a frame to the designated list, setting the frame number 
 *	of the frame and the count of the frame list to reflrect the 
 *	position of the frame within the list.
 */

void add_frame( Frame * frame, FrameList * list );

//----------------------------------------------------------

/**
 *	Dumps the contents of the designated frame to a stream.
 */

void print_frame( FILE * file, Frame * frame );

//----------------------------------------------------------

/**
 *	Dumps the contents of the designated frame list to a stream.
 */

void print_frame_list( FILE * file, FrameList * list );

//----------------------------------------------------------

/**
 *	Parses a Frame from the supplied input stream.
 */

Frame * read_frame( FILE * file );

//----------------------------------------------------------

/**
 *	Parses a FrameList from the supplied input stream.
 */

FrameList * read_frame_list( FILE * file );

//----------------------------------------------------------

/**
 *	This function is not yet correctly implemented. 
 */

FrameList * get_connected_parts( Frame * frame );

//----------------------------------------------------------

/**
 *	Compares two FrameLists for equality, by comparing the parallel 
 *	lists of Frames.
 *
 *	If all frames are equal, the return result is true, and the 
 *	values referenced by x_mismatch and y_mismatch are not
 *	altered.
 *
 *	If any pair of frames are not equal, the return result is false.
 *	In that case, the variable referenced by x_mismatch will contain 
 *	the first Frame in x that does not match the corresponding and y_mismatch are not
 *	altered.
 */

bool frame_lists_equal( 
	FrameList * x, 
	FrameList * y, 
	Frame ** x_mismatch, 
	Frame ** y_mismatch,
	int * mismatch_frame_number,
	bool show_screens_only
);

//----------------------------------------------------------

/**
 *	Compares two frames for equality by comparing cells.
 *
 *	Cells are compared in the order they appear in the frame,
 *	which will work OK as long as the frames were generated 
 *	from a window of the same dimensions by the save-screen 
 *	function.
 *
 *	This should be fixed at some point, but for now it'll have 
 *	to do.
 */

bool frames_equal( Frame * x, Frame * y );

//----------------------------------------------------------

void destroy_frame( Frame * frame );

void destroy_frame_list( FrameList * list );

#endif
