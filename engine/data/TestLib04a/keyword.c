
#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stddef.h>
#include "types.h"
#include "keyword.h"
#include "utility.h"

/*---------------------------------------------------------------------*/
static Keyword keyWords[] = {
	{ "auto", sAuto },
	{ "break", sBreak },
	{ "case", sCase },
	{ "char", sChar },
	{ "const", sConst },
	{ "continue", sContinue },
	{ "default", sDefault },
	{ "define", sDefine },
	{ "do", sDo },
	{ "double", sDouble },
	{ "else", sElse },
	{ "enum", sEnum },
	{ "extern", sExtern },
	{ "float", sFloat },
	{ "for", sFor },
	{ "goto", sGoto },
	{ "if", sIf },
	{ "ifdef", sIfdef },
	{ "ifndef", sIfndef },
	{ "inline", sInline },
	{ "int", sInt },
	{ "long", sLong },
	{ "pragma", sPragma },
	{ "register", sRegister },
	{ "restrict", sRestrict },
	{ "return", sReturn },
	{ "short", sShort },
	{ "signed", sSigned },
	{ "sizeof", sSizeof },
	{ "static", sStatic },
	{ "struct", sStruct },
	{ "switch", sSwitch },
	{ "typedef", sTypedef },
	{ "undef", sUndef },
	{ "union", sUnion },
	{ "unsigned", sUnsigned },
	{ "void", sVoid },
	{ "volatile", sVolatile },
	{ "while", sWhile },

	{ "Error!", sError },
};
/*---------------------------------------------------------------------*/
static pKeyword Find( pString identifier ) {
	for ( int i = 0; keyWords[i].tokenType != sError; i++ ) {
		if ( 0 == strcmp( identifier, keyWords[i].literal ) ) {
			return &keyWords[i];
		}
	}

	return NULL;
}
/*---------------------------------------------------------------------*/
cKeyword tKeyword = {
	keyWords,
	Find,
};
/*---------------------------------------------------------------------*/
