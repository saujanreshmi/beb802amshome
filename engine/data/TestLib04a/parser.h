#pragma once
#include "scanner.h"

typedef struct Parser Parser;
typedef Parser * pParser;

struct Parser {
	pScanner scanner;
	SourceFile document_scope;
	pSymtab current_scope;
};

typedef struct cParser {
	void( *Initialise )( pParser instance, pScanner scanner, pString file_name );
	void( *ParseFile )( pParser instance );
} cParser;

extern cParser tParser;
