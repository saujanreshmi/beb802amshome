#if ! defined( __LINES_H__ )
#define __LINES_H__

#include <stdbool.h>
#include <stdio.h>
//----------------------------------------------------------

typedef struct StringNode {
	// Pointer to next sibling in list.
	struct StringNode * next;

	// Pointer to text associated with this node. 
	char * text;

	// Determines whether the text should be freed when the node is destroyed.
	bool ok_to_free;
} StringNode;

//----------------------------------------------------------

typedef struct StringList {
	StringNode * first;
	StringNode * last;
	int count;
} StringList;

//----------------------------------------------------------

/*
 *	Creates a string node, saves the supplied string pointer, and
 *	remembers whether it is ok to free the referenced text when the 
 *	node is destroyed.
 */

StringNode * create_string_node( char * text, bool ok_to_free );

//----------------------------------------------------------

void destroy_line( StringNode * line );

//----------------------------------------------------------

StringList * create_string_list( void );

//----------------------------------------------------------

void destroy_string_list( StringList * list );

//----------------------------------------------------------

void init_string_list( StringList * list );

//----------------------------------------------------------

void add_line( StringNode * line, StringList * list );

//----------------------------------------------------------

void print_lines( FILE * file, StringList * list, int max_lines );

//----------------------------------------------------------

bool lines_equal( StringNode * c1, StringNode * c2 );

//----------------------------------------------------------

/**
 *	Returns true if and only if the two linesa are equal.
 *
 *	If the string is empty, the function returns NULL.
 */

bool string_lists_equal( StringList * c1, StringList * c2 );

//----------------------------------------------------------

/**
 *	Parse the supplied string into a list of tokens, using the
 *	supplied delimiters to separate chars.
 *
 *	If the string is empty, the function returns NULL.
 */
 
StringList * split_string( char * string, char * delimiters );

#endif
