/*
|+----------------------------------------------------------------------
||	File:	scanner.c
||	Author: Lawrence Buckingham.
||
||	The scanner unit provides conversion of a sequential input
||	stream to a sequence of lexical tokens.
||
||	For each input stream, create a Scanner structure to control the
||	lexical scan, and associate it with the file.  This is operated
||	on by procedures in the scanner unit.
||
||	It recognises tokens as follows:
||
||	Identifier 	-> letter { letter | digit }.
||	Number		-> ['-']digits['.'digits][('E'|'e')['-']digits].
||	Comma		-> ','.
||	LeftPar		-> '('.
||	RightPar	-> ')'.
||	String		-> " {anychar-but-not-"} "
||			-> ' {anychar} '.
||				(use doubled '' or "" to include one in a string. )
||	Comment 	-> '{' { anything but '}' } '}'.
||
||	where	letter	= 'a' - 'z', 'A' - 'Z', '_'.
||		digit	= '0' - '9'.
||		digits	= digit { digit }.
||
||	This set of tokens suffices to allow reasonable flexibility in the
||	data files manipulated by the system.  Symbolic and numeric
||	attribute values are useable, and possibly structured values also.
||
||	CHANGES:
||	03-may-98:	Made ? scan as a number with value HUGE_VAL.
|+----------------------------------------------------------------------
*/

#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stddef.h>
#include "types.h"
#include "scanner.h"
#include "keyword.h"
#include "utility.h"

#undef DB
#include "db.h"

#define MAGIC (long)(('N'<<24)|('A'<<16)|('C'<<8)|'S') 

void GetCharRaw( pScanner s ) {
	s->nextChar = fgetc( s->fileHandle );

	if ( s->nextChar == '\r' ) {
		s->nextChar = fgetc( s->fileHandle );
	}

	if ( s->nextChar == '\n' ) {
		s->lineNumber++;
	}
}

void GetChar( pScanner s ) {
	GetCharRaw( s );

	bool go = true;

	while ( go && s->nextChar == '\\' ) {
		GetCharRaw( s );

		if ( s->nextChar == '\n' ) {
			GetCharRaw( s );
		}
		else {
			go = false;
			ungetc( s->nextChar, s->fileHandle );
			s->nextChar = '\\';
		}
	}
}

static void FreeScanner( pScanner );
static void ScanNumericLiteral( pScanner s );
static void ScanDecimalOrFloatLiteral( pScanner s );
static void ScanFractionalPart( pScanner s );
static void ScanDigitSequence( pScanner s );
static void ScanIntSuffix( pScanner s );
static void ScanHexLiteral( pScanner s );
static void ScanBinaryLiteral( pScanner s );
static void ScanStringOrCharLiteral( pScanner s );
static void ScanDot( pScanner s );

/*---------------------------------------------------------------------*/
static void	Destructor_( pNode n ) {
	pScanner s = n->container;
	FreeScanner( s );
}

/*---------------------------------------------------------------------*/
static pScanner NewScanner( const char * fileName, FILE * stream ) {
	pScanner s = calloc( 1, sizeof( Scanner ) );
	s->nextChar = ' ';
	tCharVector.Initialise( &s->word, 100, NULL, NULL );
	tCharVector.Add( &s->word, '\0' );
	s->token = sError;
	s->magic = MAGIC;
	s->lineNumber = 1;
	s->fileName = strdup( (char *) fileName );
	s->fileHandle = stream;
	GetChar( s );
	return s;
}

static void 	ResetWord( pScanner s ) {
	tCharVector.Clear( &s->word );
	tCharVector.Add( &s->word, '\0' );
}

static void	AppendChar( pScanner s ) {
	tCharVector.Add( &s->word, '\0' );
	tCharVector.Set( &s->word, s->word.length - 2, s->nextChar );
}

static void	AppendAndGet( pScanner s ) {
	AppendChar( s );
	GetChar( s );
}

static void	FreeScanner( pScanner s ) {
	assert( s->magic == MAGIC );

	s->nextChar = EOF;
	tCharVector.Finalise( &s->word );
	s->token = sError;
	s->magic = 0L;
	s->lineNumber = 1;

	free( s->fileName );
	s->fileHandle = NULL;

	if ( s->link.owner != NULL ) RemoveNode( &s->link );
	Free( s );
}

static void	ReadIdent( pScanner s ) {
	while ( isalnum( s->nextChar ) || s->nextChar == '_' ) {
		AppendChar( s );
		GetChar( s );
	}
}

#define IsSpace(c) (c == ' ' || c == '\t' || c == '\r')

typedef struct Branch Branch;

struct Branch {
	char ch;
	Token token;
	int child_count;
	Branch * children[10];
};

static bool MatchFromTrees( pScanner s ) {
	bool MatchFromTree_aux( pScanner s, Branch * branch );

	/*   !  !=   */
	Branch bangEq = { '=', sBangEq, 0, { NULL } };
	Branch bang = { '!', sBang, 1, { &bangEq } };

	if ( MatchFromTree_aux( s, &bang ) ) { return true; }

	/*   #    ##     */
	Branch crunch2 = { '#', sCrunchCrunch, 0, { NULL } };
	Branch crunch = { '#', sCrunch, 1, { &crunch2 } };

	if ( MatchFromTree_aux( s, &crunch ) ) { return true; }

	/*
	%
	%:
	%:%:
	%=
	%>
	*/
	Branch percentColon2 = { ':', sPercentColon2, 0, { NULL } };
	Branch percentColonPercent = { '%', sError, 1, { &percentColon2 } };
	Branch percentColon = { ':', sPercentColon, 1, { &percentColonPercent } };
	Branch percentEq = { '=', sPercentEq, 0, { NULL } };
	Branch percentGt = { '>', sPercentGt, 0, { NULL } };
	Branch percent = { '%', sPercent, 3, { &percentColon, &percentEq, &percentGt } };

	if ( MatchFromTree_aux( s, &percent ) ) { return true; }

	/*
	&
	&&
	&=
	*/
	Branch ampEq = { '=', sAmpEq, 0, { NULL } };
	Branch ampAmp = { '&', sAmpAmp, 0, { NULL } };
	Branch amp = { '&', sAmp, 2, { &ampEq, &ampAmp } };

	if ( MatchFromTree_aux( s, &amp ) ) { return true; }

	/*
	<
	<%
	<:
	<<
	<<=
	<=
	*/
	Branch ltPercent = { '%', sLtPercent, 0, { NULL } };
	Branch ltColon = { ':', sLtColon, 0, { NULL } };
	Branch ltLtEq = { '=', sLtLtEq, 0, { NULL } };
	Branch ltLt = { '<', sLtLt, 1, { &ltLtEq } };
	Branch ltEq = { '=', sLe, 0, { NULL } };
	Branch lt = { '<', sLt, 4, { &ltEq, &ltLt, &ltColon, &ltPercent } };

	if ( MatchFromTree_aux( s, &lt ) ) { return true; }

	/*
	>
	>=
	>>
	>>=
	*/
	Branch gtGtEq = { '=', sGtGtEq, 0, { NULL } };
	Branch gtGt = { '>', sGtGt, 1, { &gtGtEq } };
	Branch gtEq = { '=', sGe, 0, { NULL } };
	Branch gt = { '>', sGt, 2, { &gtEq, &gtGt } };

	if ( MatchFromTree_aux( s, &gt ) ) { return true; }

	/*
	-
	--
	-=
	->
	*/
	Branch minusMinus = { '-', sMinusMinus, 0, { NULL } };
	Branch minusEq = { '=', sMinusEq, 0, { NULL } };
	Branch arrow = { '>', sArrow, 0, { NULL } };
	Branch minus = { '-', sMinus, 3, { &minusMinus, &minusEq, &arrow } };

	if ( MatchFromTree_aux( s, &minus ) ) { return true; }

	/*
	=
	==
	*/
	Branch eqEq = { '=', sEqEq, 0, { NULL } };
	Branch eq = { '=', sEq, 1, { &eqEq } };

	if ( MatchFromTree_aux( s, &eq ) ) { return true; }

	/*
	^
	^=
	*/
	Branch caretEq = { '=', sCaretEq, 0, { NULL } };
	Branch caret = { '^', sCaret, 1, { &caretEq } };

	if ( MatchFromTree_aux( s, &caret ) ) { return true; }

	/*
	+
	++
	+=
	*/
	Branch plusPlus = { '+', sPlusPlus, 0, { NULL } };
	Branch plusEq = { '=', sPlusEq, 0, { NULL } };
	Branch plus = { '+', sPlus, 2, { &plusEq, &plusPlus } };

	if ( MatchFromTree_aux( s, &plus ) ) { return true; }

	/*
	~
	*/
	Branch tilde = { '~', sTilde, 0, { NULL } };

	if ( MatchFromTree_aux( s, &tilde ) ) { return true; }

	/*
	~
	*/
	Branch question = { '?', sQuestion, 0, { NULL } };

	if ( MatchFromTree_aux( s, &question ) ) { return true; }

	/*
	;
	*/
	Branch semi = { ';', sSemi, 0, { NULL } };

	if ( MatchFromTree_aux( s, &semi ) ) { return true; }

	/*
	|
	|=
	||
	*/
	Branch pipePipe = { '|', sPipePipe, 0, { NULL } };
	Branch pipeEq = { '=', sPipeEq, 0, { NULL } };
	Branch pipe = { '|', sPipe, 2, { &pipeEq, &pipePipe } };

	if ( MatchFromTree_aux( s, &pipe ) ) { return true; }

	/*
	:
	:>
	*/
	Branch colonGt = { '>', sColonGt, 0, { NULL } };
	Branch colon = { ':', sColon, 1, { &colonGt } };

	if ( MatchFromTree_aux( s, &colon ) ) { return true; }

	/*
	*
	*=
	*/
	Branch starEq = { '=', sStarEq, 0, { NULL } };
	Branch star = { '*', sStar, 1, { &starEq } };

	if ( MatchFromTree_aux( s, &star ) ) { return true; }

	return false;
}

bool MatchFromTree_aux( pScanner s, Branch * branch ) {
	if ( s->nextChar == branch->ch ) {
		AppendAndGet( s );
		s->token = branch->token;

		for ( size_t i = 0; i < branch->child_count; i++ ) {
			if ( MatchFromTree_aux( s, branch->children[i] ) ) {
				break;
			}
		}

		return true;
	}
	else {
		return false;
	}
}

static Token GetToken( pScanner s ) {
	// TRACE
	assert( s->magic == MAGIC );

	ResetWord( s );

	if ( IsSpace( s->nextChar ) ) {
		s->token = sSpace;
		while ( IsSpace( s->nextChar ) ) {
			AppendAndGet( s );
		}
	}
	else if ( s->nextChar == EOF ) {
		s->token = sEndOfFile;
	}
	else if ( s->nextChar == '\n' ) {
		s->token = sEOL;
		AppendAndGet( s );
	}
	else if ( isalpha( s->nextChar ) ) {
		ReadIdent( s );
		pKeyword keyword = tKeyword.Find( s->word.data );
		s->token = keyword ? sKeyword : sIdentifier;
	}

	else if ( isdigit( s->nextChar ) ) {
		ScanNumericLiteral( s );
	}

	else if ( s->nextChar == ',' ) {
		s->token = sComma;
		AppendAndGet( s );
	}

	else if ( s->nextChar == '[' ) {
		s->token = sLeftBracket;
		AppendAndGet( s );
	}

	else if ( s->nextChar == ']' ) {
		s->token = sRightBracket;
		AppendAndGet( s );
	}

	else if ( s->nextChar == '(' ) {
		s->token = sLeftPar;
		AppendAndGet( s );
	}

	else if ( s->nextChar == ')' ) {
		s->token = sRightPar;
		AppendAndGet( s );
	}

	else if ( s->nextChar == '{' ) {
		s->token = sLeftBrace;
		AppendAndGet( s );
	}

	else if ( s->nextChar == '}' ) {
		s->token = sRightBrace;
		AppendAndGet( s );
	}

	else if ( s->nextChar == '.' ) {
		ScanDot( s );
	}

	else if ( s->nextChar == '"' OR s->nextChar == '\'' ) {
		ScanStringOrCharLiteral( s );
	}

	else if ( s->nextChar == '/' ) {
		AppendAndGet( s );

		if ( s->nextChar == '/' ) {
			s->token = sComment;

			while ( s->nextChar != '\n' ) {
				AppendAndGet( s );
			}
		}
		else if ( s->nextChar == '*' ) {
			//TRACE
			s->token = sComment;

			bool go = true;
			char last_char = ' ';

			while ( go && s->nextChar != EOF ) {
				AppendAndGet( s );
				//DUMP( ( "%s(%d): last_char = '%c', s->nextChar == '%c'", __FILE__, __LINE__, last_char, s->nextChar ) )

				if ( s->nextChar == '/' && last_char == '*' ) {
					go = false;
					AppendAndGet( s );
				}
				else {
					last_char = s->nextChar;
				}
			}
			//TRACE
		}
		else if ( s->nextChar == '=' ) {
			s->token = sSlashEq;
			AppendAndGet( s );
		}
		else {
			s->token = sSlash;
		}
	}

	else if ( !MatchFromTrees( s ) ) {
		s->token = sError;
		AppendAndGet( s );
	} /* end of tests on s->nextChar */

	return s->token;
} /* end of Gettoken */

//----------------------------------------------------------------------

Token SkipSpaces( pScanner s ) {
	GetToken( s );

	while ( s->token == sEOL || s->token == sSpace ) {
		GetToken( s );
	}

	return s->token;
}
//----------------------------------------------------------------------

static void ScanDot( pScanner s ) {
	AppendAndGet( s );

	if ( isdigit( s->nextChar ) ) {
		s->token = sNumericLiteral;
		ScanFractionalPart( s );
	}
	else if ( s->nextChar == '.' ) {
		AppendAndGet( s );

		if ( s->nextChar == '.' ) {
			AppendAndGet( s );
			s->token = sEllipsis;
		}
		else {
			// We end up with .. which is not a valid token combination.
			s->token = sError;
		}
	}
	else {
		s->token = sDot;
	}
}

//----------------------------------------------------------------------

static void ScanHexLiteral( pScanner s ) {

	DUMP( ( "%s(%d): s->word.length == %d", __FILE__, __LINE__, s->word.length ) )
		DUMP( ( "%s(%d): s->word.data[s->word.length - 3] == %d", __FILE__, __LINE__, s->word.data[s->word.length - 3] ) )
		DUMP( ( "%s(%d): s->word.data[s->word.length - 2] == %d", __FILE__, __LINE__, s->word.data[s->word.length - 2] ) )
		DUMP( ( "%s(%d): s->word.data[s->word.length - 1] == %d", __FILE__, __LINE__, s->word.data[s->word.length - 1] ) )

		assert( s->word.length >= 2 && tolower( s->word.data[s->word.length - 2] ) == 'x' );

	while ( isdigit( s->nextChar ) || ( tolower( s->nextChar ) >= 'a' && tolower( s->nextChar ) <= 'f' ) ) {
		AppendAndGet( s );
	}

	if ( s->word.length == 2 ) {
		// didn't get any hexadecimal digits.
		s->token = sError;
	}
}

//----------------------------------------------------------------------

static void ScanBinaryLiteral( pScanner s ) {
	assert( s->word.length >= 2 && ( s->word.data[s->word.length - 2] ) == 'b' );

	while ( s->nextChar == '1' || s->nextChar == '0' ) {
		AppendAndGet( s );
	}

	if ( s->word.length == 2 ) {
		// didn't get any binary digits.
		s->token = sError;
	}
}

//----------------------------------------------------------------------

static void ScanStringOrCharLiteral( pScanner s ) {
	char delimiter = s->nextChar;

	s->token = delimiter == '"' ? sStringLiteral : sCharLiteral;
	AppendAndGet( s );

	while ( s->nextChar != delimiter && s->nextChar != EOF && s->nextChar != '\n' && s->nextChar != '\r' ) {
		// Read only until end of current line.

		if ( s->nextChar == '\\' ) {
			AppendAndGet( s );
			AppendAndGet( s );
		}

		else {
			AppendAndGet( s );
		}
	}

	if ( s->nextChar == delimiter ) {
		AppendAndGet( s );
	}
}

void ScanFractionalPart( pScanner s ) {
	assert( s->word.data[s->word.length - 2] == '.' );

	ScanDigitSequence( s );

	if ( s->nextChar == 'e' || s->nextChar == 'E' ) {
		AppendAndGet( s );

		if ( s->nextChar == '+' || s->nextChar == '-' ) {
			AppendAndGet( s );
		}

		ScanDigitSequence( s );
	}

	int ch = tolower( s->nextChar );

	if ( ch == 'f' || ch == 'l' ) {
		AppendAndGet( s );
	}
}

static void ScanNumericLiteral( pScanner s ) {
	s->token = sNumericLiteral;

	DUMP( ( "%s(%d): s->nextChar == '%c'", __FILE__, __LINE__, s->nextChar ) )

		if ( s->nextChar == '0' ) {
			AppendAndGet( s );
			int ch = tolower( s->nextChar );

			DUMP( ( "%s(%d): s->nextChar == '%c'", __FILE__, __LINE__, s->nextChar ) )

				if ( ch == 'x' ) {
					AppendAndGet( s );

					DUMP( ( "%s(%d): s->nextChar == '%c'", __FILE__, __LINE__, s->nextChar ) )
						ScanHexLiteral( s );
					ScanIntSuffix( s );
				}
				else if ( ch == 'b' ) {
					AppendAndGet( s );

					DUMP( ( "%s(%d): s->nextChar == '%c'", __FILE__, __LINE__, s->nextChar ) )
						ScanBinaryLiteral( s );
					ScanIntSuffix( s );
				}
				else if ( isdigit( ch ) ) {
					ScanDecimalOrFloatLiteral( s );
				}
		}
		else {
			ScanDecimalOrFloatLiteral( s );
		}
}

//--------------------------------------------------------------------

static void ScanDecimalOrFloatLiteral( pScanner s ) {
	ScanDigitSequence( s );

	if ( s->nextChar == '.' ) {
		AppendAndGet( s );
		ScanFractionalPart( s );
	}
	else {
		ScanIntSuffix( s );
	}
}

//--------------------------------------------------------------------

static void ScanDigitSequence( pScanner s ) {
	while ( isdigit( s->nextChar ) ) {
		AppendAndGet( s );
	}
}

//--------------------------------------------------------------------

static void ScanIntSuffix( pScanner s ) {
	if ( tolower( s->nextChar ) == 'u' ) {
		AppendAndGet( s );

		if ( tolower( s->nextChar ) == 'l' ) {
			AppendAndGet( s );
			if ( tolower( s->nextChar ) == 'l' ) {
				AppendAndGet( s );
			}
		}
	}
	else {
		if ( tolower( s->nextChar ) == 'l' ) {
			AppendAndGet( s );

			if ( tolower( s->nextChar ) == 'l' ) {
				AppendAndGet( s );
			}

			if ( tolower( s->nextChar ) == 'u' ) {
				AppendAndGet( s );
			}
		}
	}
}

/*---------------------------------------------------------------------*/

static bool Match( pScanner scanner, pString string ) {
	return StrEq( scanner->word.data, string );
}

/*---------------------------------------------------------------------*/

cScanner tScanner = {
	NewScanner,
	FreeScanner,
	GetToken,
	SkipSpaces,
	Match,
	Destructor_,
};
