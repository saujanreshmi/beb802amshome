#include "cell_cluster.h"

static Function( Create, pCellCluster ) ( void );
static Function( Destroy, void ) ( pCellCluster this );
static Function( IsAdjacent, bool ) ( pCellCluster this, pCell cell );
static Function( IsAdjacentAny, bool ) ( pCellCluster this, pCellCluster other );

cCellCluster tCellCluster = {
	Create,
	Destroy,
	IsAdjacent,
	IsAdjacentAny,
};

static Function( Create, pCellCluster ) ( void ) {
	pCellCluster this = malloc( sizeof( CellCluster ) );
	
	if ( this == NULL ) {
		return this;
	}

	this->cells = tpCellVector.Create( 0, NULL, NULL );
	
	if ( this->cells == NULL ) {
		free( this );
		this = NULL;
	}

	return this;
}

static Function( Destroy, void ) ( pCellCluster this ) {
	tpCellVector.Destroy( this->cells );
	free( this );
}

static Function( IsAdjacent, bool ) ( pCellCluster this, pCell p ) {
	for ( int i = 0; i < this->cells->length; i++ ) {
		pCell t = this->cells->data[i];
	
		int dx = p->x - t->x;
		int dy = p->y - t->y;

		if ( -1 <= dx && dx <= 1 && -1 <= dy && dy <= 1 ) {
			return true;
		}
	}

	return false;
}

static Function( IsAdjacentAny, bool ) ( pCellCluster this, pCellCluster other ) {
	for ( int i = 0; i < other->cells->length; i++ ) {
		pCell t = other->cells->data[i];
		if ( IsAdjacent( this, t ) ) {
			return true;
		}
	}

	return false;
}
