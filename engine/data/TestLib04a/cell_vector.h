#pragma once 

#include <stdbool.h>
#include <stdlib.h>
#include "function_types.h"

#include "cells.h"

typedef struct pCellVector {
	size_t capacity;
	size_t length;
	pCell * data;
	FunctionVar( clone, void )( pCell *dest, pCell *source );
	FunctionVar( destructor, void ) ( pCell *item );
} pCellVector;

typedef pCellVector *ppCellVector;

typedef struct cpCellVector {
	/* Allocate and initialise a new vector. */
	FunctionVar( Create, ppCellVector ) (
		size_t capacity,
		FunctionVar( clone, void )( pCell *dest, pCell *source ),
		FunctionVar( destructor, void ) ( pCell *item )
		);

	/* Free the dynamic array and the vector itself (must have been initialised via Create). */
	FunctionVar( Destroy, void ) ( ppCellVector this );

	/* Initialise a pre-existing vector. */
	FunctionVar( Initialise, bool ) (
		ppCellVector this,
		size_t capacity,
		FunctionVar( clone, void )( pCell *dest, pCell *source ),
		FunctionVar( destructor, void ) ( pCell *item )
		);

	/* Release the storage used by dynamic array without deallocating a pre-existing vector. */
	FunctionVar( Finalise, void ) (
		ppCellVector this
		);

	/* Append a new item to the vector. */
	FunctionVar( Add, bool ) (
		ppCellVector this,
		pCell item
		);

	/* Insert a new item into the vector at the designated position. */
	FunctionVar( Insert, bool ) (
		ppCellVector this,
		pCell item,
		size_t pos
		);

	/* Reallocate the dynamic array to a new size. */
	FunctionVar( Resize, bool ) (
		ppCellVector this,
		size_t new_size
		);

	/* Delete the designated item from the vector. If the destructor is not null it will be applied to the removed item prior to removal. */
	FunctionVar( Remove, void ) (
		ppCellVector this,
		size_t pos
		);

	/* Traverse the items in the vector. */
	FunctionVar( Foreach, void ) (
		ppCellVector this,
		FunctionVar( func, bool )( pCell *item, Pointer data ),
		Pointer data
		);

	/* Reset the size of the vector back to zero. If the destructor is non-null, it will be invoked on each item. */
	FunctionVar( Clear, void ) (
		ppCellVector this
		);

	/* Appends a list of items to the current vector, using a shallow copy (if clone is null) or by calling clone for each item. */
	FunctionVar( AddRange, void ) (
		ppCellVector this,
		const pCell * newItems,
		size_t n
		);

	/* Sets the item at the specified location to a new value. */
	FunctionVar( Set, void ) (
		ppCellVector this,
		size_t pos,
		pCell value
		);

	/* Gets the item at the specified location. */
	FunctionVar( Get, pCell ) (
		ppCellVector this,
		size_t pos
		);

} cpCellVector;

extern cpCellVector tpCellVector;
