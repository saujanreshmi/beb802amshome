#pragma once

/*
|+------------------------------------------------------------------------
||	File: 	types.h
||	Author: Lawrence Buckingham
||
||	Standard type definitions used within this project.
|+------------------------------------------------------------------------
*/
#undef MAC

#define decl1(t,x) 		t x
#define decl2(t,x,y) 		decl1(t,x), t y
#define decl3(t,x,y,z) 		decl2(t,x,y), t z
#define decl4(t,x,y,z,a) 	decl3(t,x,y,z), t a
#define decl5(t,x,y,z,a,b)	decl4(t,x,y,z,a), t b

/* Arbitrary pointer type. */
typedef void *SafePointer;

/* Arbitrary pointer type. */
typedef void *Pointer;
	
typedef enum { DefinitelyNo, NotSure, DefinitelyYes } Fuzzy;

typedef double Numeric;
	/* The native data type to use for numbers. */
	/* I might want to change this later.       */

typedef char *pString;
	/* A legible name for string pointers. */
