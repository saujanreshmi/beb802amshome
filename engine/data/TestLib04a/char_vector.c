#include <assert.h>
#include <math.h>
#include <string.h>

#include "char_vector.h"

const static double scale = 1.1;
const static int min_capacity = 10;

static Function( Create, pCharVector ) (
	size_t capacity,
	FunctionVar( clone, void )( Char *dest, Char *source ),
	FunctionVar( destructor, void ) ( Char *item )
	);

static Function( Destroy, void ) ( pCharVector this );

static bool Initialise(
	pCharVector this,
	size_t capacity,
	FunctionVar( clone, void )( Char *dest, Char *source ),
	FunctionVar( destructor, void ) ( Char *item )
	);

static void Finalise( pCharVector this );

static Function( Add, bool ) ( pCharVector this, Char item );

static Function( Insert, bool ) ( pCharVector this, Char item, size_t pos );

static Function( Remove, void ) ( pCharVector this, size_t pos );

static Function( Resize, bool ) ( pCharVector this, size_t new_size );

static Function( Foreach, void ) ( pCharVector this, FunctionVar( func, bool )( Char *item, Pointer data ), Pointer data );

static Function( Clear, void ) ( pCharVector this );

static Function( AddRange, void ) ( pCharVector this, const Char * newItems, size_t n );

static Function( Set, void ) (
	pCharVector this,
	size_t pos,
	Char value
	);

static Function( Get, Char ) (
	pCharVector this,
	size_t pos
	);

cCharVector tCharVector = {
	Create, 
	Destroy,
	Initialise,
	Finalise,
	Add, 
	Insert, 
	Resize, 
	Remove, 
	Foreach,
	Clear,
	AddRange,
	Set,
	Get
};

static Function( Create, pCharVector ) (
	size_t capacity,
	FunctionVar( clone, void )( Char *dest, Char *source ),
	FunctionVar( destructor, void ) ( Char *item )
	) {
	pCharVector this = calloc( 1, sizeof( *this ) );
	
	if ( this && !Initialise( this, capacity, clone, destructor ) ) {
		free( this );
		this = NULL;
	}
	
	return this;
}

static bool Initialise(
	pCharVector this,
	size_t capacity,
	FunctionVar( clone, void )( Char *dest, Char *source ),
	FunctionVar( destructor, void ) ( Char *item )
	) {
	if ( this == NULL ) return false;
	
	if ( capacity < min_capacity ) {
		capacity = min_capacity;
	}

	this->data = malloc( capacity * sizeof( Char ) );
	
	if ( this->data == NULL ) {
		return false;
	}

	this->capacity = capacity;
	this->length = 0;
	this->clone = clone;
	this->destructor = destructor;

	return this;
}

static Function( Destroy, void ) ( pCharVector this ) {
	Finalise( this );
	free( this );
}

static void Finalise( pCharVector this ) {
	if ( this ) {
		free( this->data );
		memset( this, 0, sizeof( *this ) );
	}
}

static Function( Add, bool ) ( pCharVector this, Char item ) {
	if ( this->length == this->capacity ) {
		bool ok = Resize( this, ceil(this->capacity * scale) );

		if ( !ok ) return false;
 	}

	if ( this->clone ) {
		this->clone( &this->data[this->length], &item );
	}
	else {
		this->data[this->length] = item;
	}

	this->length++;

	return true;
}

static Function( Insert, bool ) ( pCharVector this, Char item, size_t pos ) {
	if ( this->length == this->capacity ) {
		bool ok = Resize( this, ceil(this->capacity * scale) );

		if ( !ok ) return false;
 	}

	for ( int i = this->length; i > pos; i-- ) {
		this->data[i] = this->data[i - 1];
	}

	if ( this->clone ) {
		this->clone( &this->data[pos], &item );
	}
	else {
		this->data[pos] = item;
	}

	this->length++;

	return true;
}

static Function( Remove, void ) ( pCharVector this, size_t pos ) {
	assert( pos >= 0 && pos < this->length );

	if ( this->destructor ) {
		this->destructor( &this->data[pos] );
	}

	for ( int i = pos; i < this->length - 1; i++ ) {
		this->data[i] = this->data[i + 1];
	}

	this->length--;
}

static Function( Resize, bool ) ( pCharVector this, size_t new_size ) {
	if ( new_size == this->capacity ) {
		return true;
	}

	Char *data = malloc( new_size * sizeof( Char ) );

	if ( data == NULL ) {
		return false;
	}

	// Destroy items that will be lost via truncation.
	if ( new_size < this->length && this->destructor ) {
		for ( size_t i = new_size; i < this->length; i++ ) {
			this->destructor( &this->data[i] );
		}
	}

	size_t length = new_size < this->length ? new_size : this->length;

	memcpy( data, this->data, length * sizeof( Char ) );
	free( this->data );
	this->data = data;
	this->capacity = new_size;
	this->length = length;

	return true;
}

static Function( Foreach, void ) ( 
	pCharVector this, 
	FunctionVar( func, bool )( Char * item, Pointer data ), 
	Pointer data 
	) {
	for ( int i = 0; i < this->length; i++ ) {
		if ( !func( &this->data[i], data ) ) {
			return;
		}
	}
}

static Function( Clear, void ) ( pCharVector this ) {
	assert( this != NULL);

	if ( this->destructor != NULL ) {
		for ( int i = 0; i < this->length; i++ ) {
			this->destructor( &this->data[i] );
		}
	}
	this->length = 0;
}

static 	Function( AddRange, void ) ( pCharVector this, const Char * newItems, size_t n ) {
	for ( int i = 0; i < n; i++ ) {
		Add( this, newItems[i] );
	}
}

static Function( Set, void ) (
	pCharVector this,
	size_t pos,
	Char value
	) {
	assert( this != NULL );
	assert( pos < this->length );

	if ( this->clone != NULL ) {
		this->clone( &this->data[pos], &value );
	}
	else {
		this->data[pos] = value;
	}
}

static Function( Get, Char ) (
	pCharVector this,
	size_t pos
	) {
	assert( this != NULL );
	assert( pos < this->length );

	return this->data[pos];
}
