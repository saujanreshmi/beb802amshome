#include <assert.h>
#include <math.h>
#include <string.h>

#include "cell_clusters.h"

const static double scale = 1.1;
const static int min_capacity = 10;

static Function( Create, ppCellClusterVector ) (
	size_t capacity,
	FunctionVar( clone, void )( pCellCluster *dest, pCellCluster *source ),
	FunctionVar( destructor, void ) ( pCellCluster *item )
	);

static Function( Destroy, void ) ( ppCellClusterVector this );

static bool Initialise(
	ppCellClusterVector this,
	size_t capacity,
	FunctionVar( clone, void )( pCellCluster *dest, pCellCluster *source ),
	FunctionVar( destructor, void ) ( pCellCluster *item )
	);

static void Finalise( ppCellClusterVector this );

static Function( Add, bool ) ( ppCellClusterVector this, pCellCluster item );

static Function( Insert, bool ) ( ppCellClusterVector this, pCellCluster item, size_t pos );

static Function( Remove, void ) ( ppCellClusterVector this, size_t pos );

static Function( Resize, bool ) ( ppCellClusterVector this, size_t new_size );

static Function( Foreach, void ) ( ppCellClusterVector this, FunctionVar( func, bool )( pCellCluster *item, Pointer data ), Pointer data );

static Function( Clear, void ) ( ppCellClusterVector this );

static Function( AddRange, void ) ( ppCellClusterVector this, const pCellCluster * newItems, size_t n );

static Function( Set, void ) (
	ppCellClusterVector this,
	size_t pos,
	pCellCluster value
	);

static Function( Get, pCellCluster ) (
	ppCellClusterVector this,
	size_t pos
	);

cpCellClusterVector tpCellClusterVector = {
	Create, 
	Destroy,
	Initialise,
	Finalise,
	Add, 
	Insert, 
	Resize, 
	Remove, 
	Foreach,
	Clear,
	AddRange,
	Set,
	Get
};

static Function( Create, ppCellClusterVector ) (
	size_t capacity,
	FunctionVar( clone, void )( pCellCluster *dest, pCellCluster *source ),
	FunctionVar( destructor, void ) ( pCellCluster *item )
	) {
	ppCellClusterVector this = calloc( 1, sizeof( *this ) );
	
	if ( this && !Initialise( this, capacity, clone, destructor ) ) {
		free( this );
		this = NULL;
	}
	
	return this;
}

static bool Initialise(
	ppCellClusterVector this,
	size_t capacity,
	FunctionVar( clone, void )( pCellCluster *dest, pCellCluster *source ),
	FunctionVar( destructor, void ) ( pCellCluster *item )
	) {
	if ( this == NULL ) return false;
	
	if ( capacity < min_capacity ) {
		capacity = min_capacity;
	}

	this->data = malloc( capacity * sizeof( pCellCluster ) );
	
	if ( this->data == NULL ) {
		return false;
	}

	this->capacity = capacity;
	this->length = 0;
	this->clone = clone;
	this->destructor = destructor;

	return this;
}

static Function( Destroy, void ) ( ppCellClusterVector this ) {
	Finalise( this );
	free( this );
}

static void Finalise( ppCellClusterVector this ) {
	if ( this ) {
		free( this->data );
		memset( this, 0, sizeof( *this ) );
	}
}

static Function( Add, bool ) ( ppCellClusterVector this, pCellCluster item ) {
	if ( this->length == this->capacity ) {
		bool ok = Resize( this, ceil(this->capacity * scale) );

		if ( !ok ) return false;
 	}

	if ( this->clone ) {
		this->clone( &this->data[this->length], &item );
	}
	else {
		this->data[this->length] = item;
	}

	this->length++;

	return true;
}

static Function( Insert, bool ) ( ppCellClusterVector this, pCellCluster item, size_t pos ) {
	if ( this->length == this->capacity ) {
		bool ok = Resize( this, ceil(this->capacity * scale) );

		if ( !ok ) return false;
 	}

	for ( int i = this->length; i > pos; i-- ) {
		this->data[i] = this->data[i - 1];
	}

	if ( this->clone ) {
		this->clone( &this->data[pos], &item );
	}
	else {
		this->data[pos] = item;
	}

	this->length++;

	return true;
}

static Function( Remove, void ) ( ppCellClusterVector this, size_t pos ) {
	assert( pos >= 0 && pos < this->length );

	if ( this->destructor ) {
		this->destructor( &this->data[pos] );
	}

	for ( int i = pos; i < this->length - 1; i++ ) {
		this->data[i] = this->data[i + 1];
	}

	this->length--;
}

static Function( Resize, bool ) ( ppCellClusterVector this, size_t new_size ) {
	if ( new_size == this->capacity ) {
		return true;
	}

	pCellCluster *data = malloc( new_size * sizeof( pCellCluster ) );

	if ( data == NULL ) {
		return false;
	}

	// Destroy items that will be lost via truncation.
	if ( new_size < this->length && this->destructor ) {
		for ( size_t i = new_size; i < this->length; i++ ) {
			this->destructor( &this->data[i] );
		}
	}

	size_t length = new_size < this->length ? new_size : this->length;

	memcpy( data, this->data, length * sizeof( pCellCluster ) );
	free( this->data );
	this->data = data;
	this->capacity = new_size;
	this->length = length;

	return true;
}

static Function( Foreach, void ) ( 
	ppCellClusterVector this, 
	FunctionVar( func, bool )( pCellCluster * item, Pointer data ), 
	Pointer data 
	) {
	for ( int i = 0; i < this->length; i++ ) {
		if ( !func( &this->data[i], data ) ) {
			return;
		}
	}
}

static Function( Clear, void ) ( ppCellClusterVector this ) {
	assert( this != NULL);

	if ( this->destructor != NULL ) {
		for ( int i = 0; i < this->length; i++ ) {
			this->destructor( &this->data[i] );
		}
	}
	this->length = 0;
}

static 	Function( AddRange, void ) ( ppCellClusterVector this, const pCellCluster * newItems, size_t n ) {
	for ( int i = 0; i < n; i++ ) {
		Add( this, newItems[i] );
	}
}

static Function( Set, void ) (
	ppCellClusterVector this,
	size_t pos,
	pCellCluster value
	) {
	assert( this != NULL );
	assert( pos < this->length );

	if ( this->clone != NULL ) {
		this->clone( &this->data[pos], &value );
	}
	else {
		this->data[pos] = value;
	}
}

static Function( Get, pCellCluster ) (
	ppCellClusterVector this,
	size_t pos
	) {
	assert( this != NULL );
	assert( pos < this->length );

	return this->data[pos];
}
