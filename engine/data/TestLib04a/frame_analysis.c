#include "frame_analysis.h"

static Function( GetConnectedCells, ppCellClusterVector ) ( CellList * cells, FunctionVar( predicate, bool ) ( pCell cell, Pointer data ), Pointer data );
static Function( CompareFrames, void ) ( FrameList * expected_frames, FrameList * actual_frames, int * frame_number, bool * all_ok );
static Function( CompareScreens, void ) ( FrameList * expected_frames, FrameList * actual_frames, int * frame_number, bool * all_ok );

cFrameAnalysis tFrameAnalysis = {
	GetConnectedCells,
	CompareFrames,
	CompareScreens
};

static void CompareFrames( FrameList * expected_frames, FrameList * actual_frames, int * frame_number, bool * all_ok ) {
	Frame * expected;
	Frame * actual;

#define ok ( *all_ok )
#define frame_num (*frame_number)

	ok = expected_frames->count == actual_frames->count;

	if ( !ok ) {
		printf( "Frame count does not match.\n" );
		printf( "Expected frame count = %d\n", expected_frames->count );
		printf( "  Actual frame count = %d\n", actual_frames->count );
		printf( "\n" );
	}

	ok = frame_lists_equal( expected_frames, actual_frames, &expected, &actual, &frame_num, false );

	if ( !ok ) {
		printf( "Screen does not match expected value at frame number %d.\n", frame_num );
		printf( "N.B. Time stamp values will usually differ. They are _not_ included in frame comparison.\n" );
		printf( "Expected frame: (((" );
		print_frame( stdout, expected );
		printf( ")))\nActual frame: (((" );
		print_frame( stdout, actual );
		printf( ")))\n" );
	}

#undef ok
#undef frame_num
}

static void CompareScreens( FrameList * expected_frames, FrameList * actual_frames, int * frame_number, bool * all_ok ) {
	Frame * expected;
	Frame * actual;

#define ok ( *all_ok )
#define frame_num (*frame_number)

	ok = expected_frames->count == actual_frames->count;

	if ( !ok ) {
		printf( "Frame count does not match.\n" );
		printf( "Expected frame count = %d\n", expected_frames->count );
		printf( "  Actual frame count = %d\n", actual_frames->count );
		printf( "\n" );
	}

	ok = frame_lists_equal( expected_frames, actual_frames, &expected, &actual, &frame_num, true );

	if ( !ok ) {
		printf( "Screen does not match expected value at frame number %d.\n", frame_num );
		printf( "N.B. Time stamp values will usually differ. They are _not_ included in frame comparison.\n" );
		printf( "Expected frame: (((" );
		print_frame( stdout, expected );
		printf( ")))\nActual frame: (((" );
		print_frame( stdout, actual );
		printf( ")))\n" );
	}

#undef ok
#undef frame_num
}

static Function( GetConnectedCells, ppCellClusterVector ) (
	CellList * cells,
	FunctionVar( predicate, bool ) ( pCell cell, Pointer data ),
	Pointer data
	) {
	pCellCluster matchingCells = tCellCluster.Create();

	for ( pCell cell = cells->first; cell != NULL; cell = cell->next ) {
		if ( predicate( cell, data ) ) {
			tpCellVector.Add( matchingCells->cells, cell );
		}
	}

	ppCellClusterVector clusters = tpCellClusterVector.Create( 0, NULL, NULL );
	ppCellClusterVector adjacentClusters = tpCellClusterVector.Create( 0, NULL, NULL );

	for ( int i = 0; i < matchingCells->cells->length; i++ ) {
		pCell p = matchingCells->cells->data[i];
		tpCellClusterVector.Clear( adjacentClusters );

		for ( int j = 0; j < clusters->length; j++ ) {
			pCellCluster c = clusters->data[j];

			if ( tCellCluster.IsAdjacent( c, p ) ) {
				tpCellClusterVector.Add( adjacentClusters, c );
			}
		}

		if ( adjacentClusters->length == 0 ) {
			pCellCluster newCluster = tCellCluster.Create();
			tpCellVector.Add( newCluster->cells, p );
			tpCellClusterVector.Add( clusters, newCluster );
		}
		else {
			tpCellVector.Add( adjacentClusters->data[0]->cells, p );

			for ( int k = adjacentClusters->length - 1; k > 0; k-- ) {
				tpCellVector.AddRange(
					adjacentClusters->data[0]->cells,
					adjacentClusters->data[k]->cells->data,
					adjacentClusters->data[k]->cells->length
					);
				tpCellClusterVector.Remove( clusters, k );
			}
		}
	}

	// I _think_ this is not necessary, but for now I don't entirely trust the reasoning powers of 
	// my rather tired brain.
	bool somethingChanged = true;

	while ( somethingChanged ) {
		somethingChanged = false;

		for ( int i = 0; i < clusters->length; i++ ) {
			for ( int j = clusters->length - 1; j > i; j-- ) {
				if ( tCellCluster.IsAdjacentAny( clusters->data[i], clusters->data[j] ) ) {
					tpCellVector.AddRange(
						clusters->data[i]->cells,
						clusters->data[j]->cells->data,
						clusters->data[j]->cells->length
						);
					tpCellClusterVector.Remove( clusters, j );
					somethingChanged = true;
				}
			}
		}
	}

	return clusters;
}

