TARGETS="cab202_graphics cab202_timers cab202_sprites makefile"
DEST=q:/cab202/2016-02/Topic01/ZDK 

echo ${TARGETS}

for f in ${TARGETS}; do
	if [ -f $f ]; then cp $f $DEST; fi;
	if [ -f $f.c ]; then cp $f.c $DEST; fi;
	if [ -f $f.h ]; then cp $f.h $DEST; fi;
	if [ -f $f.html ]; then cp $f.html $DEST; fi; 	
done

if [ -d Images ]; then cp -r Images ${DEST}; fi;

cp makefile ${DEST}

echo done
