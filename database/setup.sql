if exists(SELECT * FROM SYS.DATABASES WHERE name = 'amshome')
DROP DATABASE amshome
GO

CREATE DATABASE amshome
GO

USE amshome
GO

CREATE SCHEMA cab202
GO

CREATE TABLE cab202.[USER]
(
  ID INT IDENTITY PRIMARY KEY,
  STUDENTNUMBER VARCHAR(15) NOT NULL
)
GO

INSERT INTO cab202.[USER](STUDENTNUMBER) VALUES('09003096')
GO

CREATE TABLE cab202.[TOPIC]
(
  ID INT IDENTITY ,
  TOPIC INT PRIMARY KEY,
  NAME VARCHAR(100) NOT NULL,
  DESCRIPTION VARCHAR(500) NOT NULL
)
GO

CREATE TABLE cab202.[EXERCISE]
(
  ID INT IDENTITY PRIMARY KEY,
  EXERCISE INT NOT NULL,
  TOPIC_ID INT NOT NULL,
  NAME        VARCHAR(100) NOT NULL,
  DESCRIPTION VARCHAR(500) NOT NULL,
  CODE        VARCHAR(max) NOT NULL,
  REQUIREMENT VARCHAR(max) NOT NULL,
  FOREIGN KEY (TOPIC_ID) REFERENCES cab202.[TOPIC](TOPIC)
)
GO
/*
INSERT INTO cab202.[TOPIC](TOPIC, NAME, DESCRIPTION) VALUES(1, 'A First C program:', 'Getting started, using standard output and the CAB202 ZDK.');
INSERT INTO cab202.[TOPIC](TOPIC, NAME, DESCRIPTION) VALUES(2, 'Simple if statements and while loops', 'Programs that use while loops, if tests, integer arithmetic, and simple animation.');
INSERT INTO cab202.[TOPIC](TOPIC, NAME, DESCRIPTION) VALUES(3, 'Advanced if tests and the for loop:', 'Programs that perform collision detection between sprites and draw geometric shapes.');
GO

INSERT INTO cab202.[EXERCISE](EXERCISE, TOPIC_ID, NAME, DESCRIPTION) VALUES(1,1,'Hello World','Complete the implementation of the hello_cab202 function. This function displays a simple message.')
INSERT INTO cab202.[EXERCISE](EXERCISE, TOPIC_ID, NAME, DESCRIPTION) VALUES(2,1,'ZDK Ball','Complete the implementation of the place_ball function. This function draws a ball at the top left hand corner of the screen using the ZDK.')
INSERT INTO cab202.[EXERCISE](EXERCISE, TOPIC_ID, NAME, DESCRIPTION) VALUES(3,1,'ZDK Multiple Balls','Complete the implementation of the place_balls function. This function draws the balls at the different locations in the window.')
INSERT INTO cab202.[EXERCISE](EXERCISE, TOPIC_ID, NAME, DESCRIPTION) VALUES(4,1,'ZDK Verticle Line','Complete the implementation of the incomplete functions. This program draws a vertical line on the left side of the screen.')
INSERT INTO cab202.[EXERCISE](EXERCISE, TOPIC_ID, NAME, DESCRIPTION) VALUES(1,2,'List Consecutive Integers','Complete the implementation of the list_integers function. This function uses a while loop to write a list of consecutive integers to the standard output stream.')
INSERT INTO cab202.[EXERCISE](EXERCISE, TOPIC_ID, NAME, DESCRIPTION) VALUES(2,2,'Count Down','This function writes a descending list of integers to the standard output stream.')
INSERT INTO cab202.[EXERCISE](EXERCISE, TOPIC_ID, NAME, DESCRIPTION) VALUES(3,2,'Draw Border','Complete a function that draws a border around the inner edge of the terminal window.')
INSERT INTO cab202.[EXERCISE](EXERCISE, TOPIC_ID, NAME, DESCRIPTION) VALUES(4,2,'Running Zombie','Practice character input handling, multi-way if statements, and working with sprites.')
INSERT INTO cab202.[EXERCISE](EXERCISE, TOPIC_ID, NAME, DESCRIPTION) VALUES(5,2,'ZombieDash Jr','Worked example for lecture 2.')
INSERT INTO cab202.[EXERCISE](EXERCISE, TOPIC_ID, NAME, DESCRIPTION) VALUES(1,3,'Draw Rectangle 1','Complete the implementation of a function that augments the ZDK by adding the ability to draw rectangular boxes. The hash-tags for this exercise are: #cab202 and #cab202DrawRect1.')
INSERT INTO cab202.[EXERCISE](EXERCISE, TOPIC_ID, NAME, DESCRIPTION) VALUES(2,3,'Fill Rectangle 1','Complete a function that renders a filled rectangle defined by a collection of global variables. The hash-tags for this exercise are: #cab202 and #cab202FillRect1.')
INSERT INTO cab202.[EXERCISE](EXERCISE, TOPIC_ID, NAME, DESCRIPTION) VALUES(3,3,'Shooting Star','This program simulates the flight of an object along a straight line between two points. The hash-tags for this exercise are: #cab202 and #cab202ShootingStar.')
INSERT INTO cab202.[EXERCISE](EXERCISE, TOPIC_ID, NAME, DESCRIPTION) VALUES(4,3,'Bird vs Platform','Non-assessed support material for Assignment 1. Lecturer will complete the implementation of the bird_platform program in class.')

GO
*/
