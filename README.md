# amshome
# This repository is a home version of a existing ams used in teaching of CAB202 that is designed to run in users machine.

Requirements:
 -> Docker Community Edition Stable Channel

How to Run:
 -> Start docker

 -> Go to root folder

 -> Enter command "docker-compose build"
 This will build the required docker images. 

 -> Enter command "docker-compose up"
 This will create the docker containers from the earlier build images.

 Open another terminal and enter command "docker ps -a"
 if this command returns three containers named amshome_database, amshome_engine and amshome_webserver then 
 open the browser(firefox and chrome are recommended for better experience).

 -> Enter url "localhost:5555"
 This should open the interface of amshome.
