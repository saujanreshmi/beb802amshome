const express = require("express");
const favicon = require("serve-favicon");
const bodyParser = require("body-parser");
const homepage = require("./routes/default");
const topics = require("./routes/topics");
const exercises = require("./routes/exercises");
const notfound = require("./routes/error");


const staticAssets = __dirname + "/public";

const app = express();

app
	.set("view engine", "hjs")
	
	.use(express.static(staticAssets))

	.use(favicon(staticAssets+"/amshomelogo.png"))

	.use(bodyParser.urlencoded({
		extended: false}))
	
	.use(homepage)
	
	.use("/topics",topics)

	.use("/topics/exerciseindex", exercises)

	.use(notfound)
;

app.listen(3000);
