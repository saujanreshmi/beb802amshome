#define BUFSIZE 1024

#include <node.h>
#include <v8.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <string>

using namespace v8;

static void throwError(const char *errorMessage) {
	Isolate* isolate = Isolate::GetCurrent();
	isolate->ThrowException(Exception::Error(String::NewFromUtf8(isolate, errorMessage)));
}

static void throwTypeError(const char *errorMessage) {
	Isolate* isolate = Isolate::GetCurrent();
	isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, errorMessage)));
}

static int parseArgs(const FunctionCallbackInfo<Value>& args, int* pPort, char** pHost) {
	if (args.Length() < 1) {
		throwTypeError("port is not specified");
		return -1;
	}
	if (!args[0]->IsNumber()) {
		throwTypeError("port is not a number");
		return -1;
	}
	
	*pPort = args[0]->NumberValue();

	if (args.Length() >= 2) {
		if (!args[1]->IsString()) {
			throwTypeError("host is not a string");
			return -1;
		}
		Local<String> hostStr = args[1]->ToString();
		int hostLen = hostStr->Length();
		*pHost = (char*)malloc(hostLen + 1);
		String::Utf8Value value(hostStr);
		strcpy(*pHost,*value);
	} else {
		// host is not specified, use default value.
		*pHost = (char*)malloc(12);
		strcpy(*pHost,"127.0.0.1");
	}

	return 0;
}



void connectMethod(const FunctionCallbackInfo<Value>& args) {
	Isolate* isolate = Isolate::GetCurrent();
    HandleScope scope(isolate);

	int port;
	char* host;
	if (parseArgs(args, &port, &host) < 0) {
		return;
	}

    int sock = 0;
    struct sockaddr_in serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        throwTypeError("Socket creation error");
        return;
    }
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);
    serv_addr.sin_addr.s_addr = inet_addr(host);

    if (connect(sock , (struct sockaddr *)&serv_addr , sizeof(serv_addr)) < 0)
    {
        //throwTypeError("connect failed. Error");
        args.GetReturnValue().Set(Number::New(isolate, 0));
        return;
    }

	args.GetReturnValue().Set(Number::New(isolate, sock));
}



void sendMethod(const FunctionCallbackInfo<Value>& args){
	Isolate* isolate = Isolate::GetCurrent();
    HandleScope scope(isolate);

    if (args.Length() < 1) {
		throwTypeError("socket and data are not specified");
		return;
	} else if (args.Length() < 2) {
		throwTypeError("data is not specified");
		return;
	}
	if (!args[0]->IsNumber()) {
		throwTypeError("socket is not a number");
		return;
	}
	if (!args[1]->IsString()) {
		throwTypeError("data is not a string");
		return;
	}
	int sock;
	Local<String> data;
	char buf[BUFSIZE];

	sock = args[0]->NumberValue();
	data = args[1]->ToString();
	String::Utf8Value value(data);
	strcpy(buf,*value);

	int bytesent = send(sock, buf, strlen(buf),0);

	if (bytesent == -1){
		throwError("data cannot be sent");
		return;
	}

	args.GetReturnValue().Set(Number::New(isolate, bytesent));
}


void receiveMethod(const FunctionCallbackInfo<Value>& args){
	Isolate* isolate = Isolate::GetCurrent();
	HandleScope scope(isolate);

	int sock, byterecv;
	char buf[BUFSIZE];
	
	if (args.Length() < 1) {
		throwTypeError("socket is not specified");
		return;
	}
	if (!args[0]->IsNumber()) {
		throwTypeError("socket is not a number");
		return;
	}

	sock = args[0]->NumberValue();
	byterecv = recv(sock, buf, BUFSIZE-1, 0);
	if (byterecv <= 0) {
		throwError("recv() failed");
		return;
	} else {
		buf[byterecv] = '\0';
		args.GetReturnValue().Set(String::NewFromUtf8(isolate, buf));
	}
}


void sendFileMethod(const FunctionCallbackInfo<Value>& args){
	Isolate* isolate = Isolate::GetCurrent();
	HandleScope scope(isolate);

	int sock;
	Local<String> filelocation;
	char buf[BUFSIZE];
	struct stat	obj;
	int	file_size;

	if (args.Length() < 1) {
		throwTypeError("socket and file location are not specified");
		return;
	} else if (args.Length() < 2) {
		throwTypeError("file location is not specified");
		return;
	}
	if (!args[0]->IsNumber()) {
		throwTypeError("socket is not a number");
		return;
	}
	if (!args[1]->IsString()) {
		throwTypeError("file location is not a string");
		return;
	}

	sock = args[0]->NumberValue();
	filelocation = args[1]->ToString();
	String::Utf8Value value(filelocation);
	strcpy(buf,*value);
	stat(buf, &obj);
	file_size = obj.st_size;

	FILE *fp = fopen(buf, "r");
	int sizecheck =0;
	char *data = (char *) malloc(file_size+1);

	while(sizecheck < file_size)
	{
		int read = fread(data, sizeof(char), file_size, fp);
		int sent = send(sock, data, read, 0);
		sizecheck += sent;
		for (int i=0; i<sent; i++){
			if (data[i] == '\n')
				sizecheck += 1;
		}
	}
	fclose(fp);
	free(data);

	if (sizecheck <= 0){
		throwError("sendfile() failed");
		return;
	}
	args.GetReturnValue().Set(Number::New(isolate, sizecheck));
}


void receiveFileMethod(const FunctionCallbackInfo<Value>& args){
	Isolate* isolate = Isolate::GetCurrent();
	HandleScope scope(isolate);

	int sock, file_size;
	Local<String> filelocation;
	char buf[BUFSIZE];

	if (args.Length() < 1) {
		throwTypeError("socket, file size, file location are not specified");
		return;
	} else if (args.Length() < 2) {
		throwTypeError("file size and file location is not specified");
		return;
	} else if (args.Length() < 3) {
		throwTypeError("file location is not specified");
		return;
	}

	if (!args[0]->IsNumber()) {
		throwTypeError("socket is not a number");
		return;
	}
	if (!args[1]->IsNumber()) {
		throwTypeError("file size is not a number");
		return;
	}
	if (!args[2]->IsString()) {
		throwTypeError("file location is not a string");
		return;
	}
	sock = args[0]->NumberValue();
	file_size = args[1]->NumberValue();
	filelocation = args[2]->ToString();
	String::Utf8Value value(filelocation);
	strcpy(buf,*value);

	FILE *fp = fopen(buf, "w");
	char *data = (char *) malloc(file_size+1);
    int sizecheck = 0;
    while(sizecheck < file_size){
		int received = recv(sock, data, file_size, 0);
		int written = fwrite(data, sizeof(char), received, fp);
      	sizecheck += written;
		for (int i=0;i<written; i++){
  	      	if (data[i] == '\n')	sizecheck += 1;
      	}
    }
    fclose(fp);
    free(data);
	if (sizecheck <= 0){
		throwError("receivefile() failed");
		return;
	}
	args.GetReturnValue().Set(Number::New(isolate, sizecheck));
}



void closeMethod(const FunctionCallbackInfo<Value>& args){
	Isolate* isolate = Isolate::GetCurrent();
	HandleScope scope(isolate);

	int sock;
	
	if (args.Length() < 1) {
		throwTypeError("socket is not specified");
		return;
	}
	if (!args[0]->IsNumber()) {
		throwTypeError("socket is not a number");
		return;
	}
	
	sock = args[0]->NumberValue();
	int success = close(sock);
	if (success != 0){
		throwTypeError("cannot close the socker");
		return;
	}
	args.GetReturnValue().Set(Number::New(isolate, success));
}



void init(Handle<Object> exports) {
	NODE_SET_METHOD(exports, "connect", connectMethod);
	NODE_SET_METHOD(exports, "send", sendMethod);
	NODE_SET_METHOD(exports, "receive", receiveMethod);
	NODE_SET_METHOD(exports, "sendFile", sendFileMethod);
	NODE_SET_METHOD(exports, "receiveFile", receiveFileMethod);
	NODE_SET_METHOD(exports, "close", closeMethod);
}



NODE_MODULE(NODE_GYP_MODULE_NAME, init);
//link to build async addons
//https://nodeaddons.com/c-processing-from-node-js-part-4-asynchronous-addons/