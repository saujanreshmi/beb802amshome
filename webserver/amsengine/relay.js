const communication = require("../build/Release/communication");

const config = {
	user: 'admin',
	password: 'cab202amshome',
	engine: '172.28.1.3',
	port: 1111
}


exports.callEngine = function(relayreq, callback){

	if (relayreq.filetype == 'library'){
		queryLibraryFileEngine(relayreq, function(log){
			//console.log('callengine');
			callback(log);
		});
	}else if (relayreq.filetype == 'program'){
		queryProgramFileEngine(relayreq, function(log){
			callback(log);
		});
	}else{
		queryEngine(relayreq, function(log){
			callback(log);
		});
	}
}



function queryLibraryFileEngine(relayreq, callback){

	var log ="";
	const socket = communication.connect(config.port, config.engine);
	if (socket != 0)

		log = "     Connecting to the Engine\t\t\t\t[ PASS ]\n";
	else {
		log = "     Connecting to the Engine\t\t\t\t[ FAIL ]\n";
		log += "\n     [ Failed To Install the Exercise ]\n";
		callback(log);
		return;
	}
	communication.send(socket, "signin");

	if (communication.receive(socket) == "connected"){
		log += "     Sigining in to the Engine\t\t\t\t[ PASS ]\n";
		var login = "login?username="+config.user+"&password="+config.password;
		communication.send(socket, login);

		if (communication.receive(socket) == "authorised"){
			log += "     Engine Authentication\t\t\t\t[ PASS ]\n";
			communication.send(socket, relayreq.reqstring);
			var request = "getfilesize?"+relayreq.filename;
			
			if (communication.receive(socket) == request){
				log += "     Sending filename to the Engine\t\t\t[ PASS ]\n";
				communication.send(socket, relayreq.filesize);

				if (communication.receive(socket) == relayreq.filesize){
					log += "     Preparing to send file to the Engine\t\t[ PASS ]\n";
					communication.sendFile(socket, relayreq.location);

					if (communication.receive(socket) == "success"){
						log += "     File sent to the Engine\t\t\t\t[ PASS ]\n";
						log += "     [ Exercise Successfully Installed ]\n\n";
					}else{
						log += "     File sent to the Engine\t\t\t\t[ FAIL ]\n";
						log += "     [ Failed To Install the Exercise ]\n\n";
					}
				}else{
					log += "     Preparing to send file to the Engine\t\t[ FAIL ]\n";
					log += "     [ Failed To Install the Exercise ]\n\n";
				}
			}else{
				log += "     Sending filename to the Engine\t\t\t[ FAIL ]\n";
				log += "    [ Failed To Install the Exercise ]\n\n";
			}
		}else{
			log += "     Engine Authentication\t\t\t\t[ FAIL ]\n";
			log += "     [ Failed To Install the Exercise ]\n\n";
		}
	}else{
		log += "     Sigining in to the Engine\t\t\t\t[ FAIL ]\n";
		log += "     [ Failed To Install the Exercise ]\n\n";
	}
	communication.close(socket);
	callback(log);
}



function queryProgramFileEngine(relayreq, callback){
	var log ="";
	const socket = communication.connect(config.port, config.engine);
	communication.send(socket, "signin");
	if (communication.receive(socket) == "connected"){
		var login = "login?username="+config.user+"&password="+config.password;
		communication.send(socket, login);
		if (communication.receive(socket) == "authorised"){
			communication.send(socket, relayreq.reqstring);
			var request = "getfilesize?"+relayreq.filename;
			if (communication.receive(socket) == request){
				communication.send(socket, relayreq.filesize);
				if (communication.receive(socket) == relayreq.filesize){
					communication.sendFile(socket, relayreq.location);
						var filename = communication.receive(socket);
						communication.send(socket, filename);
						var filesize = communication.receive(socket);
						communication.send(socket, filesize);
						communication.receiveFile(socket, parseInt(filesize), relayreq.location2+filename);
						communication.send(socket, "success");
						if (communication.receive(socket) == "success"){
							log = filename;

					}else{
						//log += " > File failed to send\n";
						log = "error";
					}
				}else{
					//log += " > Engine Request Failed \n";
					log = "error";
				}
			}else{
				//log += " > Engine Request Failed \n";
				log = "error";
			}
		}else{
			//log += " > Engine Authentication Failed \n";
			log = "error";
		}
	}else{
		//log += " > Sorry cannot connect to Engine\n";
		log = "error";
	}
	communication.close(socket);
	callback(log);
}


function queryEngine(relayreq, callback){
	var log="";
	const socket = communication.connect(config.port, config.engine);
	if (socket != 0)
		log = " > Connecting to the engine \t\t\t\t[ PASS ]\n";
	else {
		log = " > Connecting to the engine \t\t\t\t[ FAIL ]\n";
		log += "\n [ Engine is not available ]\n";
		callback(log);
		return;
	}

	communication.send(socket, "signin");
	if (communication.receive(socket) == "connected"){
		log += " > Sigining in to the Engine\t\t\t\t[ PASS ]\n";
		var login = "login?username="+config.user+"&password="+config.password;
		communication.send(socket, login);

		if (communication.receive(socket) == "authorised"){
			log += " > Engine Authentication \t\t\t\t[ PASS ]\n";
			communication.send(socket, relayreq.reqstring);

			var request = "getfilesize?"+relayreq.filename;
			
			if (communication.receive(socket) == request){
				log += " > Message exchange test \t\t\t\t[ PASS ]\n";

				communication.send(socket, relayreq.filesize);

				if (communication.receive(socket) == relayreq.filesize){
					log += " > File size exchange test \t\t\t\t[ PASS ]\n";

					communication.sendFile(socket, relayreq.location);
					
					if (communication.receive(socket) == "success"){
						log += " > File exchange test \t\t\t\t\t[ PASS ]\n";
						log += "\n [ Engine is available ]\n";
					}else{
						log += " > File exchange test \t\t\t\t\t[ FAIL ]\n";
						log += "\n [ Engine is not available ]\n";
					}
				}else{
					log += " > File size exchange test \t\t\t\t[ FAIL ]\n";
					log += "\n [ Engine is not available ]\n";
				}
			}else{
				log += " > Message exchange test \t\t\t\t[ FAIL ]\n";
				log += "\n [ Engine is not available ]\n";
			}
		}else{
			log += " > Engine Authentication \t\t\t\t[ FAIL ]\n";
			log += "\n [ Engine is not available ]\n";
		}
	}else{
		log += "> Sigining in to the Engine\t\t\t\t[ FAIL ]\n";
		log += "\n [ Engine is not available ]\n";
	}
	communication.close(socket);
	callback(log);
}


