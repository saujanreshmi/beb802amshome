const sql = require('mssql');
const config ={
        user: 'sa',
        password: 'cab202@amshome',
        server: '172.28.1.2', 
        database: 'amshome',
        port: 1433,
        options: {
            encrypt: false
        }
    }

//this function queries table 'USER'
exports.USER = function(callback){	

    sql.connect(config, function (err) {
    
        if (err) {
            sql.close();
            callback('connect');
            return;
        }

        var request = new sql.Request();
        // query to the database and get the records
        request.query('select * from cab202.[USER]', function (err, result) {
            sql.close();    
                if (err) {
                    callback('request');
                }
                else{    	
                callback(result.recordset);
                }
            });
        });
}

exports.DROPDOWN = function(callback){  

    sql.connect(config, function (err) {
    
        if (err) {
            sql.close();
            callback('connect');
            return;
        }

        var request = new sql.Request();
        // query to the database and get the records
        request.query('select t.TOPIC, count(e.EXERCISE) as EXERCISE '+
                      'from cab202.EXERCISE e '+
                      'LEFT JOIN cab202.TOPIC t '+
                      'ON e.TOPIC_ID = t.TOPIC '+
                      'GROUP BY  t.TOPIC '+
                      'ORDER BY  t.TOPIC ASC', function (err, result) {
            sql.close();    
                if (err) {
                    callback('request');
                }
                else{       
                callback(result.recordset);
                }
            });
        });
}


exports.TOPICLIST= function(callback){
    sql.connect(config, function (err) {
    
        if (err) {
            sql.close();
            callback('connect');
            return;
        }

        var request = new sql.Request();
        // query to the database and get the records
        request.query('select t.TOPIC as TOPIC, t.NAME as NAME, t.DESCRIPTION as DESCRIPTION , count(e.EXERCISE) as EXERCISES '+
                      'from cab202.[TOPIC] t '+
                      'left join cab202.[EXERCISE] e '+
                      'on t.TOPIC = e.TOPIC_ID '+
                      'group by t.TOPIC, t.NAME, t.DESCRIPTION '+
                      'order by t.TOPIC', function (err, result) {
            sql.close();    
                if (err) {
                    callback('request');
                }
                else{       
                callback(result.recordset);
                }
            });
        });
}


exports.EXERCISELIST = function(callback, id){  

    sql.connect(config, function (err) {
    
        if (err) {
            sql.close();
            callback('connect');
            return;
        }

        var request = new sql.Request();
        // query to the database and get the records
        request.query("select * from cab202.[EXERCISE] "+
                      "where cab202.[EXERCISE].TOPIC_ID = '"+id+"' "+
                      "order by cab202.[EXERCISE].EXERCISE", function (err, result) {
        sql.close();    
            if (err) {
                callback('request');
            }
            else{       
            callback(result.recordset);
            }
        });
    });
}

exports.SPECIFICTOPIC = function(callback, id){
    sql.connect(config, function (err) {
    
        if (err) {
            sql.close();
            callback('connect');
            return;
        }

        var request = new sql.Request();
        // query to the database and get the records
        request.query("select * "+
                      "from cab202.[TOPIC] "+
                      "where cab202.[TOPIC].TOPIC = '"+id+"'", function (err, result) {
            sql.close();    
                if (err) {
                    callback('request');
                }
                else{       
                callback(result.recordset);
                }
            });
        });
}

exports.SPECIFICEXERCISE = function(callback, t_id, e_id){
    sql.connect(config, function (err) {  
        if (err) {
            sql.close();
            callback('connect');
            return;
        }

        var request = new sql.Request();
        // query to the database and get the records
        request.query("select * "+
                      "from cab202.[EXERCISE] "+
                      "where cab202.[EXERCISE].EXERCISE = '"+e_id+"' and "+
                      "cab202.[EXERCISE].TOPIC_ID = '"+t_id+"'", function (err, result) {
            sql.close();    
            if (err) {
                callback('request');
            }
            else{       
                callback(result.recordset);
            }
        });
    });
}

exports.POSTTOPIC = function(callback, id, name, description){
    sql.connect(config, function (err) {  
        if (err) {
            sql.close();
            callback('connect');
            return;
        }

        var request = new sql.Request();
        // query to the database and get the records
        request.query("insert into cab202.[TOPIC](TOPIC, NAME, DESCRIPTION) "+
                      "values ("+id+",'"+name+"','"+description+"')", function (err, result) {
            sql.close();    
            if (err) {
                callback('request');
            }
            else{       
                callback(result.rowsAffected);
            }
        });
    });
}

exports.POSTEXERCISE = function(callback, id, topic, name, description, code, requirement){

    new sql.ConnectionPool(config).connect().then(pool => {
    return pool.request().query("insert into cab202.[EXERCISE](EXERCISE, TOPIC_ID, NAME, DESCRIPTION, CODE, REQUIREMENT) "+
                      "values ("+id+","+topic+",'"+name+"','"+description+"','"+code+"','"+requirement+"')")
    }).then(result => {   
        return callback(result.rowsAffected);
        sql.close();
    }).catch(err => {

        return callback('request');
        sql.close();
    });
}




