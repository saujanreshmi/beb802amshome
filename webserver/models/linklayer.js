const query = require('./querylayer');

exports.getStudentNumber = function(callback){
	query.USER(function(result){
		var student = '';

		if (JSON.stringify(result) === '[]'){
			student = '00000000'
		}
		else if (result == 'connect' || result == 'request'){
			student = result;
		}
		else{
            student = result[0].STUDENTNUMBER
		}
		callback(student);
	})
}

exports.getDropDownContent = function(callback){
	query.DROPDOWN(function(result) {
		if (JSON.stringify(result) === '[]'){
			callback('Empty');
		}
		else if (result == 'connect' || result == 'request'){
			callback('Error');
		}
		else {
			callback(result);
		}
	})
}

exports.getTopicListRows = function(callback){
	query.TOPICLIST(function(result){
		if (JSON.stringify(result) === '[]'){
			callback('Empty');
		}
		else if (result == 'connect' || result == 'request'){
			callback('Error');
		}
		else {
			//result.sort(predicateBy("ID"));
			callback(result);
		}
	})
}

exports.getExerciseListRows = function(callback, id){
	query.EXERCISELIST(function(result){
		if (JSON.stringify(result) === '[]'){
			callback('Empty');
		}
		else if (result == 'connect' || result == 'request'){
			callback('Error');
		}
		else {
			//result.sort(predicateBy("ID"));
			callback(result);
		}
	}, id)
}

exports.getTopicDetails = function(callback, id){
	query.SPECIFICTOPIC(function(result){
		if (JSON.stringify(result) === '[]'){
			callback('Empty');
		}
		else if (result == 'connect' || result == 'request'){
			callback('Error');
		}
		else {
			callback(result[0]);
		}
	}, id)
}

exports.getExerciseDetails = function(callback, t_id, e_id){
	query.SPECIFICEXERCISE(function(result){
		if (JSON.stringify(result) === '[]'){
			callback('Empty');
		}
		else if (result == 'connect' || result == 'request'){
			callback('Error');
		}
		else {
			callback(result[0]);
		}
	}, t_id, e_id)
}
	
exports.postTopicDetails = function(callback, topic){
	query.POSTTOPIC(function(result){
		if (JSON.stringify(result) === '[]'){
			callback('Empty');
		}
		else if (result == 'connect' || result == 'request'){
			callback('Error');
		}
		else {
			callback(result[0]);
		}
	}, topic.id, topic.name, topic.description)
}

exports.postExerciseDetails = function(callback, exercise, topic, requirement) {
	query.POSTEXERCISE(function(result){
		if (JSON.stringify(result) === '[]'){
			callback('Empty');
		}
		else if (result == 'connect' || result == 'request'){
			callback('Error');
		}
		else {
			callback(result[0]);
		}
	}, exercise.id, topic, exercise.name, exercise.description, exercise.code.join(''), requirement)
}