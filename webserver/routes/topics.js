const express = require("express");
const router = express.Router();
const credential = require("../controllers/credential");
const listrow = require("../controllers/listrow");

router
	.get("/", (req, res) => {
		credential.student(function(result){
			listrow.constructTopicList(function(){	
				listrow.constructDropDownList(function(){
					res.render("topiclisttemplate", {
						page: "exercises",
						student: result,
						header: "Weekly Exercises",
						details: "Please select a topic from the list below.",
						partials: {topiclistrow: "topiclistrow",
								   dropdown: "dropdown"}
					});
				});
			});
		});
	})

	.get("/topicindex", (req, res) => {
		credential.getspecifictopic(function(result){
			if (result != 'Empty'){
				credential.student(function(student_id){
					listrow.constructExerciseList(function(){	
						listrow.constructDropDownList(function(){
							res.render("topictemplate", {
								title: "Weekly Exercises Topic "+result.TOPIC+"",
								page: "exercises",
								student: student_id,
								header: "CAB202 - Topic "+result.TOPIC+": "+result.NAME+"",
								description: result.DESCRIPTION,
								detail1: "Submissions for assessment items closed at 11:59:59 PM on 31/12/2017.",
								detail2: "Please select an exercise from the list below.",
								partials: {exerciselistrow: "exerciselistrow",
										   dropdown: "dropdown"}
							});	
						});
					}, req.query.topic);
				});
			}
			else {
				return res.redirect('/error');
			}
		}, req.query.topic)
	})

module.exports = router;