const express = require("express");
const formidable = require('formidable');
const router = express.Router();
const amsalivetest = require("../controllers/amsalivetest"); 
const amsrenovate = require("../controllers/amsrenovate"); 
const credential = require("../controllers/credential");
const listrow = require("../controllers/listrow");

router
	.get("/", (req, res) => {	
		credential.student(function(result){
			listrow.constructDropDownList(function(){
				res.render("default", {
					page: "home",
					student: result,
					partials: {dropdown: "dropdown"}, 
					header: "Welcome to AMS@Home",
					details: "This is a serverless replica of AMS that is designed to facilitate the students to enhance thier programming skills. This system requires several modules up and running for better experience for the user therefore it is necessary to check if all the required modules are up and running.",
					button: "Inspect AMS@Home",	
					action: "/",
					feedback: " \n >"
				});
			});
		});
	})

	.post("/", (req, res) => {
		amsalivetest.inspect(function(log){
			credential.student(function(result){
				listrow.constructDropDownList(function(){
					res.render("default", {
						page: "home",
						student: result,
						partials: {dropdown: "dropdown"}, 
						header: "Welcome to AMS@Home",
						details: "This is a serverless replica of AMS that is designed to facilitate the students to enhance thier programming skills. This system requires several modules up and running for better experience for the user therefore it is necessary to check if all the required modules are up and running.",
						button: "Inspect AMS@Home",	
						action: "/",
						feedback: log
					});
				});
			});
			
		});	
	})


	.get("/installer", (req, res) => {
		credential.student(function(result){
			listrow.constructDropDownList(function(){
				res.render("installer", {
					page: "installer",
					student: result,
					partials: {dropdown: "dropdown"}, 
					header: "Installer Module",
					details: "This is the exercise install module for AMS@Home that is used to install all the exercises for CAB202 to facilitate serverless AMS services. Each exercise needs to be seperately uploaded here to activate the exercises in exercise module.",
					action: "/installer",
					feedback: " \n >"
				});
			});
		});
	})

	.post("/installer", (req, res) => {
		var form = new formidable.IncomingForm();
		form.parse(req, function(err, fields, files){
			credential.student(function(result){
				listrow.constructDropDownList(function(){
					amsrenovate.postExercise(fields, files, function(log){
						res.render("installer", {
							page: "installer",
							student: result,
							partials: {dropdown: "dropdown"}, 
							header: "Installer Module",
							details: "This is the exercise install module for AMS@Home that is used to install all the exercises for CAB202 to facilitate serverless AMS services. Each exercise needs to be seperately uploaded here to activate the exercises in exercise module.",
							action: "/installer",
							feedback: log
						});
					});
				});
			});
		});
	})

module.exports = router;



