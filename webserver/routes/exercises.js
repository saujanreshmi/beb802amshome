const formidable = require('formidable');
const express = require("express");
const router = express.Router();
const amsrenovate = require("../controllers/amsrenovate"); 
const solutionexamine = require("../controllers/solutionexamine"); 
const credential = require("../controllers/credential");
const listrow = require("../controllers/listrow");


router
	.get("/", (req, res) => {
		credential.getspecificexercise(function(result){
			if (result != 'Empty'){
				credential.student(function(student_id){
					listrow.constructRequirement(function(){
						listrow.constructDropDownList(function(){
							res.render("exercisetemplate", {
								title: "Topic "+result.TOPIC_ID+" Exercise "+result.EXERCISE+"",
								page: "exercises",
								student: student_id,
								partials: {dropdown: "dropdown",
										   requirement: "requirement"}, 
								header: "CAB202 - Topic "+result.TOPIC_ID+", Exercise "+result.EXERCISE+": "+result.NAME+"!",
								action: "/topics/exerciseindex",
								submittedcode: result.CODE,
								topic: result.TOPIC_ID,
								exercise: result.EXERCISE,
								transcript: ""
							});	
						});
					}, result.REQUIREMENT);		
				});
			}
			else {
				return res.redirect('/error');
			}
		},req.query.topic, req.query.exercise)
	})

	.post("/", (req, res) => {
		var form = new formidable.IncomingForm();
		form.parse(req, function(err, fields, files){
			credential.getspecificexercise(function(result){
				if (result != 'Empty'){
					credential.student(function(student_id){
						listrow.constructRequirement(function(){
							listrow.constructDropDownList(function(){
								solutionexamine.postSolution(fields, files, function(log){
									res.render("exercisetemplate", {
										title: "Topic "+result.TOPIC_ID+" Exercise "+result.EXERCISE+"",
										page: "exercises",
										student: student_id,
										partials: {dropdown: "dropdown",
												   requirement: "requirement"}, 
										header: "CAB202 - Topic "+result.TOPIC_ID+", Exercise "+result.EXERCISE+": "+result.NAME+"!",
										action: "/topics/exerciseindex",
										submittedcode: result.CODE,
										topic: result.TOPIC_ID,
										exercise: result.EXERCISE,
										transcript: log
									});	
								});
							});
						}, result.REQUIREMENT);
					});	
				}
				else {
					return res.redirect('/error');
				}
			}, fields.topic, fields.exercise);
		})
	});
	
module.exports = router;