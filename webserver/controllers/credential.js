const linklayer = require("../models/linklayer");

exports.student = function(callback){
	linklayer.getStudentNumber(function(result){
		if (result == 'connect' || result == 'request')
			callback('00000000');
		else 
			callback(result);
	});
}

exports.getspecifictopic = function (callback, id){
	if (isNaN(id)){
		return callback('Empty');
	}

	linklayer.getTopicDetails(function(result){
		if (result == 'Empty' || result == 'Error')
			callback('Empty');
		else 
			callback(result);
	}, id)
}

exports.getspecificexercise = function (callback, t_id, e_id){
	if (isNaN(t_id) || isNaN(e_id)){
		return callback('Empty');
	}

	linklayer.getExerciseDetails(function(result){
		if (result == 'Empty' || result == 'Error')
			callback('Empty');
		else 
			callback(result);
	}, t_id, e_id)
}