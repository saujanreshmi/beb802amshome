const fs = require('fs');
const relay = require("../amsengine/relay"); 
const linkquery = require("../models/linklayer");

const enginetest = {
	filename: 'Archive.zip',
	filesize: fs.statSync(__dirname + "/../data/alivetest/Archive.zip").size.toString(),
	location: __dirname + "/../data/alivetest/Archive.zip",
	filetype: 'test',
	reqstring: "libraryindex?topic=1&exercise=0&file=Archive.zip"
}



exports.inspect = function(callback){
	//inspect database
	var log = " \n > Checking Databae Availability\n";
	log +=    " ---------------------------------\n";
	
	inspectDatabase(function(dbfeedback){
		log += dbfeedback;

		//inspect engine
		log += " \n > Checking Engine Availability\n";
		log +=    " ---------------------------------\n";
		inspectEngine(function(engfeedback){
			log += engfeedback;
			callback(log);
		});
	});
}



function inspectDatabase(callback){			
	linkquery.getStudentNumber(function(student){
		var log = " > Preparing to inspect the database \t\t\t[ PASS ]\n";
		if (student == 'connect'){
			log += " > Connecting to the database \t\t\t\t[ FAIL ]\n";
			log += " \n [ Database is not available ]\n\n";
		}else if (student == 'request'){
			log += " > Database query request \t\t\t\t[ FAIL ]\n";
			log += " \n [ Database is not available ]\n\n";
		}else{
			log += " > Connecting to the databae \t\t\t\t[ PASS ]\n";
			log += " > Database query request \t\t\t\t[ PASS ]\n";
			log += " \n [ Database is available ]\n\n";
		}
		callback(log);
	})					
}



function inspectEngine(callback){
	var log = " > Preparing to inspect the engine \t\t\t[ PASS ]\n"
	relay.callEngine(enginetest, function(feedback){
		log += feedback;
		callback(log);
	});
}


//test database

//test engine
