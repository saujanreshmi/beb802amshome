const formidable = require('formidable');
const fs = require('fs');
const path = require('path');
const tar = require('tar-fs')
const Filehound = require('filehound');
const relay = require("../amsengine/relay"); 
const constants = require("./constants");
const linkquery = require("../models/linklayer");

exports.postExercise = function(fields, files, callback){
	processRequest(fields, files, function(log){
		callback(log);
	});
}



function processRequest(fields, files, callback){
	var log=" \n";

	if (files.filetoupload.name != '')
		log += " > File Selected \t\t\t\t\t[ PASS ]\n\n";
	else 
		log += " > File Selected\t\t\t\t\t[ FAIL ]\n\n";

	validRequest(fields, files, function(valid){
		if (valid){
			saveFile(files, function(success){
				if (success){
					log += " > File Received\t\t\t\t\t[ PASS ]\n\n";
					var file = __dirname + "/../data/" + files.filetoupload.name;
					var folder = __dirname +'/../data/'+files.filetoupload.name.substring(0, files.filetoupload.name.indexOf('.'));
					var extraction = fs.createReadStream(file).pipe(tar.extract(folder));

					extraction.on('finish', () => {
						var subdirectories = getDirectories(folder);
						var topicJson = JSON.parse(fs.readFileSync(folder+'/detail.json','utf8'));

						linkquery.postTopicDetails(function(result){
							if (result == 1){
								
								var exercise = 0;
								log += " > Topic "+('0' + topicJson.id).slice(-2)+" loaded into database\t\t\t[ PASS ]\n\n";
								for(var i=1; i<=subdirectories.length; i++)
								{
									copyDir(folder+'/'+subdirectories[i-1]+'/Images', __dirname+'/../public/Images');
									
									var relayreq = constructRelayRequest(folder+'/'+subdirectories[i-1]+'/'+subdirectories[i-1]+'.zip', subdirectories[i-1]+'.zip', topicJson.id, i);
									//send zip file to engine
									relay.callEngine(relayreq, function(feedback){
										log += "   [ Installing Exercise "+('0'+i).slice(-2)+" ]\n";
										log += "   --------------------------------------\n";
										log += feedback;
										var data = fs.readFileSync(folder+'/'+subdirectories[i-1]+'/requirement.hjs', 'utf8');
									
									    var exerciseJson = JSON.parse(fs.readFileSync(folder+'/'+subdirectories[i-1]+'/detail.json'));

									    linkquery.postExerciseDetails(function(result){
												
											if (result == 1)
											{
												exercise += 1;
												log += "   Exercise "+('0'+exercise).slice(-2)+" loaded into database \t\t\t[ PASS ]\n\n";
											}else{
												exercise += 1;
												log += "   Exercise "+('0'+exercise).slice(-2)+" loaded into database \t\t\t[ FAIL ]\n\n";
											}
											if (exercise == subdirectories.length)
											{
												callback(log);
											}

										}, exerciseJson, topicJson.id, data);
										});
								}							
							}
							else
							{
								log += " > Database Topic post\t\t\t\t\t[ FAIL ]\n\n";
								callback(log);
							}
						},topicJson);
						
					})
				}else{
					log += " > File Received\t\t\t[ FAIL ]\n";
					log += "\n [ Failed To Install the Exercise ]\n";
					callback(log);
				}	
			});
		}else {
			log += "\n [ Failed To Install the Exercise]\n";
			callback(log);
		}
	});
}


function validRequest(fields, files, callback){

	if (files.filetoupload.name != '')
		callback(true);
	else 
		callback(false);
}



function saveFile(files, callback){
		var oldpath = files.filetoupload.path;
		var newpath = __dirname + "/../data/" + files.filetoupload.name;
		fs.rename(oldpath, newpath, function (err, result){
			if (err) {
				callback(false);
			}else
			callback(true);
		});
}


function constructRelayRequest(filelocation, file, topic, exercise){
	var relayreq = {
		filename: file,
		filesize: fs.statSync(filelocation).size.toString(),
		location: filelocation,
		filetype: "library",
		reqstring: "libraryindex?topic="+topic+"&exercise="+exercise+"&file="+file
	}
	return relayreq;
}


var mkdir = function(dir) {
	// making directory without exception if exists
	try {
		fs.mkdirSync(dir, 0755);
	} catch(e) {
		if(e.code != "EEXIST") {
			throw e;
		}
	}
};

var rmdir = function(dir) {
	if (path.existsSync(dir)) {
		var list = fs.readdirSync(dir);
		for(var i = 0; i < list.length; i++) {
			var filename = path.join(dir, list[i]);
			var stat = fs.statSync(filename);
			
			if(filename == "." || filename == "..") {
				// pass these files
			} else if(stat.isDirectory()) {
				// rmdir recursively
				rmdir(filename);
			} else {
				// rm fiilename
				fs.unlinkSync(filename);
			}
		}
		fs.rmdirSync(dir);
	} else {
		console.warn("warn: " + dir + " not exists");
	}
};

var copyDir = function(src, dest) {
	mkdir(dest);
	var files = fs.readdirSync(src);
	for(var i = 0; i < files.length; i++) {
		var current = fs.lstatSync(path.join(src, files[i]));
		if(current.isDirectory()) {
			copyDir(path.join(src, files[i]), path.join(dest, files[i]));
		} else if(current.isSymbolicLink()) {
			var symlink = fs.readlinkSync(path.join(src, files[i]));
			fs.symlinkSync(symlink, path.join(dest, files[i]));
		} else {
			copy(path.join(src, files[i]), path.join(dest, files[i]));
		}
	}
};

var copy = function(src, dest) {
	var oldFile = fs.createReadStream(src);
	var newFile = fs.createWriteStream(dest);
	oldFile.pipe(newFile);
};

function getDirectories(path) {
  return fs.readdirSync(path).filter(function (file) {
    return fs.statSync(path+'/'+file).isDirectory();
  });
}