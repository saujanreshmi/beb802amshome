const formidable = require('formidable');
const fs = require('fs');
const relay = require("../amsengine/relay"); 
const path = require('path');


exports.postSolution = function(fields, files, callback){

	if (fields.agree == 'true')
	{
		fs.truncate(path.join(__dirname, '../data/submission.c'), 0, function(){
			fs.appendFileSync(path.join(__dirname, '../data/submission.c'), fields.code);
			
			var relayreq = constructRelayRequest(path.join(__dirname,'../data/submission.c'), path.join(__dirname,'../data/'), 'submission.c', fields.topic, fields.exercise);

			processRequest(function(result){

				callback(result);

			}, relayreq);
		});

	}else{
		callback("");
	}
}


function processRequest(callback, relayreq){
	relay.callEngine(relayreq, function(feedback){
		if (feedback != "error")
		{
			var transcript = fs.readFileSync(relayreq.location2+feedback,'utf8')
			fs.unlinkSync(relayreq.location2+feedback);
			callback(transcript);
		}
	});
}


function constructRelayRequest(filelocation, location2, file, topic, exercise){
	var relayreq = {
		filename: file,
		filesize: fs.statSync(filelocation).size.toString(),
		location: filelocation,
		location2: location2,
		filetype: "program",
		reqstring: "topicindex?topic="+topic+"&exercise="+exercise+"&file="+file
	}
	return relayreq;
}
