
function define(name, value) {
    Object.defineProperty(exports, name, {
        value:      value,
        enumerable: true
    });
}

define("TOPIC", 4);
define("EXERCISE_TOPIC1", 3);
define("EXERCISE_TOPIC2", 3);
define("EXERCISE_TOPIC3", 3);
