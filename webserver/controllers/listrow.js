const fs = require('fs');
const path = require('path');
const linklayer = require("../models/linklayer");

exports.constructTopicList = function(callback){
	fs.truncate(path.join(__dirname, '../views/topiclistrow.hjs'), 0, function(){
		linklayer.getTopicListRows(function(result){
			if (result != 'Empty' && result != 'Error')
			{
				for (var key in result) {
					if (result.hasOwnProperty(key)) {
						var row = createTopicListRow(result[key].TOPIC, result[key].NAME, result[key].EXERCISES, result[key].DESCRIPTION);
						fs.appendFileSync(path.join(__dirname, '../views/topiclistrow.hjs'), row);
					}
				}
			}
			callback();
		});
	});
}

exports.constructExerciseList = function(callback, id){
	fs.truncate(path.join(__dirname, '../views/exerciselistrow.hjs'), 0, function(){
		linklayer.getExerciseListRows(function(result){
			if (result != 'Empty' && result != 'Error')
			{
				for (var key in result) {
					if (result.hasOwnProperty(key)) {
						var row = createExerciseListRow(result[key].EXERCISE, result[key].TOPIC_ID, result[key].NAME, 0, result[key].DESCRIPTION);
						fs.appendFileSync(path.join(__dirname, '../views/exerciselistrow.hjs'), row);
					}
				}
			}
			callback();
		}, id);
	});
}

exports.constructDropDownList = function(callback){
	fs.truncate(path.join(__dirname, '../views/dropdown.hjs'), 0, function(){
		linklayer.getDropDownContent(function(result){
			if (result != 'Empty' && result != 'Error')
			{
				for (var key in result) {
					if (result.hasOwnProperty(key)) {
						var row = createDropDown(result[key].TOPIC, 0, result[key].EXERCISE);
						fs.appendFileSync(path.join(__dirname, '../views/dropdown.hjs'), row);
					}
				}
			}
			callback();
		});
	});
}

exports.constructRequirement = function(callback, requirement){
	fs.truncate(path.join(__dirname, '../views/requirement.hjs'), 0, function(){
		fs.appendFileSync(path.join(__dirname, '../views/requirement.hjs'), requirement);
		callback();
	})
}



function createTopicListRow(id, name, exercises, description){
	var line = "";
	line += '<div class="row">\n'
	line += '<div class="topic-index">\n'
	line += '<p>Topic '+id+'</p>\n'
	line += '</div>\n'
	line += '<div class="topic-details">\n'
   	line += '<p><b>'+name+':</b> '+description+'</p>\n'
   	line += '<p> This Topic contains '+exercises+' exercises\n'
   	line += '<form action="/topics/topicindex" method="GET">\n'
   	line += '<button type="submit" name="topic" value="'+id+'">Go to exercises of Topic '+id+' ▶</button>\n'
   	line += '</form>\n'
   	line += '</div>\n'
   	line += '</div>\n'
   	return line;
}

function createExerciseListRow(id, topic, name, attempt, description){
	var line = "";
	line += '<div class="row">\n'
	line += '<div class="topic-index">\n'
	line += '<p>Exercise '+id+'</p>\n'
	line += '</div>\n'
	line += '<div class="topic-details">\n'
   	line += '<p><b>'+name+'!:</b> '+description+'</p>\n'
   	line += '<p> You have made '+attempt+' attempts\n'
   	line += '<form action="/topics/exerciseindex" method="GET">\n'
   	line += '<input type="hidden" name="topic" value="'+topic+'"\n>'
   	line += '<button type="submit" name="exercise" value="'+id+'">Attempt '+name+' ▶</button>\n'
   	line += '</form>\n'
   	line += '</div>\n'
   	line += '</div>\n'
   	return line;
}

function createDropDown(topic, attempt, exercise){
	var t = ('0' + topic).slice(-2);
	var a = ('0' + attempt).slice(-2);
	var e = ('0' + exercise).slice(-2);
	var line = "";
	line += '<a href="" class="" >Topic '+ t +' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '+a+'/'+e+'</a>\n'
    line += '<a href="" class="divider"></a>\n'
    return line;
}